<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'Create Employee';
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-create">

    <?php if($_GET['type'] == 1) { ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    <?php } else {?>
        <?= $this->render('_formmou', [
            'model' => $model,
        ]) ?>
    <?php } ?>

</div>
