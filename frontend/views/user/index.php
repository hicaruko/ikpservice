<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'license',
            'first_name',
            'last_name',
            'phone',
            [
                'label' => 'Type',
                'attribute'=>'type',
                'headerOptions' => ['width' => '80'],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->type == 0)
                    {
                        return "ผู้ดูแลระบบ";
                    }
                    else  if($model->type == 1){

                        return "ผู้รับมอบอำนาจ";
                    }
                    else  if($model->type == 2){
                        return "ผู้ใช้ทั่วไป";

                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
