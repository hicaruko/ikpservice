<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'สร้าง ต่อ 2 ปีหลัง';
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mou-create">


    <?= $this->render('_form', [
        'model' => $model,
        'isUpdate' => 0,

    ]) ?>

</div>
