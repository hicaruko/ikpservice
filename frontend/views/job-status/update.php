<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobStatus */

$this->title = 'Update Job Status: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Job Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-status-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
