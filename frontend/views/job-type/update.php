<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobType */

$this->title = 'Update ประเภทงาน: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ประเภทงาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="job-type-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
