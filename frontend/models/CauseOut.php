<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cause_out".
 *
 * @property int $id
 * @property string $title
 */
class CauseOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cause_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'เหตุผลที่ออก',
        ];
    }
}
