<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'first_name_en') ?>

    <?= $form->field($model, 'last_name_en') ?>

    <?php // echo $form->field($model, 'nickname') ?>

    <?php // echo $form->field($model, 'blood') ?>

    <?php // echo $form->field($model, 'date_of_birth') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'contact_number') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'foreign_address') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'passport') ?>

    <?php // echo $form->field($model, 'passport_ext') ?>

    <?php // echo $form->field($model, 'passport_is') ?>

    <?php // echo $form->field($model, 'passport_ext_alert') ?>

    <?php // echo $form->field($model, 'issue_location') ?>

    <?php // echo $form->field($model, 'visa_is') ?>

    <?php // echo $form->field($model, 'visa_ext') ?>

    <?php // echo $form->field($model, 'visa_number') ?>

    <?php // echo $form->field($model, 'visa_type') ?>

    <?php // echo $form->field($model, 'stamped') ?>

    <?php // echo $form->field($model, 'entrance_card') ?>

    <?php // echo $form->field($model, 'workpermit_is') ?>

    <?php // echo $form->field($model, 'workpermit_ext') ?>

    <?php // echo $form->field($model, 'tt_number') ?>

    <?php // echo $form->field($model, 'work_permit_number') ?>

    <?php // echo $form->field($model, 'job_description') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'report_tm_date') ?>

    <?php // echo $form->field($model, 'report_status') ?>

    <?php // echo $form->field($model, 'mou_id') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'company_id') ?>

    <?php // echo $form->field($model, 'nationality_id') ?>

    <?php // echo $form->field($model, 'prefix_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
