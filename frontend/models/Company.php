<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $name_en
 * @property string $address_no
 * @property string $moo
 * @property string $soi
 *
 * @property string $branch
 * @property string $road
 * @property string $telephone
 * @property string $fax
 * @property int $business_type_id
 * @property int $type
 * @property string $first_name
 * @property string $first_name_en
 * @property string $last_name
 * @property string $last_name_th
 * @property string $position
 * @property string $position_en
 * @property int $prefix_id
 * @property int $provinces_id
 * @property int $districts_id
 * @property int $subdistricts_id
 * @property int $user_id
 * @property string $license
 * @property string $license_en
 * @property string $soi_n
 * @property string $road_en
 * @property string $province
 * @property string $province_en
 * @property string $districts
 * @property string $districts_en
 * @property string $subdistricts
 * @property string $subdistricts_en
 * @property string $zipcode
 * @property string $first_name2
 * @property string $first_name2_en
 * @property string $last_name2
 * @property string $last_name2_en
 * @property int $prefix_id1
 * @property string $license_date
 * @property int $license_price
 * @property User $user
 * @property string $job_description
 * @property int $province_area
 * @property int $number_ceo

 * @property BusinessType $businessType
 * @property Districts $districts0
 * @property Prefix $prefix
 * @property Prefix $prefixId1
 * @property Provinces $provinces
 * @property Subdistricts $subdistricts0
 * @property Mou[] $mous
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_en', 'address_no', 'business_type_id', 'first_name', 'first_name_en', 'last_name', 'last_name_th', 'position', 'position_en', 'prefix_id','type','number_ceo'], 'required'],
            [['business_type_id', 'type', 'prefix_id', 'provinces_id', 'districts_id', 'subdistricts_id'], 'integer'],
            [['user_id', 'first_name2', 'first_name2_en', 'last_name2', 'last_name2_en','prefix_id1','job_description','province_area','license_date','license_price','branch'], 'safe'],
            [['code', 'name', 'name_en', 'address_no', 'moo', 'soi', 'road', 'telephone', 'fax', 'first_name', 'last_name', 'position', 'license', 'license_en', 'soi_n', 'road_en', 'province', 'province_en', 'districts', 'districts_en', 'subdistricts', 'subdistricts_en', 'zipcode'], 'string', 'max' => 255],
            [['first_name_en', 'last_name_th', 'position_en'], 'string', 'max' => 45],
            [['business_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessType::className(), 'targetAttribute' => ['business_type_id' => 'id']],
            [['districts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['districts_id' => 'id']],
            [['prefix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prefix::className(), 'targetAttribute' => ['prefix_id' => 'id']],
            [['provinces_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provinces::className(), 'targetAttribute' => ['provinces_id' => 'id']],
            [['subdistricts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdistricts::className(), 'targetAttribute' => ['subdistricts_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'โค้ดนายจ้าง',
            'name' => 'ชื่อภาษาไทย ( นิติบุคคลใส่ชื่อบริษัท / บุคคลธรรมดาใส่คำนำหน้า ชื่อและนามสกุล ให้ครบถ้วน )',
            'name_en' => 'ชื่อภาษาอังกฤษ ( นิติบุคคลใส่ชื่อบริษัท / บุคคลธรรมดาใส่คำนำหน้า ชื่อและนามสกุล ให้ครบถ้วน )',
            'business_type_id' => 'ประเภทธุรกิจ',
            'type' => 'ประเภท',
            'number_ceo' => 'จำนวนบุคคลที่สามารถลงลายมือชื่อได้ / คน',
            'first_name' => 'ชื่อภาษาไทย ',
            'first_name_en' => 'ชื่อภาษาอังกฤษ ',
            'last_name' => 'นามสกุลภาษาไทย',
            'last_name_th' => 'นามสกุลภาษาอังกฤษ',
            'position' => 'ตำแหน่งภาษาไทย',
            'position_en' => 'ตำแหน่งภาษาอังกฤษ',
            'first_name2' => 'ชื่อภาษาไทยคนที่ 2 ',
            'first_name2_en' => 'ชื่อภาษาอังกฤษคนที่ 2 ',
            'last_name2' => 'นามสกุลภาษาไทยคนที่ 2',
            'last_name2_en' => 'นามสกุลภาษาอังกฤษคนที่ 2',
            'prefix_id1' => 'คำนำหน้าชื่อคนที่ 2',
            'prefix_id' => 'คำนำหน้าชื่อคนที่ 1',
            'provinces_id' => 'Provinces ID',
            'province_area' => 'สำนักงานจัดหางานพื้นที่ (1-10)',
            'districts_id' => 'Districts ID',
            'subdistricts_id' => 'Subdistricts ID',
            'license' => 'เลขที่นิติบุคคล / บุคคลธรรมดา',
            'job_description' => 'ลักษณะงาน',
            'license_en' => 'เลขที่ใบอณุญาติภาษาอังกฤษ',
            'address_no' => 'ที่อยู่เลขที่',
            'moo' => 'หมู่',
            'soi' => 'ซอยภาษาไทย',
            'road' => 'ถนนภาษาไทย',
            'telephone' => 'เบอร์โทร',
            'fax' => 'แฟกซ์',
            'soi_n' => 'ซอยภาษาอังกฤษ',
            'road_en' => 'ถนนภาษาอังกฤษ',
            'province' => 'จังหวัดภาษาไทย',
            'province_en' => 'จังหวัดภาษาอังกฤษ',
            'districts' => 'อำเภอภาษาไทย',
            'districts_en' => 'อำเภอภาษาอังกฤษ',
            'subdistricts' => 'ตำบลภาษาไทย',
            'subdistricts_en' => 'ตำบลภาษาอังกฤษ',
            'zipcode' => 'รหัสไปรษณีย์',
            'license_date' => 'วันที่จดทะเบียน',
            'license_price' => 'ทุนจดทะเบียนที่ชำระแล้ว',
            'branch' => 'สาขา',

        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->user_id = Yii::$app->user->id;
            } else {

            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessType()
    {
        return $this->hasOne(BusinessType::className(), ['id' => 'business_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts0()
    {
        return $this->hasOne(Districts::className(), ['id' => 'districts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefix()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefixId1()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinces()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'provinces_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistricts0()
    {
        return $this->hasOne(Subdistricts::className(), ['id' => 'subdistricts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMous()
    {
        return $this->hasMany(Mou::className(), ['company_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}
