<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Witness */

$this->title = 'Update Witness: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Witnesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="witness-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
