<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Prefix]].
 *
 * @see Prefix
 */
class PrefixQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Prefix[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Prefix|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
