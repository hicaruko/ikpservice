<?php

use app\models\User;
use app\models\Witness;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Out */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="out-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12" style="display: none;">
            <?= $form->field($model, 'type')->textInput(['value' => $_GET['type']]) ?>

        </div>
        <div class="col-md-12">

            <h4>ข้อมูล นายจ้าง</h4>
            <div style="display: none;">
                <?php
                $Mou = \app\models\Mou::find()->where(['id'=>$_GET['mou']])->one();
                echo  $form->field($model, 'company_id')->textInput(['value' => $Mou->company_id]);
                echo  $form->field($model, 'mou_id')->textInput(['value' => $Mou->id]);
                echo  $form->field($model, 'nationality_id')->textInput(['value' => $Mou->nationality_id]);

                ?>
            </div>
            <?php

            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

            ?>
        </div>
    </div>



    <?= $form->field($model,'out_date')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'

    ]]) ?>




        <?php
        if($user->type == 0) {
            $datas = Witness::find()->all();
        } else {
            $datas = Witness::find()->where(['user_id'=>Yii::$app->user->getId()])->all();

        }
        $datalists = [];
        foreach ($datas as $values){
            $datalists[$values->id] = $values->first_name." ".$values->last_name;
        }

        echo $form->field($model, 'witness_id')->widget(Select2::classname(), [
            'data' => $datalists,
            'options' => ['placeholder' => 'เลือกพยานคนที่ 1 ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);

        ?>


        <?php

        echo $form->field($model, 'witness_id2')->widget(Select2::classname(), [
            'data' => $datalists,
            'options' => ['placeholder' => 'เลือกพยานคนที่ 2 ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);

        ?>
    <?php if($user->type == 0) { ?>

    <?php
    $datas = User::find()->where(['is_grantee'=>1])->all();
    $datalists = [];
    foreach ($datas as $values){
        $datalists[$values->id] = $values->first_name." ".$values->last_name;
    }

    echo $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $datalists,
        'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>


    <?php } else { ?>
    <div style="display: none">

        <?php echo $form->field($model, 'user_id')->textInput(['value'=>Yii::$app->user->id]) ?>
    </div>
    <?php }?>



    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
