<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prefix */

$this->title = 'Update Prefix: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Prefixes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prefix-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
