<aside class="main-sidebar">

    <section class="sidebar">

        <?php if(!Yii::$app->user->isGuest) {
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            ?>

        <?php } ?>

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <?php if(!Yii::$app->user->isGuest) { ?>
                <p><?php echo $user->first_name ?> <?php echo $user->last_name ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                <?php } ?>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php if(!Yii::$app->user->isGuest) {
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            ?>
            <?php if($user->type == 0) { ?>

                <?= dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Home', 'icon' => 'dashboard', 'url' => ['/site']],
                            ['label' => 'MOU Menu ', 'options' => ['class' => 'header']],
                            [
                                'label' => 'MOU',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ MOU', 'icon' => 'file-code-o', 'url' => ['mou/index']],
                                    ['label' => 'สร้าง MOU ', 'icon' => 'file-code-o', 'url' => ['mou/create']],
                                    ['label' => 'สถานะงาน', 'icon' => 'file-code-o', 'url' => ['job-status/index']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],

                            [
                                'label' => 'แจ้งเข้า / ต่ออายุ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ แจ้งเข้า / ต่ออายุ', 'icon' => 'file-code-o', 'url' => ['mou-new/index']],
                                    ['label' => 'สร้าง แจ้งเข้า / ต่ออายุ ', 'icon' => 'file-code-o', 'url' => ['mou-new/create']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],


                            [
                                'label' => 'แจ้งออก',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ  แจ้งออก', 'icon' => 'file-code-o', 'url' => ['out/index?type=1']],
                                    ['label' => 'สร้าง  แจ้งออก ', 'icon' => 'file-code-o', 'url' => ['out/create?type=1']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],


                            ['label' => 'Main Menu ', 'options' => ['class' => 'header']],
                            [
                                'label' => 'ผู้ใช้งานระบบ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'User Manage', 'icon' => 'file-code-o', 'url' => ['user/index']],
                                    ['label' => 'User Create', 'icon' => 'file-code-o', 'url' => ['user/create']],
                                ],
                            ],
                            [
                                'label' => 'คำนำหน้าชื่อ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Prefix Manage', 'icon' => 'file-code-o', 'url' => ['prefix/index']],
                                    ['label' => 'Prefix Create', 'icon' => 'file-code-o', 'url' => ['prefix/create']],
                                ],
                            ],
                            [
                                'label' => 'ประเภทงาน',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'ประเภทงาน Manage', 'icon' => 'file-code-o', 'url' => ['job-type/index']],
                                    ['label' => 'ประเภทงาน Create', 'icon' => 'file-code-o', 'url' => ['job-type/create']],
                                ],
                            ],
                            [
                                'label' => 'ลักษณะงาน',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'ลักษณะงาน Manage', 'icon' => 'file-code-o', 'url' => ['job-description/index']],
                                    ['label' => 'ลักษณะงาน Create', 'icon' => 'file-code-o', 'url' => ['job-description/create']],
                                ],
                            ],



                            [
                                'label' => 'เหตุผลที่ออก',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'เหตุผลที่ออก Manage', 'icon' => 'file-code-o', 'url' => ['cause-out/index']],
                                    ['label' => 'เหตุผลที่ออก Create', 'icon' => 'file-code-o', 'url' => ['cause-out/create']],
                                ],
                            ],
                        [
                            'label' => 'ประเภทเอกสารอื่นๆ',
                            'icon' => 'circle-o',
                            'url' => '#',
                            'items' => [
                                ['label' => 'ประเภทเอกสารอื่นๆ Manage', 'icon' => 'file-code-o', 'url' => ['other-document-type/index']],
                                ['label' => 'ประเภทเอกสารอื่นๆ Create', 'icon' => 'file-code-o', 'url' => ['other-document-type/create']],
                            ],
                        ],
                            [
                                'label' => 'สัญชาติ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Nationality Manage', 'icon' => 'file-code-o', 'url' => ['nationality/index']],
                                    ['label' => 'Nationality Create', 'icon' => 'file-code-o', 'url' => ['nationality/create']],
                                ],
                            ],
                            ['label' => 'Business Menu ', 'options' => ['class' => 'header']],

                            [
                                'label' => 'ประเภทธุรกิจ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Business Type Manage', 'icon' => 'file-code-o', 'url' => ['business-type/index']],
                                    ['label' => 'Business Type Create', 'icon' => 'file-code-o', 'url' => ['business-type/create']],
                                ],
                            ],
                            [
                                'label' => 'นายจ้าง',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Company Manage', 'icon' => 'file-code-o', 'url' => ['company/index']],
                                    ['label' => 'Company Create', 'icon' => 'file-code-o', 'url' => ['company/create']],
                                ],
                            ],
                            [
                                'label' => 'เอเจนซี่',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Agent Manage', 'icon' => 'file-code-o', 'url' => ['agent/index']],
                                    ['label' => 'Agent Create', 'icon' => 'file-code-o', 'url' => ['agent/create']],
                                ],
                            ],
                            [
                                'label' => 'พยาน',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Witness Manage', 'icon' => 'file-code-o', 'url' => ['witness/index']],
                                    ['label' => 'Witness Create', 'icon' => 'file-code-o', 'url' => ['witness/create']],
                                ],
                            ],

                            [
                                'label' => 'ศูนย์อบรม',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Dan Immigration Manage', 'icon' => 'file-code-o', 'url' => ['training/index']],
                                    ['label' => 'Dan Immigration Create', 'icon' => 'file-code-o', 'url' => ['training/create']],
                                ],
                            ],
                            [
                                'label' => 'ด่านตรวจคนเข้าเมือง',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Dan Immigration Manage', 'icon' => 'file-code-o', 'url' => ['dan-immigration/index']],
                                    ['label' => 'Dan Immigration Create', 'icon' => 'file-code-o', 'url' => ['dan-immigration/create']],
                                ],
                            ],

                            [
                                'label' => 'ข้อมูลจังหวัด',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Provinces Manage', 'icon' => 'file-code-o', 'url' => ['provinces/index']],
                                    ['label' => 'Provinces Create', 'icon' => 'file-code-o', 'url' => ['provinces/create']],
                                ],
                            ],

                            [
                                'label' => 'ข้อมูลอำเภอ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Districts Manage', 'icon' => 'file-code-o', 'url' => ['districts/index']],
                                    ['label' => 'Districts Create', 'icon' => 'file-code-o', 'url' => ['districts/create']],
                                ],
                            ],

                            [
                                'label' => 'ข้อมูลตำบล',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Sub Districts Manage', 'icon' => 'file-code-o', 'url' => ['subdistricts/index']],
                                    ['label' => 'Sub Districts Create', 'icon' => 'file-code-o', 'url' => ['subdistricts/create']],
                                ],
                            ],
                            [
                                'label' => 'ตั้งค่าระบบ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Setting Manage', 'icon' => 'file-code-o', 'url' => ['setting/update?id=1']],
                                ],
                            ],
                            ['label' => 'Logout', 'url' => ['site/logout'], 'template'=>'<a href="{url}" data-method="post">{label}</a>', 'visible' => !Yii::$app->user->isGuest],

                            ['label' => 'System Menu ', 'options' => ['class' => 'header']],

                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                            [
                                'label' => 'Some tools',
                                'icon' => 'share',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                                    [
                                        'label' => 'Level One',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                            [
                                                'label' => 'Level Two',
                                                'icon' => 'circle-o',
                                                'url' => '#',
                                                'items' => [
                                                    ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                                    ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
                ) ?>

            <?php } else { ?>
                <?= dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'MOU Menu ', 'options' => ['class' => 'header']],
                            [
                                'label' => 'MOU',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ MOU', 'icon' => 'file-code-o', 'url' => ['mou/index']],
                                    ['label' => 'สร้าง MOU ', 'icon' => 'file-code-o', 'url' => ['mou/create']],
                                    ['label' => 'สถานะงาน', 'icon' => 'file-code-o', 'url' => ['job-status/index']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],

                            [
                                'label' => 'แจ้งเข้า / ต่ออายุ',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ แจ้งเข้า / ต่ออายุ', 'icon' => 'file-code-o', 'url' => ['mou-new/index']],
                                    ['label' => 'สร้าง แจ้งเข้า / ต่ออายุ ', 'icon' => 'file-code-o', 'url' => ['mou-new/create']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],


                            [
                                'label' => 'แจ้งออก',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รายการ  แจ้งออก', 'icon' => 'file-code-o', 'url' => ['out/index?type=1']],
                                    ['label' => 'สร้าง  แจ้งออก ', 'icon' => 'file-code-o', 'url' => ['out/create?type=1']],
//                                    ['label' => 'ลูกจ้าง', 'icon' => 'file-code-o', 'url' => ['employee/index']],

                                ],
                            ],

                            ['label' => 'Business Menu ', 'options' => ['class' => 'header']],
                            [
                                'label' => 'นายจ้าง',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Company Manage', 'icon' => 'file-code-o', 'url' => ['company/index']],
                                    ['label' => 'Company Create', 'icon' => 'file-code-o', 'url' => ['company/create']],
                                ],
                            ],
                            [
                                'label' => 'พยาน',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Witness Manage', 'icon' => 'file-code-o', 'url' => ['witness/index']],
                                    ['label' => 'Witness Create', 'icon' => 'file-code-o', 'url' => ['witness/create']],
                                ],
                            ],
                            ['label' => 'Logout', 'url' => ['site/logout'], 'template'=>'<a href="{url}" data-method="post">{label}</a>', 'visible' => !Yii::$app->user->isGuest],



                        ],
                    ]
                ) ?>
            <?php }   ?>



        <?php } else {  ?>

            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [

                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Logout', 'url' => ['site/logout'], 'template'=>'<a href="{url}" data-method="post">{label}</a>', 'visible' => !Yii::$app->user->isGuest],

                    ],
                ]
            ) ?>

        <?php }  ?>



    </section>

</aside>
