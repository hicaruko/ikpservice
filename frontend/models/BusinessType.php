<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "business_type".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 *
 * @property Company[] $companies
 */
class BusinessType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'title_en'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title_en'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_en' => 'Title En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['business_type_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return BusinessTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BusinessTypeQuery(get_called_class());
    }
}
