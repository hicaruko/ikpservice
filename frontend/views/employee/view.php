<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employee-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'first_name_en',
            'last_name_en',
            'nickname',
            'blood',
            'date_of_birth',
            'sex',
            'contact_number',
            'age',
            'email:email',
            'foreign_address:ntext',
            'photo:ntext',
            'passport',
            'passport_ext',
            'passport_is',
            'passport_ext_alert:ntext',
            'issue_location',
            'visa_is',
            'visa_ext',
            'visa_number',
            'visa_type',
            'stamped',
            'entrance_card',
            'workpermit_is',
            'workpermit_ext',
            'tt_number',
            'work_permit_number',
            'job_description',
            'position',
            'report_tm_date',
            'report_status',
            'mou_id',
            'type',
            'address:ntext',
            'company_id',
            'nationality_id',
            'prefix_id',
        ],
    ]) ?>

</div>
