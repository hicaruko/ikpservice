<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\Employee;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Pdfemc44moucontactController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
        $value = Employee::find()->where(['id'=>$_GET['em_id']])->one();
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";



        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/BT_EM_C_new44.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);




            if($pageNo == 5) { // 9

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 48;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->province.'</div>
', 24, $y1, 26, 90, 'auto');
                $y2 = 44;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->province_en.'</div>
', 119, $y2, 23, 90, 'auto');

                $y1 = 59;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', 20, $y1, 79, 90, 'auto');
                $y2 = 49;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->name_en.'</div>
', 112, $y2, 77, 90, 'auto');

                $y1 = 69;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.'</div>
', 34, $y1, 99-34, 90, 'auto');
                $y2 = 56;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.','.$mou->company->moo.','.$mou->company->soi_n.' </div>
', 124, $y2, 65, 90, 'auto');

                $dis_name =" อ.";
                $sub_disname = " ต.";
                if($mou->company->province == "กรุงเทพมหานคร"){
                    $dis_name =" เขต";
                    $sub_disname = " แขวง";
                }


                $y1 = 89;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$sub_disname.''.$mou->company->subdistricts.$dis_name.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', 20, $y1, 79, 90, 'auto');




                $y2 = 59;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts_en.','.$mou->company->districts_en.','.$mou->company->province_en.','.$mou->company->zipcode.'</div>
', 112, $y2, 77, 90, 'auto');



                $y22 = 78;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->province_en.'</div>
', 115, $y22, 30, 90, 'auto');

                $y2 = 85;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->name_en.'</div>
', 112, $y2, 77, 90, 'auto');
                $y2 = $y2+6.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.','.$mou->company->moo.','.$mou->company->soi_n.' </div>
', 128, $y2, 189-128, 90, 'auto');




                $y11 = 114;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.'</div>
', 34, $y11, 99-34, 90, 'auto');

                $y11 = 129;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$sub_disname.''.$mou->company->subdistricts.$dis_name.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', 20, $y11, 79, 90, 'auto');


                $y1s = 105;
                $start = 16;
                $end = 98;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1s, $end - $start, 90, 'auto');

                $y1s = 105;
                $start = 113;
                $end = 190;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1s, $end - $start, 90, 'auto');



                $y2s = 110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.','.$mou->company->moo.','.$mou->company->soi_n.' </div>
', 124, $y2s, 65, 90, 'auto');

                $y2s = 115;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts_en.','.$mou->company->districts_en.','.$mou->company->province_en.','.$mou->company->zipcode.'</div>
', 112, $y2s, 77, 90, 'auto');



                $y2 = $y2+3;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts_en.','.$mou->company->districts_en.','.$mou->company->province_en.','.$mou->company->zipcode.'</div>
', 112, $y2, 77, 90, 'auto');


                $y1 = 175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">งานกรรมกร</div>
', 72, $y1, 100-72, 90, 'auto');

                $y1 = 184;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 33, $y1, 62-33, 90, 'auto');

                $y1 = 195;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 54, $y1, 76-54, 90, 'auto');


                $y2 = 170;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">Labour</div>
', 177, $y2, 192-177, 90, 'auto');

                $y2 = 170;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 139, $y2, 156-139, 90, 'auto');

                $y2 = 175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 181, $y2, 192-181, 90, 'auto');



                $y2 = 190;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">Labour</div>
', 159, $y2, 173-159, 90, 'auto');

                $y2 = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 122, $y2, 135-122, 90, 'auto');
                $y2 = 199;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 141, $y2, 151-141, 90, 'auto');


                if($mou->preriod_month == 0){
                    $y2 = 215;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'</div>
', 171, $y2, 184-171, 90, 'auto');

                } else {
                    $y2 = 215;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 171, $y2, 184-171, 90, 'auto');

                }


                if($mou->preriod_month == 0){
                    $y1 = 215;

                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'</div>
', 61, $y1, 79-61, 90, 'auto');

                } else {

                    $y1 = 215;

                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 61, $y1, 79-61, 90, 'auto');

                }


                $dis_name =" อ.";
                $sub_disname = " ต.";
                if($mou->province == "กรุงเทพมหานคร"){
                    $dis_name =" เขต";
                    $sub_disname = " แขวง";
                }


                $y1 = 234;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.' ม.'.$mou->moo.','.$mou->soi.$sub_disname.$mou->subdistricts.'</div>
', 35, $y1, 64, 90, 'auto');

                $y1 = 249.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$dis_name.''.$mou->districts.','.$mou->province.','.$mou->zipcode.'</div>
', 20, $y1, 79, 90, 'auto');


                $y2 = 235;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.','.$mou->moo.','.$mou->soi_n.','.$mou->subdistricts_en.','.$mou->districts_en.','.$mou->province_en.','.$mou->zipcode.'</div>
', 112, 231, 77, 90, 'auto');

                if($mou->preriod_month == 0){


                    $y2 = 240;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'</div>
', 159, $y2, 172-159, 90, 'auto');

                } else {


                    $y2 = 240;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 159, $y2, 172-159, 90, 'auto');

                }

                $y2 = 253;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.','.$mou->moo.','.$mou->soi_n.','.$mou->subdistricts_en.'</div>
', 120, $y2, 189-120, 90, 'auto');

                $y2 = $y2+5.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->districts_en.','.$mou->province_en.','.$mou->zipcode.'</div>
', 112, $y2, 189-112, 90, 'auto');


            }  // 4
            if($pageNo ==  6) { // 4

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


                //left

                $y1 = 25;
                $start = 61;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 35;
                $start = 49;
                $end =71;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 65;
                $start = 51;
                $end =68;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 84;
                $start = 44;
                $end =60;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');


                //rigth

                $y1 = 25;
                $start = 171;
                $end =185;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 30;
                $start = 130;
                $end =144;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 35;
                $start = 168;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 40;
                $start = 141;
                $end =149;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 60;
                $start = 114;
                $end =122;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 69;
                $start = 167;
                $end =176;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 80;
                $start = 114;
                $end =122;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 94;
                $start = 156;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');



            } // 5
            if($pageNo ==  7){ // 5
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            } // 6
            if($pageNo ==  8){ // 6
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            }  // 7
            if($pageNo ==  9){ // 7
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

// left

                $y1 = 93;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 153;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 127;
                $start = 30;
                $end = 82;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 143;
                $start = 132;
                $end = 174;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');



                $y1 = 178;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

// right

                $y1 = 98;
                $start = 128;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 123;
                $start = 128;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 183;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title_en.$mou->witness->first_name_en.' '.$mou->witness->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 203;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title_en.$mou->witness->first_name_en.' '.$mou->witness->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 223;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title_en.$mou->witnessIdTwo->first_name_en.' '.$mou->witnessIdTwo->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 243;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title_en.$mou->witnessIdTwo->first_name_en.' '.$mou->witnessIdTwo->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');




            } // 8


        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
