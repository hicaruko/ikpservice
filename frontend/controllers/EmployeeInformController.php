<?php

namespace frontend\controllers;

use app\models\Directory;
use app\models\Documents;
use app\models\EmployeeInform;
use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeInformController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmployeeInform();
        $model->scenario = 'create';
        $directory = Directory::find()->where(['key'=>"e-passport"])->one();
        if ($model->load(Yii::$app->request->post())) {
            if(isset($_GET['mou'])){
                $model->mou_new_id =  $_GET['mou'];
            }
            $model->code="emp-".$model->id;
            $photo  = UploadedFile::getInstance($model, 'photo');
            $passport_image  = UploadedFile::getInstance($model, 'passport_file');
            $doc_out_old_company = UploadedFile::getInstance($model, 'doc_out_old_company');

            $path = $this->getUploadPath();
            $pathDoc = $this->getUploadPathDoc();
            $pathUrl = $this->getUploadUrl();

            if($doc_out_old_company !== null) {
                $fileName = $model->first_name_en."-".md5($photo->baseName.time()) . '_docout.' . $photo->extension;
                if($doc_out_old_company->saveAs( $path.$fileName)) {
                    $model->doc_out_old_company = 'employee/' . $fileName;
                }
            }


            if ($photo !== null) {
                $fileName = $model->first_name_en."-".md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->photo = 'employee/'.$fileName;
                    if($passport_image !== null) {
                        $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName.time()) . '.' . $passport_image->extension;
                        if($passport_image->saveAs( $pathDoc.$fileName_passport)) {
                            if( $model->save()){
                                $newDoc = new Documents();
                                $newDoc->mou_id = $model->mou_new_id;
                                $newDoc->file = 'documents/'.$fileName_passport;
                                $newDoc->directory_id = $directory->id;
                                $newDoc->doc_type = 1;
                                $newDoc->ref_id = $model->id;
                                $newDoc->save();
                                if (isset($_GET['mou'])) {
                                    return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                                } else {
                                    return $this->redirect(['view', 'id' => $model->id]);

                                }
                            }
                        }
                    } else {
                        $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                    }

                } else {
                    if ($passport_image !== null) {
                        $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName.time()) . '.' . $passport_image->extension;
                        if($passport_image->saveAs( $pathDoc.$fileName_passport)) {
                            if( $model->save()){
                                $newDoc = new Documents();
                                $newDoc->mou_id = $model->mou_new_id;
                                $newDoc->file = 'documents/'.$fileName_passport;
                                $newDoc->directory_id = $directory->id;
                                $newDoc->doc_type = 1;
                                $newDoc->ref_id = $model->id;
                                $newDoc->save();
                                if (isset($_GET['mou'])) {
                                    return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                                } else {
                                    return $this->redirect(['view', 'id' => $model->id]);

                                }
                            }
                        }
                    } else {
                        $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                    }

                }
            } else {
                if ($passport_image !== null) {
                    $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName . time()) . '.' . $passport_image->extension;
                    if ($passport_image->saveAs($pathDoc . $fileName_passport)) {
                        if( $model->save()){
                            $newDoc = new Documents();
                            $newDoc->mou_id = $model->mou_new_id;
                            $newDoc->file = 'documents/'.$fileName_passport;
                            $newDoc->directory_id = $directory->id;
                            $newDoc->doc_type = 1;
                            $newDoc->ref_id = $model->id;
                            $newDoc->save();
                            if (isset($_GET['mou'])) {
                                return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                            } else {
                                return $this->redirect(['view', 'id' => $model->id]);
                            }
                        }

                    }
                } else {
                    $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                }
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/employee/';
    }
    public function getUploadPathDoc(){
        return Yii::getAlias('@webroot').'/documents/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/employee/';
    }


    public function actionRemovedoc(){

        $doc_passportData = \app\models\Documents::findOne($_GET['value']);
        if($doc_passportData){
            $doc_passportData->delete();
        }
            return $this->redirect(['employee-inform/update', 'id' => $_GET['emp'], 'mou' => $_GET['mou'], 'new' => 0]);

    }


    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPath = $model->photo;
        $oldPathdoc_out_old_company = $model->doc_out_old_company;

        if ($model->load(Yii::$app->request->post())) {
            $pathDoc = $this->getUploadPathDoc();
            $path = $this->getUploadPath();

            $passport_image = UploadedFile::getInstance($model, 'passport_file');
            $doc_out_old_company = UploadedFile::getInstance($model, 'doc_out_old_company');

            if($doc_out_old_company !== null) {
                $fileName = $model->first_name_en."-".md5($doc_out_old_company->baseName.time()) . '_docout.' . $doc_out_old_company->extension;
                if($doc_out_old_company->saveAs( $path.$fileName)) {
                    $model->doc_out_old_company = 'employee/' . $fileName;
                }
            } else {
                $model->doc_out_old_company = $oldPathdoc_out_old_company;
            }


            if ($passport_image !== null) {
                $directory = Directory::find()->where(['key' => "e-passport"])->one();
                $fileName_passport = $model->first_name_en . "-" . md5($passport_image->baseName . time()) . '.' . $passport_image->extension;
                if ($passport_image->saveAs($pathDoc . $fileName_passport)) {
                    $newDoc = new Documents();
                    $newDoc->mou_id = $model->mou_new_id;
                    $newDoc->file = 'documents/' . $fileName_passport;
                    $newDoc->directory_id = $directory->id;
                    $newDoc->doc_type = 1;
                    $newDoc->ref_id = $model->id;
                    $newDoc->save();
                }
            }
            $photo  = UploadedFile::getInstance($model, 'photo');
            $pathDoc = $this->getUploadPathDoc();
            $pathUrl = $this->getUploadUrl();
            $model->code="emp-".$model->id;
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->photo = 'employee/'.$fileName;

                    if($model->save()){
                        if(isset($_GET['mou'])){
                            return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                        } else {
                            return $this->redirect(['view', 'id' => $model->id]);

                        }
                    }

                } else {
                    $model->photo = $oldPath;
                    if($model->save()) {

                        if (isset($_GET['mou'])) {
                            return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                        } else {
                            return $this->redirect(['view', 'id' => $model->id]);

                        }
                    }
                }
            } else {
                $model->photo = $oldPath;
                if($model->save()) {

                    if (isset($_GET['mou'])) {
                        return $this->redirect(['mou-new/viewemp', 'id' => $model->mou_new_id]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                } else {
                    print_r($model->getErrors());
                }
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(isset($_GET['mou'])){
            return $this->redirect(['mou-new/viewemp', 'id' => $_GET['mou']]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeInform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployeeInform::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
