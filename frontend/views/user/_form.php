<?php

use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$model->isNewRecord) {
        if($model->license_file){
            echo '<embed src="'.Url::base().'/'.$model->license_file.'" width="500" height="500" />';
        }
    } ?>
    <?= $form->field($model, 'license_file')->fileInput() ?>

    <?php
    echo $form->field($model, 'type')->dropDownList(
        array(
           0=>'ผู้ดูแลระบบ',
           1=>'ผู้รับมอบอำนาจ',
           2=>'ผู้ใช้ทั่วไป',
        ),
        ['prompt'=>'ประเภทผู้ใช้']

    ); ?>



    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php
    $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
    echo $form->field($model, 'prefix_id')->dropDownList(
        $natiC,
        ['prompt'=>'เลือกคำนำหน้าชื่อ']

    ); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model,'license_start')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]]) ?>

    <?= $form->field($model,'license_end')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'address_no')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'moo')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'soi')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'soi_n')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'road')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'road_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>


    <div class="row">
        <div class="col-md-6">

            <?php
            $template = '<div>' .
                '<p class="repo-name">{{value}}</p>' .
                '</div>';

            echo $form->field($model, 'subdistricts')->widget(Typeahead::classname(), [

                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('label')",
                        'display' => 'label',
                        'remote' => [
                            'url' => Url::to(['company/country-list']) . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ],'templates' => [
                        'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                        'suggestion' => new JsExpression("Handlebars.compile('{$template}')"),

                    ]

                    ]
                ],

                'options' => ['placeholder' => 'พิมพ์เพื่อค้นหาตำบล ...','hint' => false , 'highlight' => true ],
                'pluginOptions' => [ 'hint' => false , 'highlight' => true ],
                'pluginEvents' => [
                    'typeahead:active' => 'function() { console.log("typeahead:active"); }',
                    'typeahead:idle' => 'function() { console.log("typeahead:idle"); }',
                    'typeahead:open' => 'function() { console.log("typeahead:open"); }',
                    'typeahead:close' => 'function() { console.log("typeahead:close"); }',
                    'typeahead:change' => 'function() { console.log("typeahead:change"); }',
                    'typeahead:render' => 'function() { console.log("typeahead:render"); }',
                    'typeahead:select' => 'function(ev, suggestion) { 
      console.log(suggestion); 
      $("#user-subdistricts_en").val(suggestion.name_in_english)
      $("#user-districts_en").val(suggestion.districts_name_in_english)
      $("#user-districts").val(suggestion.districts_name_in_thai)
      $("#user-province_en").val(suggestion.provinces_name_in_english)
      $("#user-province").val(suggestion.provinces_name_in_thai)
      $("#user-zipcode").val(suggestion.zip_code) 

     }',
                    'typeahead:autocomplete' => 'function() { console.log("typeahead:autocomplete"); }',
                    'typeahead:cursorchange' => 'function() { console.log("typeahead:cursorchange"); }',
                    'typeahead:asyncrequest' => 'function() { console.log("typeahead:asyncrequest"); }',
                    'typeahead:asynccancel' => 'function() { console.log("typeahead:asynccancel"); }',
                    'typeahead:asyncreceive' => 'function() { console.log("typeahead:asyncreceive"); }',
                ],

            ]);

            ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subdistricts_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'districts')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'districts_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'province_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

     <?php
    echo $form->field($model, 'is_grantee')->dropDownList(
        array(
            0=>'ไม่เป็นผู้รับมอบอำนาจ',
            1=>'ผู้รับมอบอำนาจ',
        ),
        ['prompt'=>'เลือกมอบหมาย']

    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
