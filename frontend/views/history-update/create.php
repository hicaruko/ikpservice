<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HistoryUpdate */

$this->title = 'Create History Update';
$this->params['breadcrumbs'][] = ['label' => 'History Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="history-update-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
