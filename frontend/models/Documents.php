<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string $file
 * @property int $directory_id
 * @property int $mou_id
 * @property int $doc_type
 * @property int $ref_id
 * @property int $out_id
 *
 * @property Directory $directory
 * @property Mou $mou
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'string'],
            [['mou_id','doc_type','ref_id','out_id'], 'safe'],
            [['directory_id'], 'required'],
            [['directory_id', 'mou_id'], 'integer'],
            [['directory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Directory::className(), 'targetAttribute' => ['directory_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'directory_id' => 'Directory ID',
            'mou_id' => 'Mou ID',
            'out_id' => 'out ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectory()
    {
        return $this->hasOne(Directory::className(), ['id' => 'directory_id']);
    }


}
