<?php

namespace frontend\controllers;

use app\models\User;
use app\models\UserDevice;
use Dompdf\Helpers;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{

    public $modelClass = 'app\models\UserDevice';

    public function actionDevice()
    {
        header('Content-Type: application/json');
        if(isset($_POST["device_token"]) && isset($_POST["user_id"])){
            $user =  User::findOne(['id'=>$_POST["user_id"]]);
            if($user){
                $UserDevice =  UserDevice::findOne(['device_token'=>$_POST["device_token"]]);
                if(!$UserDevice){
                    $UserDeviceNew = new UserDevice();
                    $UserDeviceNew->user_id = $_POST["user_id"];
                    $UserDeviceNew->device_token = $_POST["device_token"];
                    $UserDeviceNew->status = 1;
                    $UserDeviceNew->device = $_SERVER['HTTP_USER_AGENT'];
                    $UserDeviceNew->info = $_SERVER['REMOTE_ADDR'];
                    $UserDeviceNew->model = $_SERVER['HTTP_USER_AGENT'];
                    $UserDeviceNew->save();
                    return [
                        "status"=>"true",
                        'message'=>"",
                        'data'=>$UserDeviceNew
                    ];
                } else {
                    return [
                        "status"=>"true",
                        'message'=>"",
                        'info'=>$_SERVER,
                        'data'=>$UserDevice
                    ];
                }
            } else {
                return [
                    "status"=>"false",
                    'message'=>"no User invalid value",
                    'data'=>null
                ];
            }

        } else {
            return [
                "status"=>"false",
                'message'=>"invalid value",
                'data'=>null
            ];
        }


    }

}
