<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OtherDocumentType */

$this->title = 'Create Other Document Type';
$this->params['breadcrumbs'][] = ['label' => 'Other Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="other-document-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
