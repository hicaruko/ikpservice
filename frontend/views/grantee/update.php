<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grantee */

$this->title = 'Update Grantee: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Grantees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grantee-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
