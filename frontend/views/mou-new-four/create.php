<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'สร้าง ครบ 4 ปี';
$this->params['breadcrumbs'][] = ['label' => 'ครบ 4 ปี', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mou-create">


    <?= $this->render('_form', [
        'model' => $model,
        'isUpdate' => 0,

    ]) ?>

</div>
