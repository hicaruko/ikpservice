<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GranteeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grantees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grantee-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Grantee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'first_name_en',
            'last_name',
            'last_name_en',
            //'license',
            //'prefix_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
