<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DanImmigration */

$this->title = 'Update Dan Immigration: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Dan Immigrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dan-immigration-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
