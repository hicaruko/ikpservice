<?php

use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MouStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mou-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $nati = ArrayHelper::map(\app\models\JobStatus::find()->where(['status'=>$_GET['type']])->all(), 'id', 'title');
    echo $form->field($model, 'job_status_id')->dropDownList(
        $nati,
        ['prompt'=>'เลือกสถานะ']
    ); ?>

    <div class="" style="display: none;">

    <?= $form->field($model, 'ref_id')->textInput(['value' =>$_GET['mou'] ]) ?>
    <?= $form->field($model, 'type')->textInput(['value' =>$_GET['type']]) ?>

    </div>
    <?= $form->field($model,'job_date')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]]) ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
