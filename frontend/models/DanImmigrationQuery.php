<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DanImmigration]].
 *
 * @see DanImmigration
 */
class DanImmigrationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DanImmigration[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DanImmigration|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
