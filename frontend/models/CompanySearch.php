<?php

namespace frontend\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
 * CompanySearch represents the model behind the search form of `app\models\Company`.
 */
class CompanySearch extends Company
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'business_type_id', 'type', 'prefix_id', 'provinces_id', 'districts_id', 'subdistricts_id'], 'integer'],
            [['code', 'name', 'name_en', 'address_no', 'moo', 'soi', 'road', 'telephone', 'fax', 'first_name', 'first_name_en', 'last_name', 'last_name_th', 'position', 'position_en', 'license', 'license_en', 'soi_n', 'road_en', 'province', 'province_en', 'districts', 'districts_en', 'subdistricts', 'subdistricts_en', 'zipcode'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        $user_login = User::findOne(Yii::$app->user->id);

        if($user_login){
            if($user_login->type != 0){
                $query->andFilterWhere([
                    'id' => $this->id,
                    'business_type_id' => $this->business_type_id,
                    'type' => $this->type,
                    'prefix_id' => $this->prefix_id,
                    'provinces_id' => $this->provinces_id,
                    'districts_id' => $this->districts_id,
                    'subdistricts_id' => $this->subdistricts_id,
                    'user_id' => Yii::$app->user->id,
                ]);
            } else {
                $query->andFilterWhere([
                    'id' => $this->id,
                    'business_type_id' => $this->business_type_id,
                    'type' => $this->type,
                    'prefix_id' => $this->prefix_id,
                    'provinces_id' => $this->provinces_id,
                    'districts_id' => $this->districts_id,
                    'subdistricts_id' => $this->subdistricts_id,
                ]);
            }
        } else {
            $query->andFilterWhere([
                'id' => $this->id,
                'business_type_id' => $this->business_type_id,
                'type' => $this->type,
                'prefix_id' => $this->prefix_id,
                'provinces_id' => $this->provinces_id,
                'districts_id' => $this->districts_id,
                'subdistricts_id' => $this->subdistricts_id,
            ]);
        }


        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'address_no', $this->address_no])
            ->andFilterWhere(['like', 'moo', $this->moo])
            ->andFilterWhere(['like', 'soi', $this->soi])
            ->andFilterWhere(['like', 'road', $this->road])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'last_name_th', $this->last_name_th])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'position_en', $this->position_en])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'license_en', $this->license_en])
            ->andFilterWhere(['like', 'soi_n', $this->soi_n])
            ->andFilterWhere(['like', 'road_en', $this->road_en])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'province_en', $this->province_en])
            ->andFilterWhere(['like', 'districts', $this->districts])
            ->andFilterWhere(['like', 'districts_en', $this->districts_en])
            ->andFilterWhere(['like', 'subdistricts', $this->subdistricts])
            ->andFilterWhere(['like', 'subdistricts_en', $this->subdistricts_en])
            ->andFilterWhere(['like', 'zipcode', $this->zipcode]);

        return $dataProvider;
    }
}
