<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SubdistrictsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subdistricts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subdistricts-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subdistricts', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'name_in_thai',
            'name_in_english',
            'latitude',
            //'longitude',
            //'district_id',
            //'zip_code',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
