<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'อัพเดทสถานะ '.$model->company->name." / แจ้งเข้า / ต่ออายุ ";
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <p>
            <?= Html::a('อัพเดทสถานะ', ['mou-status/create', 'mou' => $model->id, 'type' => 2], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'job_status_id',
                    'mou_id',
                    'user_id',
                    'create_date',
                    //'create_at',
                    //'detail:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{view} {update} {delete}</div>',
                        'urlCreator' => function( $action, $modelA, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['mou-status/view', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 3]);
                            }
                            if ($action == "update") {
                                return Url::to(['mou-status/update', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 3]);
                            }
                            if ($action == "delete") {
                                return Url::to(['mou-status/delete', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 3]);
                            }
                        }

                    ],
//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดแจ้งออก', ['view', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 3], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งออก', ['pdfbt12out/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจ', ['companydoc3/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('แก้ไขแจ้งออก', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
