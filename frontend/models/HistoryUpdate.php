<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history_update".
 *
 * @property int $id
 * @property string $type
 * @property string $create_at
 * @property string $update_at
 * @property int $ref_id
 * @property int $user_id
 *
 * @property User $user
 */
class HistoryUpdate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history_update';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at'], 'safe'],
            [['ref_id', 'user_id'], 'required'],
            [['ref_id', 'user_id'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'ref_id' => 'Ref ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
