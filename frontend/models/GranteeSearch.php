<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Grantee;

/**
 * GranteeSearch represents the model behind the search form of `app\models\Grantee`.
 */
class GranteeSearch extends Grantee
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'prefix_id'], 'integer'],
            [['first_name', 'first_name_en', 'last_name', 'last_name_en', 'license'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grantee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prefix_id' => $this->prefix_id,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'last_name_en', $this->last_name_en])
            ->andFilterWhere(['like', 'license', $this->license]);

        return $dataProvider;
    }
}
