<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_inform".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $code
 * @property string $first_name_en
 * @property string $last_name_en
 * @property string $nickname
 * @property string $blood
 * @property string $date_of_birth
 * @property string $sex
 * @property string $contact_number
 * @property string $age
 * @property string $email
 * @property string $line
 * @property string $foreign_address
 * @property string $photo
 * @property string $passport
 * @property string $passport_ext
 * @property string $passport_is
 * @property string $passport_ext_alert
 * @property string $issue_location
 * @property string $visa_is
 * @property string $visa_ext
 * @property string $visa_number
 * @property string $visa_type
 * @property string $stamped
 * @property string $entrance_card
 * @property string $workpermit_is
 * @property string $workpermit_ext
 * @property string $tt_number
 * @property string $work_permit_number
 * @property string $job_description
 * @property string $position
 * @property string $report_tm_date
 * @property string $job_type
 * @property string $job_des
 * @property string $emp_number
 * @property int $report_status
 * @property int $mou_new_id
 * @property int $type
 * @property string $address
 * @property int $company_id
 * @property int $nationality_id
 * @property int $prefix_id
 * @property int $user_id
 * @property int $status
 *
 * @property Company $company
 * @property User $user
 * @property Nationality $nationality
 * @property Prefix $prefix
 */
class EmployeeInformtwo extends \yii\db\ActiveRecord
{

    public $passport_image;
    public $passport_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_inform_two';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_of_birth', 'visa_is', 'visa_ext', 'workpermit_is', 'workpermit_ext','line','mou_new_id','code','user_id','job_type','job_des','status','co_location'], 'safe'],
            [['foreign_address', 'photo', 'passport_ext_alert', 'address'], 'string'],
            [['emp_number'], 'string', 'max' => 13],
            [['emp_number'], 'string', 'min' => 13],
            [['passport'], 'unique'],
            [['report_status', 'mou_new_id', 'type', 'company_id', 'nationality_id', 'prefix_id'], 'integer'],
            [['company_id', 'nationality_id', 'prefix_id', 'first_name_en', 'passport', 'issue_location', 'passport_ext', 'passport_is','emp_number'], 'required'],
            [['passport_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg,pdf', 'on' => 'create'],
            [['first_name', 'last_name','last_name_en', 'nickname', 'blood', 'sex', 'contact_number', 'age', 'email', 'visa_number', 'visa_type', 'stamped', 'entrance_card', 'tt_number', 'work_permit_number', 'job_description', 'position', 'report_tm_date'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
//            [['mou_new_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mou::className(), 'targetAttribute' => ['mou_new_id' => 'id']],
            [['nationality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nationality::className(), 'targetAttribute' => ['nationality_id' => 'id']],
            [['prefix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prefix::className(), 'targetAttribute' => ['prefix_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'รหัสลูกจ้าง',
            'first_name' => 'ชื่อ (ไทย)',
            'last_name' => 'นามสกุล (ไทย)',
            'first_name_en' => 'ชื่อ (อังกฤษ)',
            'last_name_en' => 'นามสกุล (อังกฤษ)',
            'nickname' => 'ชื่อเล่น',
            'blood' => 'กรุ๊ปเลือด',
            'date_of_birth' => 'วัน เดือน ปีเกิด',
            'sex' => 'เพศ',
            'contact_number' => 'เบอร์ติดต่อ',
            'age' => 'อายุ',
            'email' => 'Email',
            'line' => 'line',
            'foreign_address' => 'ที่อยู่ต่างประเทศ',
            'photo' => 'รูปพนักงาน',
            'passport' => 'เลข Passport ',
            'passport_ext' => 'Passport หมดอายุ',
            'passport_is' => 'Passport ออกวันที่',
            'passport_ext_alert' => 'อายุเล่มคงเหลือ',
            'issue_location' => 'สถานที่ออกเล่ม',
            'co_location' => 'ประเทศออกเล่ม',
            'visa_is' => 'Visa ออกวันที่',
            'visa_ext' => 'Visa หมดอายุ',
            'visa_number' => 'เลขลงตรา',
            'visa_type' => 'ประเภท Visa',
            'stamped' => 'ที่ลงตรา',
            'entrance_card' => 'บัตรค่าเข้า(ตม.6)',
            'workpermit_is' => 'Workpermit ออกวันที่',
            'workpermit_ext' => 'Workpermit หมดอายุ',
            'tt_number' => 'เลข ทธ. 38/1',
            'work_permit_number' => 'เลขใบอนุญาตทำงาน',
            'job_description' => 'ลักษณะงาน',
            'position' => 'ตำแหน่ง',
            'report_tm_date' => 'รายงานตัวต่อ ตม.',
            'report_status' => 'สถานะรายงาน 24 ชม.',
            'mou_new_id' => 'Mou ID',
            'type' => 'ประเภทลูกจ้าง',
            'address' => 'ที่อยู่',
            'company_id' => 'นายจ้าง',
            'nationality_id' => 'สัญชาติ',
            'prefix_id' => 'คำนำหน้าชื่อ',
            'job_type' => 'ตำแหน่งหน้าที่',
            'job_des' => 'ลักษณะงาน',
            'passport_image' => 'เอกสาร passport',
            'emp_number' => 'เลขประจำตัวคนต่างด้าว 13 หลัก',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->user_id = Yii::$app->user->id;
            } else {

            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMouNew()
    {
        return $this->hasOne(MouNew::className(), ['id' => 'mou_new_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(Nationality::className(), ['id' => 'nationality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefix()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id']);
    }
}
