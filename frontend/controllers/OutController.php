<?php

namespace frontend\controllers;

use app\models\Directory;
use app\models\DirectorySearch;
use app\models\DirectoryTemp;
use app\models\DocumentsSearch;
use app\models\Employee;
use app\models\EmployeeOut;
use app\models\EmployeeSearch;
use app\models\MouNew;
use app\models\MouStatusSearch;
use frontend\models\EmployeeOutSearch;
use Yii;
use app\models\Out;
use frontend\models\OutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OutController implements the CRUD actions for Out model.
 */
class OutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Out models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Out model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Out model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Out();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->type == 1){
                return $this->redirect(['index','type'=>$_GET['type']]);
            } else {
                return $this->redirect(['mou/view','id'=>$_GET['mou']]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Out model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->type == 1){
                return $this->redirect(['index','type'=>$_GET['type']]);
            } else {
                return $this->redirect(['mou/view','id'=>$_GET['mou']]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionViewemp($id)
    {
        $searchModel = new EmployeeOutSearch();
        $dataProvider = $searchModel->searchByout(Yii::$app->request->queryParams,$id);

        return $this->render('viewemp', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing Out model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model =   $this->findModel($id);
        $model->delete();

        if($model->type == 1){


            return $this->redirect(['index','type'=>$_GET['type']]);
        } else {
            $EmployeeOut = EmployeeOut::find()->where(['out_id'=>$model->id])->all();
            foreach ($EmployeeOut as $val){
                $Employee = Employee::findOne($val->employee_id);
                $Employee->status = 1;
                $Employee->save();
            }
            return $this->redirect(['mou/view','id'=>$_GET['mou']]);

        }

    }


    public function actionViewsubdocument($id)
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$_GET['dir']);
        return $this->render('viewsubdocument', [
            'model' => $this->findModel($id),
            'dir' => Directory::find()->where(['id'=>$_GET['dir']])->one(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewstatus($id)
    {
        $searchModel = new MouStatusSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id,$_GET['type']);
        return $this->render('viewstatus', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewdocument($id)
    {

        $DirectoryTemp = DirectoryTemp::find()->where(['bt23'=>1])->all();
        $mouData = MouNew::find()->where(['id'=>$id])->one();
        foreach ($DirectoryTemp as $key=>$value){
            $DirectorysCheck = Directory::find()->where(['out_id'=>$id,'bt23'=>2,'directory_temp_id'=>$value->id])->one();
            if(!$DirectorysCheck){
                $Directory = new Directory();
                $Directory->out_id = $id;
                $Directory->bt23 = 2;
                $Directory->key = $value->key;
                $Directory->create_at = $mouData->create_at;
                $Directory->update_at = $mouData->create_at;
                $Directory->name = $value->title;
                $Directory->directory_temp_id = $value->id;
                $Directory->order = $value->order;
                $Directory->status = 1;
                $Directory->save();
            } else {
                $Directory = Directory::findOne($DirectorysCheck->id);
                $Directory->name = $value->title;
                $Directory->order = $value->order;
                $Directory->key = $value->key;
                $Directory->doc_type = $value->doc_type;
                $Directory->save();
            }

        }


        $searchModel = new DirectorySearch();
        $dataProvider = $searchModel->searchBymouBT(Yii::$app->request->queryParams,$id);
        return $this->render('viewdocument', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Out model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Out the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Out::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
