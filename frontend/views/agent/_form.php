<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agent-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?php
    $nati = ArrayHelper::map(\app\models\Nationality::find()->all(), 'id', 'title');
    echo $form->field($model, 'nationality_id')->dropDownList(
        $nati,
        ['prompt'=>'เลือกสัญชาติ']
    ); ?>

    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'license')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'license_file')->fileInput() ?>


    <?php if(!$model->isNewRecord) {?>
    <img src="<?php echo Url::to('@web/'.$model->license_file, true)?>" style=" width: 500px;height: auto;" >
    <?php } ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
