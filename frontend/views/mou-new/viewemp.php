<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'ลูกจ้าง '.$model->company->name." / แจ้งเข้า / ต่ออายุ ";
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">
            <p>
                <?= Html::a('เพิ่มลูกจ้าง', ['employee-inform/create', 'mou' => $model->id], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Photo',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            return '<img src="'.Url::base().'/'.$model->photo.'" class="" alt="User Image" width="100">';
                        },
                    ],
                    'code',
                    'nationality.title',
                    'first_name_en',
                    'last_name_en',
                    'passport',
                    'emp_number',
//                    'report_status',
                    //'nickname',
                    //'blood',
                    //'date_of_birth',
                    //'sex',
                    //'contact_number',
                    //'age',
                    //'email:email',
                    //'foreign_address:ntext',
                    //'photo:ntext',
                    //'passport',
                    //'passport_ext',
                    //'passport_is',
                    //'passport_ext_alert:ntext',
                    //'issue_location',
                    //'visa_is',
                    //'visa_ext',
                    //'visa_number',
                    //'visa_type',
                    //'stamped',
                    //'entrance_card',
                    //'workpermit_is',
                    //'workpermit_ext',
                    //'tt_number',
                    //'work_permit_number',
                    //'job_description',ß
                    //'position',
                    //'report_tm_date',
                    //'report_status',
                    //'mou_id',
                    //'type',
                    //'address:ntext',
                    //'company_id',
                    //'nationality_id',
                    //'prefix_id',
                    [
                        'label' => 'บต 50 อ1',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt50a1in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'"  target="_blank">บต 50 อ1</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt50a1in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 50 อ1</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt50a1in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 50 อ1</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 50 อ2',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt50a2in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 50 อ2</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt50a2in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 50 อ2</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt50a1in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 50 อ2</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'บต 48',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt48in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 48</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt48in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 48</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt48in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 48</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 30',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt30in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 30</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt30in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 30</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt30in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 30</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 31',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt31/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 31 </a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt31/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 31 </a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt31/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 31 </a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 44',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt44in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 44</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt44in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 44</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt44in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 44</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 25',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt25/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 25</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt25/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 25</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt25/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 25</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'บต 11',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt11in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 11</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt11in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 11</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt11in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 11</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'บต 46',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt46in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 46</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt46in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 46</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt46in/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">บต 46</a>';
                            }
                        },
                    ],
                    [
                        'label' => 'สัญญาจ้าง',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfemcemp/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">สัญญาจ้าง</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfemp/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">สัญญาจ้าง</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfemlemp/index', 'em_id' => $model->id, 'id' => $model->mou_new_id]).'" target="_blank">สัญญาจ้าง</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'มอบอำนาจต่างด้าว',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfemcassign/index', 'em_id' => $model->id, 'id' => $model->mou_new_id, 'type' => 2]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfemassign/index', 'em_id' => $model->id, 'id' => $model->mou_new_id, 'type' => 2]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfemlassign/index', 'em_id' => $model->id, 'id' => $model->mou_new_id, 'type' => 2]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            }
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete}</div>',
                        'urlCreator' => function( $action, $model, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['employee-inform/view', 'id' => $model->id, 'mou' => $_GET['id'], 'new' => 0]);
                            }
                            if ($action == "update") {
                                return Url::to(['employee-inform/update', 'id' => $model->id, 'mou' => $_GET['id'], 'new' => 0]);
                            }
                            if ($action == "delete") {
                                $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
                                if($user->type == 0) {
                                    return Url::to(['employee-inform/delete', 'id' => $model->id, 'mou' => $_GET['id'], 'new' => 1]);
                                }
                            }
                        }

                    ],

                ],
            ]); ?>

        </div>
        </div>
        <div class="col-md-3">
            <?php
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            $ShowPrint  = false;
            $Directory = \app\models\Directory::find()->where(['key'=>'c-doc','mou_id'=>$model->id])->one();
            if($Directory){
                if($Directory->status == 2 ){
                    $Directorys = \app\models\Directory::find()->where(['key'=>'c-slip','mou_id'=>$model->id])->one();
                    if($Directorys->status == 2 ){
                        $ShowPrint = true;
                    }
                }
            }
            if($user->type == 0){
                $ShowPrint = true;
            }

            ?>

            <?= Html::a('รายละเอียดต่อ แจ้งเข้า / ต่ออายุ', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?php if($ShowPrint) { ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?php } ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 2], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>

            <?php if($ShowPrint) { ?>
                <?= Html::a('พิมพ์เอกสาร บัญชีรายชื่อมติ 13 กค 64', ['pdfbtin2mt/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?php } ?>
            <?= Html::a('แก้ไขต่อ แจ้งเข้า / ต่ออายุ', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>


        </div>
    </div>

</div>
