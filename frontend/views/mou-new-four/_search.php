<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MouSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mou-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'company_id') ?>

    <?= $form->field($model, 'male') ?>

    <?= $form->field($model, 'female') ?>

    <?php // echo $form->field($model, 'agent_id') ?>

    <?php // echo $form->field($model, 'grantee_id') ?>

    <?php // echo $form->field($model, 'qualification_age') ?>

    <?php // echo $form->field($model, 'qualification_height') ?>

    <?php // echo $form->field($model, 'qualification_weight') ?>

    <?php // echo $form->field($model, 'preriod_year') ?>

    <?php // echo $form->field($model, 'preriod_month') ?>

    <?php // echo $form->field($model, 'wage_per_day') ?>

    <?php // echo $form->field($model, 'week_holiday') ?>

    <?php // echo $form->field($model, 'annual') ?>

    <?php // echo $form->field($model, 'holiday_id') ?>

    <?php // echo $form->field($model, 'dan_Immigration_id') ?>

    <?php // echo $form->field($model, 'nationality_id') ?>

    <?php // echo $form->field($model, 'witness_id') ?>

    <?php // echo $form->field($model, 'witness_id_two') ?>

    <?php // echo $form->field($model, 'paydate') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'print') ?>

    <?php // echo $form->field($model, 'version') ?>

    <?php // echo $form->field($model, 'document_date') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
