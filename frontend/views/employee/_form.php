<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="employee-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12" >
        <?= $form->field($model, 'photo')->fileInput() ?>

        <?php if(!$model->isNewRecord) { ?>
        <?= $form->field($model, 'code')->textInput(['maxlength' => true,'readonly'=>true,'value'=>'emp-'.$model->id]); ?>
        <?php } ?>

        <?php if(isset($_GET['mou'])) { ?>
            <div style="display: none;">
                <?= $form->field($model, 'mou_id')->textInput(['value'=>$_GET['mou']]) ?>
                <?php
                $mou = \app\models\Mou::find()->where(['id'=>$_GET['mou']])->one();
                echo $form->field($model, 'nationality_id')->textInput(['value' => $mou->nationality_id]) ;
                ?>

            </div>
        <?php } else { ?>
            <?php
            $natiC = ArrayHelper::map(\app\models\Mou::find()->all(), 'id', 'code');

            echo $form->field($model, 'mou_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกเข้า Mou'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

        <?php }  ?>


        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4>1. ข้อมูลนายจ้าง</h4>
        </div>
        <div class="col-md-12">

            <?php if(isset($_GET['mou'])) { ?>
                <div class="row" style="display: none;">
                    <?php
                    $Mou = \app\models\Mou::find()->where(['id'=>$_GET['mou']])->one();
                    echo  $form->field($model, 'company_id')->textInput(['value' => $Mou->company_id]);
                    ?>
                </div>
                <?php
                echo  $Mou->company->name;
                ?>
            <?php } else {  ?>

            <?php
            if($user->type == 0) {
                $natiC = ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name');
            } else {
                $natiC = ArrayHelper::map(\app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all(), 'id', 'name');
            }



            echo $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกนายจ้าง ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => '
                     function(e) { 
                    if($("#employee-company_id").val()){ 
                     $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#employee-company_id").val(),
            type: "get",
            dataType: "json",
        }).done(function(response) {
                console.log(response)
                var add = "เลขที่ "+response.address_no+" หมู่ "+response.moo+" ตำบล"+response.subdistricts+" อำเภอ"+response.districts+" จังหวัด"+response.province+" "+response.zipcode ;
                $("#employee-address").val(add)
                
        }).fail(function() {
            console.log("error");
            
        });
        
        } else {
              
        }
      
             }
                    ',
                ],
            ]);
            ?>

            <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

            <?php }  ?>

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <h4> 2. ข้อมูลประวัติ</h4>
        </div>

        <div class="col-md-6">
            <?php
            $jobtype = ArrayHelper::map(\app\models\JobType::find()->all(), 'title', 'title');

            echo $form->field($model, 'job_type')->dropDownList(
                $jobtype,
                ['prompt'=>'เลือกตำแหน่งหน้าที่']





            ); ?>
        </div>
        <div class="col-md-6">
            <?php
            $jobtype = ArrayHelper::map(\app\models\JobDescription::find()->all(), 'title', 'title');
            echo $form->field($model, 'job_des')->dropDownList(
                $jobtype,
                ['prompt'=>'เลือกลักษณะงาน']

            ); ?>
        </div>



        <div class="col-md-6">
            <?php
            $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
            echo $form->field($model, 'prefix_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกคำนำหน้าชื่อ']

            ); ?>
        </div>



        <div class="col-md-6">
            <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name_en')->textInput(['maxlength' => true]) ?>


        </div>


        <div class="col-md-6">

            <?php
            echo $form->field($model, 'blood')->dropDownList(
                array("O"=>"O","A"=>"A","B"=>"B","AB"=>"AB",),
                ['prompt'=>'กรุ๊ปเลือด']

            ); ?>

        </div>


        <div class="col-md-6">

            <?= $form->field($model,'date_of_birth')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

        </div>
        <div class="col-md-6">


            <?php
            echo $form->field($model, 'sex')->dropDownList(
                array(1=>"ชาย",2=>"หญิง"),
                ['prompt'=>'เลือกเพศ']

            ); ?>

        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>

        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'foreign_address')->textarea(['rows' => 6]) ?>

        </div>


    </div>






    <div class="row">
        <div class="col-md-12">
            <h4> 3 ข้อมูลเอกสารต้นทาง
            </h4>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model,'passport_ext')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>
            <?= $form->field($model,'passport_is')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'passport_ext_alert')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'issue_location')->textInput(['maxlength' => true]) ?>


        </div>
    </div>












    <div class="row">
        <div class="col-md-12">
            <h4> 4  ข้อมูลการอนุญาติเข้าประเทศ
            </h4>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'visa_number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model,'visa_is')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

            <?= $form->field($model,'visa_ext')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>
        </div>
        <div class="col-md-6">


            <?= $form->field($model, 'visa_type')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'stamped')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'entrance_card')->textInput(['maxlength' => true]) ?>
        </div>
    </div>





    <div class="row">
        <div class="col-md-12">
            <h4> 5. ข้อมูลใบอนุญาตทำงาน
            </h4>
        </div>
        <div class="col-md-6">

            <?= $form->field($model,'workpermit_is')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

            <?= $form->field($model,'workpermit_ext')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

            <?= $form->field($model, 'tt_number')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">


            <?= $form->field($model, 'work_permit_number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'job_description')->textInput(['maxlength' => true]) ?>


        </div>
    </div>






    <div class="row">
        <div class="col-md-12">
            <h4> 6. รายงานตัวกับเจ้าหน้าที่
            </h4>
        </div>
    </div>

    <?= $form->field($model,'report_tm_date')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
    ]]) ?>

    <?php

    echo $form->field($model, 'report_status')->dropDownList(
        array(1=>"ไม่ใช่",2=>"ใช่"),
        ['prompt'=>'สถานะรายงาน 24 ชม.']
    );
//
//    echo $form->field($model, 'cancle')->dropDownList(
//        array(0=>"ไม่ใช่",1=>"ใช่"),
//        ['prompt'=>'ยกเลิก.']
//    );

    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
