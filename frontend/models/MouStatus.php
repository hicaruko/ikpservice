<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mou_status".
 *
 * @property int $id
 * @property int $job_status_id
 * @property int $ref_id
 * @property int $user_id
 * @property int $type
 * @property int $status
 * @property string $create_date
 * @property string $create_at
 * @property string $detail
 * @property string $job_date
 * @property JobStatus $jobStatus
 * @property User $user

 */
class MouStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mou_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_status_id', 'ref_id', 'user_id'], 'required'],
            [['job_status_id', 'ref_id', 'user_id','type'], 'integer'],
            [['create_date', 'create_at','job_date','type','status'], 'safe'],
            [['detail'], 'string'],
            [['job_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => JobStatus::className(), 'targetAttribute' => ['job_status_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'job_status_id' => 'สถานะ',
            'ref_id' => 'Ref ID',
            'user_id' => 'ผู้ใช้งานระบบ',
            'create_date' => 'วันที่',
            'create_at' => 'วันที่สร้าง',
            'detail' => 'รายละเอียด',
            'job_date' => 'วันนัดหมาย',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->create_at = date('Y-m-d H:i:s');
                $this->create_date = date('Y-m-d');
            }
            return true;
        }
        return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefID()
    {
        if($this->type == 1) {
            return $this->hasOne(Mou::className(), ['id' => 'ref_id']);
        } else  if($this->type == 2){
            return $this->hasOne(MouNew::className(), ['id' => 'ref_id']);
        } else {
            return $this->hasOne(Out::className(), ['id' => 'ref_id']);
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobStatus()
    {
        return $this->hasOne(JobStatus::className(), ['id' => 'job_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
