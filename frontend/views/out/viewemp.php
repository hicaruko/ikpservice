<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'ลูกจ้าง '.$model->company->name." / แจ้งออก ";
$this->params['breadcrumbs'][] = ['label' => 'Demand', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">
            <p>
                <?php if(isset($_GET['type'])) {

                    if($_GET['type'] == 2){
                        echo Html::a('กลับหน้า MOU', ['mou/view', 'id' => $model->mou_id], ['class' => 'btn btn-success']);
                        echo Html::a('เพิ่มลูกจ้าง', ['employee-out/create', 'mou' => $model->id, 'type' => 2], ['class' => 'btn btn-success']);
                    } else {
                        echo Html::a('เพิ่มลูกจ้าง', ['employee-out/create', 'out' => $model->id, 'type' => 1], ['class' => 'btn btn-success']);
                    }
                    ?>

                <?php } else { ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('เพิ่มลูกจ้าง', ['employee-out/create', 'mou' => $model->id, 'type' => 1], ['class' => 'btn btn-success']) ?>

                <?php } ?>

            </p>
            <div class="table-responsive">
            <?php if($_GET['type'] == 2){ ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'employee.first_name_en',
                        'employee.last_name_en',
                        'employee.passport',
                        'employee.work_permit_number',
                        'causeOut.title',
                        'employee.nationality.title',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete}</div>',
                            'urlCreator' => function( $action, $model, $key, $index ){
                                if ($action == "view") {
                                    if(isset($_GET['type'])) {
                                        if ($_GET['type'] == 2) {
                                            return Url::to(['employee-out/view', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);

                                        } else {
                                            return Url::to(['employee-out/view', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                        }
                                    } else {
                                        return Url::to(['employee-out/view', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                    }
                                }
                                if ($action == "update") {
                                    if(isset($_GET['type'])) {
                                        if ($_GET['type'] == 2) {
                                            return Url::to(['employee-out/update', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);

                                        } else {
                                            return Url::to(['employee-out/update', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                        }
                                    } else {
                                        return Url::to(['employee-out/update', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                    }

                                }
                                if ($action == "delete") {
                                    return Url::to(['employee-out/delete', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);
                                }
                            }

                        ],
                    ],
                ]); ?>
            <?php } else { ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'first_name_en',
                        'last_name_en',
                        'passport',
                        'work_permit_number',
                        'causeOut.title',
                        'nationality.title',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete}</div>',
                            'urlCreator' => function( $action, $model, $key, $index ){
                                if ($action == "view") {
                                    if(isset($_GET['type'])) {
                                        if ($_GET['type'] == 2) {
                                            return Url::to(['employee-out/view', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);

                                        } else {
                                            return Url::to(['employee-out/view', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);
                                        }
                                    } else {
                                        return Url::to(['employee-out/view', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                    }

                                }
                                if ($action == "update") {
                                    if(isset($_GET['type'])) {
                                        if ($_GET['type'] == 2) {
                                            return Url::to(['employee-out/update', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);

                                        } else {
                                            return Url::to(['employee-out/update', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);
                                        }
                                    } else {
                                        return Url::to(['employee-out/update', 'id' => $model->id, 'out' => $_GET['id'], 'type' => $_GET['type']]);

                                    }

                                }
                                if ($action == "delete") {
                                    if ($_GET['type'] == 2) {
                                            return Url::to(['employee-out/delete', 'id' => $model->id, 'mou' => $_GET['id'], 'type' => $_GET['type']]);
                                        } else {
                                            return Url::to(['employee-out/delete', 'id' => $model->id, 'type' => $_GET['type']]);

                                    }
                                }
                            }

                        ],
                    ],
                ]); ?>
            <?php }  ?>



        </div>
        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดแจ้งออก', ['view', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 3], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งออก', ['pdfbt12out/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจ', ['companydoc3/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('แก้ไขแจ้งออก', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>
    </div>

</div>
