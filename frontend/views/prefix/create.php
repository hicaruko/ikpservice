<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prefix */

$this->title = 'Create Prefix';
$this->params['breadcrumbs'][] = ['label' => 'Prefixes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prefix-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
