<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'อัพเดทสถานะ '.$model->company->name." / แจ้งเข้า / ต่ออายุ ";
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <p>
            <?= Html::a('อัพเดทสถานะ', ['mou-status/create', 'mou' => $model->id, 'type' => 2], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'job_status_id',
                    'mou_id',
                    'user_id',
                    'create_date',
                    //'create_at',
                    //'detail:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{view} {update} {delete}</div>',
                        'urlCreator' => function( $action, $modelA, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['mou-status/view', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                            if ($action == "update") {
                                return Url::to(['mou-status/update', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                            if ($action == "delete") {
                                return Url::to(['mou-status/delete', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                        }

                    ],
//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?php
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            $ShowPrint  = false;
            $Directory = \app\models\Directory::find()->where(['key'=>'c-doc','mou_id'=>$model->id])->one();
            if($Directory){
                if($Directory->status == 2 ){
                    $Directorys = \app\models\Directory::find()->where(['key'=>'c-slip','mou_id'=>$model->id])->one();
                    if($Directorys->status == 2 ){
                        $ShowPrint = true;
                    }
                }
            }
            if($user->type == 0){
                $ShowPrint = true;
            }

            ?>

            <?= Html::a('รายละเอียดต่อ แจ้งเข้า / ต่ออายุ', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?php if($ShowPrint) { ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?php } ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 2], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>

            <?php if($ShowPrint) { ?>
                <?= Html::a('พิมพ์เอกสาร บัญชีรายชื่อมติ 13 กค 64', ['pdfbtin2mt/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

                <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?php } ?>
            <?= Html::a('แก้ไขต่อ แจ้งเข้า / ต่ออายุ', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>


        </div>

    </div>

</div>
