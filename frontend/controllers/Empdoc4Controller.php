<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;

use app\models\User;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Empdoc4Controller extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }
    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {

        $mou = MouNew::find()->where(['id'=>$_GET['id']])->one();
        $mou_user = User::find()->where(['id'=>$mou->user_id])->one();

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleLeft = "text-align: left;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";

        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/btdoc.pdf');

//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            if($pageNo ==  1){ // 15
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 69;
                $start = 93;
                $end =107;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($mou->create_at,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 69;
                $start = 116;
                $end =144;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($mou->create_at,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 69;
                $start = 150;
                $end =168;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($mou->create_at,'year').'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 101;
                $start = 59;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 109;
                $start = 28;
                $end =67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->phone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

  $y1 = 109;
                $start = 154;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 210;
                $start = 73;
                $end =141;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 210;
                $start = 156;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 217;
                $start = 33;
                $end =100;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 242;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 265;
                $start = 44;
                $end =82;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 265;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


            }  // 15



        }

//
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 90, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 129, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  50, 1, 90, 'auto');
//
//        }
//$paydate
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//        }
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//        }


//        $ttt = htmlentities("ใช้ดำเนินกา แจ้งคนงานต่างด้าวเข้าทำงาน <br/>
//ตามเอกสารแจ้งเข้าแรงงาน เลขที่ LE20070024   <br/>
//ให้กับนายจ้าง บริษัท xxxxxx   <br/>
//โดยมอบผู้รับมอบอำนาจดำเนินการ ชื่อ xxxxx");
//        $wm = \Mpdf\Utils\UtfString::strcode2utf($ttt);

//

//        $pdf->AddPage();
//        $pdf->Image(Yii::$app->basePath.'/web/doc/nj-17.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
//        $y1 = 137;
//        $start = 48;
//        $end =179;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 117;
//        $end =125;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->number_price.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 161;
//        $end =183;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//        $y1 = 151;
//        $start = 30;
//        $end =175;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$this->Convert($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 238;
//        $start = 39;
//        $end =85;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
