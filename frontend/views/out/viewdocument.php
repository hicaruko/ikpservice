<?php

//use yii\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'เอกสารประกอบการ '.$model->company->name." / แจ้งเข้า / ต่ออายุ";
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$mouDat = $model;
$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
if($user->type == 0) {
}
?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">
            <div class="table-responsive">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name:ntext',
                    [
                        'label' => 'Open',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            return Html::a('เปิดโฟลเดอร์', ['viewsubdocument','id' => $_GET['id'],'dir' => $model->id], ['class' => 'btn btn-success']);

                        },
                    ],
                    'update_at',
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'status',
                        'format'=>'raw',
                        'visible'=>($user->type == 0),
                        'editableOptions' => [
                            'header' => 'Status',
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => [1 => 'wait approve',2 => 'approve'],
                            'options' => ['class'=>'form-control', 'prompt'=>'Select status...'],
                            'displayValueConfig'=> [
                                1 => '<i class="glyphicon glyphicon-hourglass"></i> wait approve',
                                2 => '<i class="glyphicon glyphicon-flag"></i> approve',
                            ],
                            'formOptions' => [
                                'action' => \yii\helpers\Url::to([ 'directory/update-status' ])
                            ]

                        ],
                    ],
//                    [
//                        'class' => 'yii\grid\ActionColumn',
//                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update}</div>',
//                        'urlCreator' => function( $action, $model, $key, $index ){
//                            if ($action == "view") {
//                                return Url::to(['employee/view', 'id' => $model->id]);
//
//                            }
//                            if ($action == "update") {
//                                return Url::to(['employee/update', 'id' => $model->id, 'mou' => $_GET['id']]);
//
//                            }
////                            if ($action == "delete") {
////                                return Url::to(['employee/delete', 'id' => $model->id]);
////                            }
//                        }
//
//                    ],
                    //'mou_id',

//                    ['class' => 'yii\grid\ActionColumn',
//                        'buttonOptions'=>['class'=>'btn btn-default'],
//                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view}</div>',
//                        'options'=> ['style'=>'width:200px;'],
//                    ],

//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดแจ้งออก', ['view', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 3], ['class' => 'btn btn-primary btn-block']) ?>



            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งออก', ['pdfbt12out/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจ', ['companydoc3/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <br>
            <?= Html::a('แก้ไขแจ้งออก', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
