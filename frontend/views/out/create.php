<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Out */

$this->title = 'สร้างแจ้งออก';
$this->params['breadcrumbs'][] = ['label' => 'รายการ แจ้งออก', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="out-create">

    <?php if($_GET['type'] == 1) { ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php } else { ?>
        <?= $this->render('_formmou', [
            'model' => $model,
        ]) ?>
    <?php }  ?>

</div>
