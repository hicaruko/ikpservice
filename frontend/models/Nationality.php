<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nationality".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $url
 * @property string $bt23
 *
 * @property Mou[] $mous
 */
class Nationality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nationality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'title_en'], 'required'],
            [['title', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'สัญชาติ',
            'title_en' => 'Title En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMous()
    {
        return $this->hasMany(Mou::className(), ['nationality_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return NationalityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NationalityQuery(get_called_class());
    }
}
