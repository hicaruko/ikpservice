<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="company-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'name',
            'name_en',
            'address_no',
            'moo',
            'soi',
            'road',
            'telephone',
            'fax',
            'business_type_id',
            'type',
            'first_name',
            'first_name_en',
            'last_name',
            'last_name_th',
            'position',
            'position_en',
            'prefix_id',
            'provinces_id',
            'districts_id',
            'subdistricts_id',
            'license',
            'license_en',
            'soi_n',
            'road_en',
            'province',
            'province_en',
            'districts',
            'districts_en',
            'subdistricts',
            'subdistricts_en',
            'zipcode',
        ],
    ]) ?>

</div>
