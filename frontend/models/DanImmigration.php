<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dan_Immigration".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 */
class DanImmigration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dan_Immigration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'title_en'], 'required'],
            [['title', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_en' => 'Title En',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DanImmigrationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DanImmigrationQuery(get_called_class());
    }
}
