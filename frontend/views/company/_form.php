<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\typeahead\Typeahead;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */


$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}

$user = \app\models\User::find()->where(['id' => Yii::$app->user->getId()])->one();


?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>
    <h4>ข้อมูลนายจ้าง</h4>

    <div class="row">

        <div class="col-md-6">

            <?php if($user->type == 0) { ?>

                <?php
                echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $datalists,
                    'options' => ['placeholder' => 'เลือกผู้รับผิดชอบ ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            <?php } ?>
            <?php

            echo $form->field($model, 'type')->dropDownList(
                array(1=>"นิติบุคคล",2=>"บุคคลธรรมดา"),
                [
                    'prompt'=>'ประเภท',
                    'onChange'=>'
                    if(this.value == 2){
                    $(".license_detail").hide()
                    } else {
                    $(".license_detail").show()
                    }
                    '
                ]
            );

            ?>
            <?php
            $natiC = ArrayHelper::map(\app\models\BusinessType::find()->all(), 'id', 'title');
            echo $form->field($model, 'business_type_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกประเภทธุรกิจ ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
             ?>

            <?php
            $jobtype = ArrayHelper::map(\app\models\JobDescription::find()->all(), 'title', 'title');
            echo $form->field($model, 'job_description')->dropDownList(
                $jobtype,
                ['prompt'=>'----- เลือกลักษณะงาน ----- ']

            ); ?>


        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

        </div>

        <?php if($user->type == 0) { ?>
            <div class="col-md-12">
                <?= $form->field($model, 'branch')->textInput(['maxlength' => true]) ?>
            </div>
        <?php } ?>



    </div>
    <h4>ข้อมูลที่อยู่นายจ้าง</h4>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'address_no')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'moo')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'soi')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'soi_n')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'road')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'road_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>


    <div class="row">
        <div class="col-md-6">

            <?php
            $template = '<div>' .
                '<p class="repo-name">{{value}}</p>' .
                '</div>';

            echo $form->field($model, 'subdistricts')->widget(Typeahead::classname(), [

                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('label')",
                        'display' => 'label',
                        'remote' => [
                            'url' => Url::to(['company/country-list']) . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ],'templates' => [
                        'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                        'suggestion' => new JsExpression("Handlebars.compile('{$template}')"),

                    ]

                    ]
                ],

                'options' => ['placeholder' => 'พิมพ์เพื่อค้นหาตำบล ...','hint' => false , 'highlight' => true ],
                'pluginOptions' => [ 'hint' => false , 'highlight' => true ],
                'pluginEvents' => [
    'typeahead:active' => 'function() { console.log("typeahead:active"); }',
    'typeahead:idle' => 'function() { console.log("typeahead:idle"); }',
    'typeahead:open' => 'function() { console.log("typeahead:open"); }',
    'typeahead:close' => 'function() { console.log("typeahead:close"); }',
    'typeahead:change' => 'function() { console.log("typeahead:change"); }',
    'typeahead:render' => 'function() { console.log("typeahead:render"); }',
    'typeahead:select' => 'function(ev, suggestion) { 
      console.log(suggestion); 
      $("#company-subdistricts_en").val(suggestion.name_in_english)
      $("#company-districts_en").val(suggestion.districts_name_in_english)
      $("#company-districts").val(suggestion.districts_name_in_thai)
      $("#company-province_en").val(suggestion.provinces_name_in_english)
      $("#company-province").val(suggestion.provinces_name_in_thai)
      $("#company-zipcode").val(suggestion.zip_code) 

     }',
    'typeahead:autocomplete' => 'function() { console.log("typeahead:autocomplete"); }',
    'typeahead:cursorchange' => 'function() { console.log("typeahead:cursorchange"); }',
    'typeahead:asyncrequest' => 'function() { console.log("typeahead:asyncrequest"); }',
    'typeahead:asynccancel' => 'function() { console.log("typeahead:asynccancel"); }',
    'typeahead:asyncreceive' => 'function() { console.log("typeahead:asyncreceive"); }',
                ],

            ]);

            ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'subdistricts_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'districts')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'districts_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'province_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'province_area')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

        </div>
    </div>


    <h4>บุคคลที่สามารถลงลายมือชื่อได้</h4>
    <?php

    echo $form->field($model, 'number_ceo')->dropDownList(
        array(1=>1,2=>2),
        [
            'prompt'=>'ประเภท',
            'onChange'=>'
                    if(this.value == 2){
                    $(".p1").show()
                    $(".p2").show()

                    } else {
                      $(".p1").show()
                      $(".p2").hide()
                    }
                    '
        ]
    );

    $p1 = "";
    $p2 = "none";
    if($model->number_ceo == 2){
        $p1 = "";
        $p2 = "";
    }
    ?>



    <div class="row p1" style="display: <?php echo $p1?>;">
        <div class="col-md-12">
            <?php
            $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
            echo $form->field($model, 'prefix_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกคำนำหน้าชื่อ']

            ); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row p1" style="display: <?php echo $p1?>;">
        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name_th')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row p2"  style="display: <?php echo $p2?>;">
        <div class="col-md-12">
            <?php
            $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
            echo $form->field($model, 'prefix_id1')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกคำนำหน้าชื่อ']

            ); ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'first_name2')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'first_name2_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row p2" style="display: <?php echo $p2?>;">
        <div class="col-md-6">
            <?= $form->field($model, 'last_name2')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name2_en')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row" >
        <div class="col-md-6">
            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'position_en')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?php
    $display = "none";
    if($model->type == 1) {
        $display = "";
    }
    ?>
    <div class="row " style="">
        <div class="col-md-6">
            <?= $form->field($model, 'license')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row license_detail" style="display:<?php echo $display?> ">
        <div class="col-md-6">
            <?= $form->field($model,'license_date')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

            <?= $form->field($model, 'license_price')->textInput(['maxlength' => true]) ?>

        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
