<?php

use kartik\file\FileInput;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */


$this->title = 'โฟลเดอร์  '.$dir->name;
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="mou-view">

    <div class="row">
        <div class="col-md-9">
            <div class="documents-form">


                <?php

                $doc = \app\models\Documents::find()->where(['directory_id'=>$dir->id])->all();
                $doc_list = array();
                $doc_config = array();
                foreach ($doc as $value){

                    $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
                    array_push($doc_list,Url::to('@web/'.$value->file, true));
                    if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                        array_push($doc_config,array(
                            'caption'=>$value->file,
                            'type'=>'image',
                            'url' => Url::to(['documents/delete','id'=>$value->id,'mou'=>$model->id,'dir'=>$dir->id]),
                            'key'=>$value->id,
                        ));
                    } else {
                        array_push($doc_config,array(
                            'caption'=>$value->file,
                            'type'=>$ext.'',
                            'url' => Url::to(['documents/delete','id'=>$value->id,'mou'=>$model->id,'dir'=>$dir->id]),
                            'key'=>$value->id,

                        ));
                    }

                }

                ActiveForm::begin([
                    'options'=>['enctype'=>'multipart/form-data'] // important
                ]);
                echo FileInput::widget([
                    'name'=>'input_file',
                    'options' => ['multiple' => true],

                    'pluginOptions' => [
                        'previewFileType' => 'any',
                        'initialPreviewFileType' => 'image',
                        'uploadUrl' => Url::toRoute(["documents/upload-file-new",'id'=>$dir->id,'mou'=>$model->id]),
                        'initialPreview'=>$doc_list,
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>false,
                        'initialPreviewConfig'=>$doc_config,
                        'showCaption' => true,
                        'showPreview' => true,

//                         'purifyHtml'=> true,

                    ],
                    'pluginEvents'=>[
                        "filebatchuploadcomplete" => "function() { window.location.reload(true);
                            }",
                    ]
                ]);

                ActiveForm::end();

                ?>

            </div>
            <br>

            <br>
            <br>

        </div>
        <div class="col-md-3">
            <?php
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            $ShowPrint  = false;
            $Directory = \app\models\Directory::find()->where(['key'=>'c-doc','mou_id'=>$model->id])->one();
            if($Directory){
                if($Directory->status == 2 ){
                    $Directorys = \app\models\Directory::find()->where(['key'=>'c-slip','mou_id'=>$model->id])->one();
                    if($Directorys->status == 2 ){
                        $ShowPrint = true;
                    }
                }
            }
            if($user->type == 0){
                $ShowPrint = true;
            }

            ?>

            <?= Html::a('รายละเอียดต่อ แจ้งเข้า / ต่ออายุ', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?php if($ShowPrint) { ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?php } ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 2], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>

            <?php if($ShowPrint) { ?>
                <?= Html::a('พิมพ์เอกสาร บัญชีรายชื่อมติ 13 กค 64', ['pdfbtin2mt/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

                <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
                <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc2/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?php } ?>
            <?= Html::a('แก้ไขต่อ แจ้งเข้า / ต่ออายุ', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>



        </div>
    </div>

</div>
