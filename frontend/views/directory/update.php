<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directory */

$this->title = 'Update Directory: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Directories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directory-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
