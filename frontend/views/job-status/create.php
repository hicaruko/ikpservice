<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobStatus */

$this->title = 'Create Job Status';
$this->params['breadcrumbs'][] = ['label' => 'Job Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-status-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
