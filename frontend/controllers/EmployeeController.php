<?php

namespace frontend\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();

        if ($model->load(Yii::$app->request->post())) {
            if(isset($_GET['mou'])){
                $model->mou_id =  $_GET['mou'];
            }
            $model->code="emp-".$model->id;
            $photo  = UploadedFile::getInstance($model, 'photo');
            $path = $this->getUploadPath();
            $pathUrl = $this->getUploadUrl();
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->photo = 'employee/'.$fileName;
                    $model->save();
                    if(isset($_GET['mou'])){
                        return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                } else {
                    $model->save();
                    if(isset($_GET['mou'])){
                        return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }                }
            } else {
                $model->save();
                if(isset($_GET['mou'])){
                    return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                } else {
                    return $this->redirect(['view', 'id' => $model->id]);

                }
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/employee/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/employee/';
    }
    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPath = $model->photo;
        if ($model->load(Yii::$app->request->post())) {
            $photo  = UploadedFile::getInstance($model, 'photo');
            $path = $this->getUploadPath();
            $pathUrl = $this->getUploadUrl();
            $model->code="emp-".$model->id;
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->photo = 'employee/'.$fileName;

                    $model->save();
                    if(isset($_GET['mou'])){
                        return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                } else {
                    $model->photo = $oldPath;
                    $model->save();
                    if(isset($_GET['mou'])){
                        return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                }
            } else {
                $model->photo = $oldPath;
                $model->save();
                if(isset($_GET['mou'])){
                    return $this->redirect(['mou/viewemp', 'id' => $model->mou_id]);
                } else {
                    return $this->redirect(['view', 'id' => $model->id]);

                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if(isset($_GET['mou'])){
            return $this->redirect(['mou/viewemp', 'id' => $_GET['mou']]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
