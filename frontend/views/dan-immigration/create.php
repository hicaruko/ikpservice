<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DanImmigration */

$this->title = 'Create Dan Immigration';
$this->params['breadcrumbs'][] = ['label' => 'Dan Immigrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dan-immigration-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
