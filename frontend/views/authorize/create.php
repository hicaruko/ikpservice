<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Authorize */

$this->title = 'ผู้รับมอบอำนาจ';
$this->params['breadcrumbs'][] = ['label' => 'ผู้รับมอบอำนาจ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authorize-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
