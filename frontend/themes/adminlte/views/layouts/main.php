<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>



        <link rel="apple-touch-icon" sizes="57x57" href="/frontend/web/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/frontend/web/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/frontend/web/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/frontend/web/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/frontend/web/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/frontend/web/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/frontend/web/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/frontend/web/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/frontend/web/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/frontend/web/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/frontend/web/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/frontend/web/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/frontend/web/favicon-16x16.png">
        <link rel="manifest" href="/frontend/web/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

<!--        <link rel="manifest" href="/manifest.json">-->

        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase-app.js"></script>

        <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase-messaging.js"></script>

        <!-- TODO: Add SDKs for Firebase products that you want to use
             https://firebase.google.com/docs/web/setup#available-libraries -->
        <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase-analytics.js"></script>
        <script>
            // Your web app's Firebase configuration
            // For Firebase JS SDK v7.20.0 and later, measurementId is optional
            var firebaseConfig = {
                apiKey: "AIzaSyAzKKifW-uYj3_P37sX5LHWTnkeEd7KBJw",
                authDomain: "ikpservice-62544.firebaseapp.com",
                databaseURL: "https://ikpservice-62544-default-rtdb.firebaseio.com",
                projectId: "ikpservice-62544",
                storageBucket: "ikpservice-62544.appspot.com",
                messagingSenderId: "1004205395694",
                appId: "1:1004205395694:web:d3814c8366c79523eaedff",
                measurementId: "G-2BQZLHMFC6"
            };
            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);
            firebase.analytics();
        </script>


    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">


        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>



    <?php $this->endBody() ?>
    <script>
        // Retrieve Firebase Messaging object.
        var messaging = firebase.messaging();

        function subscribeFCM() {
            messaging.requestPermission()
                .then(function() {
                    console.log('Notification permission granted.');

                    //


                    showToken();
                    // TODO(developer): Retrieve a Instance ID token for use with FCM.
                    // …
                })
                .catch(function(err) {
                    console.log('Unable to get permission to notify. ', err);
                });
        }

        function showToken() {
            // Get Instance ID token. Initially this makes a network call, once retrieved
            // subsequent calls to getToken will return from cache.
            messaging.getToken()
                .then(function(currentToken) {
                    if (currentToken) {
                        $.ajax({
                            type: "POST",
                            data: {user_id:<?php echo Yii::$app->user->getId()?>,device_token:currentToken},
                            url: "<?php echo Yii::$app->request->baseUrl?>/index.php/api/device",
                            success: function(data, status){
                                console.log("Data: " + data + "\nStatus: " + status);
                            }
                        });
                        console.log("currentToken", currentToken);
                        // $("#token").html(currentToken);
                    } else {
                        // Show permission request.
                        console.log('No Instance ID token available. Request permission to generate one.');
                        // $("#token").html("foo");
                    }
                })
                .catch(function(err) {
                    console.log('An error occurred while retrieving token. ', err);
                });
        }
        messaging.onTokenRefresh(function() {
            messaging.getToken()
                .then(function(refreshedToken) {
                    console.log('Token refreshed.');
                    // $("#token").html(refreshedToken);
                })
                .catch(function(err) {
                    console.log('Unable to retrieve refreshed token ', err);
                });
        });

        subscribeFCM();
        // // IDs of divs that display registration token UI or request permission UI.
        // const tokenDivId = 'token_div';
        // const permissionDivId = 'permission_div';
        //
        // // Handle incoming messages. Called when:
        // // - a message is received while the app has focus
        // // - the user clicks on an app notification created by a service worker
        // //   `messaging.onBackgroundMessage` handler.
        // messaging.onMessage((payload) => {
        //     console.log('Message received. ', payload);
        //     // Update the UI to include the received message.
        //     appendMessage(payload);
        // });
        //
        // function resetUI() {
        //     clearMessages();
        //     showToken('loading...');
        //     // Get registration token. Initially this makes a network call, once retrieved
        //     // subsequent calls to getToken will return from cache.
        //     messaging.getToken({vapidKey: 'BLzI7tjhkHdKJvz6fbWYbt0KHHdvvVorCNwsMV1xJHw8Tbe8SzptUVdPixYxnxLzQThLVKS_iAL3MrBDhq0ZmSY'}).then((currentToken) => {
        //         if (currentToken) {
        //             sendTokenToServer(currentToken);
        //             updateUIForPushEnabled(currentToken);
        //         } else {
        //             // Show permission request.
        //             console.log('No registration token available. Request permission to generate one.');
        //             // Show permission UI.
        //             updateUIForPushPermissionRequired();
        //             setTokenSentToServer(false);
        //         }
        //     }).catch((err) => {
        //         console.log('An error occurred while retrieving token. ', err);
        //         showToken('Error retrieving registration token. ', err);
        //         setTokenSentToServer(false);
        //     });
        // }
        //
        //
        // function showToken(currentToken) {
        //     // Show token in console and UI.
        //     const tokenElement = document.querySelector('#token');
        //     tokenElement.textContent = currentToken;
        // }
        //
        // // Send the registration token your application server, so that it can:
        // // - send messages back to this app
        // // - subscribe/unsubscribe the token from topics
        // function sendTokenToServer(currentToken) {
        //     if (!isTokenSentToServer()) {
        //         console.log('Sending token to server...');
        //         // TODO(developer): Send the current token to your server.
        //         setTokenSentToServer(true);
        //     } else {
        //         console.log('Token already sent to server so won\'t send it again ' +
        //             'unless it changes');
        //     }
        // }
        //
        // function isTokenSentToServer() {
        //     return window.localStorage.getItem('sentToServer') === '1';
        // }
        //
        // function setTokenSentToServer(sent) {
        //     window.localStorage.setItem('sentToServer', sent ? '1' : '0');
        // }
        //
        // function showHideDiv(divId, show) {
        //     const div = document.querySelector('#' + divId);
        //     if (show) {
        //         div.style = 'display: visible';
        //     } else {
        //         div.style = 'display: none';
        //     }
        // }
        //
        // function requestPermission() {
        //     console.log('Requesting permission...');
        //     Notification.requestPermission().then((permission) => {
        //         if (permission === 'granted') {
        //             console.log('Notification permission granted.');
        //             // TODO(developer): Retrieve a registration token for use with FCM.
        //             // In many cases once an app has been granted notification permission,
        //             // it should update its UI reflecting this.
        //             resetUI();
        //         } else {
        //             console.log('Unable to get permission to notify.');
        //         }
        //     });
        // }
        //
        // function deleteToken() {
        //     // Delete registration token.
        //     messaging.getToken().then((currentToken) => {
        //         messaging.deleteToken(currentToken).then(() => {
        //             console.log('Token deleted.');
        //             setTokenSentToServer(false);
        //             // Once token is deleted update UI.
        //             resetUI();
        //         }).catch((err) => {
        //             console.log('Unable to delete token. ', err);
        //         });
        //     }).catch((err) => {
        //         console.log('Error retrieving registration token. ', err);
        //         showToken('Error retrieving registration token. ', err);
        //     });
        // }
        //
        // // Add a message to the messages element.
        // function appendMessage(payload) {
        //     const messagesElement = document.querySelector('#messages');
        //     const dataHeaderElement = document.createElement('h5');
        //     const dataElement = document.createElement('pre');
        //     dataElement.style = 'overflow-x:hidden;';
        //     dataHeaderElement.textContent = 'Received message:';
        //     dataElement.textContent = JSON.stringify(payload, null, 2);
        //     messagesElement.appendChild(dataHeaderElement);
        //     messagesElement.appendChild(dataElement);
        // }
        //
        // // Clear the messages element of all children.
        // function clearMessages() {
        //     const messagesElement = document.querySelector('#messages');
        //     while (messagesElement.hasChildNodes()) {
        //         messagesElement.removeChild(messagesElement.lastChild);
        //     }
        // }
        //
        // function updateUIForPushEnabled(currentToken) {
        //     showHideDiv(tokenDivId, true);
        //     showHideDiv(permissionDivId, false);
        //     showToken(currentToken);
        // }
        //
        // function updateUIForPushPermissionRequired() {
        //     showHideDiv(tokenDivId, false);
        //     showHideDiv(permissionDivId, true);
        // }
        //
        // resetUI();
    </script>

    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
