<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'Create Demand';
$this->params['breadcrumbs'][] = ['label' => 'Demand', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mou-create">


    <?= $this->render('_form', [
        'model' => $model,
        'isUpdate' => 0,

    ]) ?>

</div>
