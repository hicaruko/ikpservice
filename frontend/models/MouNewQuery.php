<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MouNew]].
 *
 * @see Mou
 */
class MouNewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MouNew[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MouNew|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
