<?php

namespace frontend\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MouNew;

/**
 * MouNewSearch represents the model behind the search form of `app\models\MouNew`.
 */
class MouNewSearch extends MouNew
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'male', 'female', 'preriod_year', 'preriod_month', 'wage_per_day', 'week_holiday', 'annual', 'holiday_id', 'nationality_id', 'witness_id', 'witness_id_two', 'print', 'version', 'user_id'], 'integer'],
            [['code', 'qualification_age', 'qualification_height', 'qualification_weight', 'paydate', 'create_at', 'update_date', 'document_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MouNew::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $user_login = User::findOne(Yii::$app->user->id);

        if($user_login){
            if($user_login->type != 0){
                $query->andFilterWhere([
                    'id' => $this->id,
                    'company_id' => $this->company_id,
                    'male' => $this->male,
                    'female' => $this->female,
                    'preriod_year' => $this->preriod_year,
                    'preriod_month' => $this->preriod_month,
                    'wage_per_day' => $this->wage_per_day,
                    'week_holiday' => $this->week_holiday,
                    'annual' => $this->annual,
                    'holiday_id' => $this->holiday_id,
                    'nationality_id' => $this->nationality_id,
                    'witness_id' => $this->witness_id,
                    'witness_id_two' => $this->witness_id_two,
                    'create_at' => $this->create_at,
                    'update_date' => $this->update_date,
                    'print' => $this->print,
                    'version' => $this->version,
                    'document_date' => $this->document_date,
                    'user_id' => Yii::$app->user->id,
                ]);
            } else {
                $query->andFilterWhere([
                    'id' => $this->id,
                    'company_id' => $this->company_id,
                    'male' => $this->male,
                    'female' => $this->female,
                    'preriod_year' => $this->preriod_year,
                    'preriod_month' => $this->preriod_month,
                    'wage_per_day' => $this->wage_per_day,
                    'week_holiday' => $this->week_holiday,
                    'annual' => $this->annual,
                    'holiday_id' => $this->holiday_id,
                    'nationality_id' => $this->nationality_id,
                    'witness_id' => $this->witness_id,
                    'witness_id_two' => $this->witness_id_two,
                    'create_at' => $this->create_at,
                    'update_date' => $this->update_date,
                    'print' => $this->print,
                    'version' => $this->version,
                    'document_date' => $this->document_date,
                ]);
            }
        } else {
            $query->andFilterWhere([
                'id' => $this->id,
                'company_id' => $this->company_id,
                'male' => $this->male,
                'female' => $this->female,
                'preriod_year' => $this->preriod_year,
                'preriod_month' => $this->preriod_month,
                'wage_per_day' => $this->wage_per_day,
                'week_holiday' => $this->week_holiday,
                'annual' => $this->annual,
                'holiday_id' => $this->holiday_id,
                'nationality_id' => $this->nationality_id,
                'witness_id' => $this->witness_id,
                'witness_id_two' => $this->witness_id_two,
                'create_at' => $this->create_at,
                'update_date' => $this->update_date,
                'print' => $this->print,
                'version' => $this->version,
                'document_date' => $this->document_date,
            ]);
        }
        // grid filtering conditions

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'qualification_age', $this->qualification_age])
            ->andFilterWhere(['like', 'qualification_height', $this->qualification_height])
            ->andFilterWhere(['like', 'qualification_weight', $this->qualification_weight])
            ->andFilterWhere(['like', 'paydate', $this->paydate]);

        return $dataProvider;
    }
}
