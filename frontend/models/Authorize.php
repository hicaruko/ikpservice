<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authorize".
 *
 * @property int $id
 * @property int $user_id
 * @property int $mou_id
 * @property int $type
 * @property int $witness_id
 * @property int $witness_id_two
 * @property string $create_at
 *
 * @property User $user
 *
 * @property Witness $witness
 * @property Witness $witnessIdTwo
 */
class Authorize extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authorize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'mou_id', 'type'], 'integer'],
            [['witness_id', 'witness_id_two'], 'required'],
            [['create_at'], 'safe'],
            [['witness_id'], 'exist', 'skipOnError' => true, 'targetClass' => Witness::className(), 'targetAttribute' => ['witness_id' => 'id']],
            [['witness_id_two'], 'exist', 'skipOnError' => true, 'targetClass' => Witness::className(), 'targetAttribute' => ['witness_id_two' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->create_at = date('Y-m-d H:i:s');

            } else {
            }
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ผู้รับมิบอำนาจ',
            'mou_id' => 'Mou ID',
            'type' => 'Type',
            'witness_id' => 'พยานคนที่ 1',
            'witness_id_two' => 'พยานคนที่ 2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitness()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitnessIdTwo()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id_two']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
