<?php

namespace frontend\controllers;

use app\models\Districts;
use app\models\Mou;
use app\models\Provinces;
use app\models\Subdistricts;
use Yii;
use app\models\Company;
use frontend\models\CompanySearch;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCountryList($q = null) {
        ini_set("memory_limit",-1);
        defined('YII_DEBUG') or define('YII_DEBUG', false);
        header('Content-Type: application/json');
        ini_set("memory_limit",-1);

        $query = new Query;

        $query->select('*')
            ->from('subdistricts')
            ->where('name_in_thai LIKE "%' . $q .'%"')
            ->limit(10)
            ->orderBy('name_in_thai');
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $Districts = Districts::find()->where(['id'=>$d['district_id']])->one();
            if($Districts){
                $Provinces = Provinces::find()->where(['id'=>$Districts->province_id])->one();
                if($Provinces) {
                    $Subdistricts = Subdistricts::find()->where(['id'=>intval($d['id'])])->one();

                    array_push($out, array(
                        'value' => $d['name_in_thai'] . ' | ' . $Districts->name_in_thai . ' | ' .$Subdistricts->name_in_english. $Provinces->name_in_thai . ' | ' . $d['zip_code'],
                        'id' => $d['id'],
                        'label' => $Subdistricts->name_in_thai,
                        'districts_name_in_english' => $Districts->name_in_english,
                        'districts_name_in_thai' => $Districts->name_in_thai,
                        'provinces_name_in_english' => $Provinces->name_in_english,
                        'provinces_name_in_thai' => $Provinces->name_in_thai,
                        'zip_code' => $d['zip_code'],
                        'name_in_english' => $Subdistricts->name_in_english,
                    ));
                }
            }


        }

//        $outs['results'] = array_values($out);

        echo Json::encode($out);
//        echo Json::encode($outs);
    }


    public function  actionGetCompany() {
        header('Content-Type: application/json');
        $model = Company::findOne($_GET['id']);
        echo Json::encode($model);

    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();

        if ($model->load(Yii::$app->request->post())) {
            $mou = Company::find()->select(['id'=>'MAX(`id`)'])->one();
            if($mou){
                $model->code = $model->zipcode."-".date('Ymd').'-'.($mou->id+1);
            } else {
                $model->code = $model->zipcode."-".date('Ymd').'-'.($mou->id+1);
            }
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelOldCode = $model->zipcode;
        if ($model->load(Yii::$app->request->post()) ) {

            if($modelOldCode == ""){
                $model->code = $model->zipcode."".$model->code;
            }


            $model->save();
            return $this->redirect(['index']);;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
