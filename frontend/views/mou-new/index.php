<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MouSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แจ้งเข้า / ต่ออายุ';
$this->params['breadcrumbs'][] = $this->title;

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
$ActionColumn = ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update}'];
if($user->type == 0) {
    $ActionColumn = ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update} {delete}'];
}
$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}
?>
<div class="mou-index  ">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('สร้าง แจ้งเข้า / ต่ออายุ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'document_date',
            'code',
            [
                'attribute'=>'company_id',
                'filter'=>ArrayHelper::map(\app\models\Company::find()->asArray()->all(), 'id', 'name'),
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    return $model->company->name;
                },
            ],
            'nationality.title',
            'update_date',

//            '',
            //'agent_id',
            //'grantee_id',
            //'qualification_age',
            //'qualification_height',
            //'qualification_weight',
            //'preriod_year',
            //'preriod_month',
            //'wage_per_day',
            //'week_holiday',
            //'annual',
            //'holiday_id',
            //'dan_Immigration_id',
            //'nationality_id',
            //'witness_id',
            //'witness_id_two',
            //'paydate',
            //'create_at',
            //'update_date',
            //'print',
            //'version',
            //'document_date',
            //'user_id',
            [
                'label' => 'Create By',
                'attribute'=>'user_id',
                'headerOptions' => ['width' => '80'],
                'filter'=>$datalists,
                'value' => function($model, $key, $index)
                {
                    $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                    if($datas){
                        return $datas->first_name." ".$datas->last_name;
                    }


                },
            ],
            $ActionColumn,
        ],
    ]); ?>
</div>
</div>
