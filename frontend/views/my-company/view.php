<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MyCompany */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'My Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="my-company-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'name_en',
            'address_no',
            'soi',
            'soi_en',
            'road',
            'road_en',
            'districts',
            'districts_en',
            'sub_ districts',
            'sub_ districts_en',
            'province',
            'province_en',
            'phone',
            'license',
            'first_name',
            'first_name_en',
            'last_name',
            'last_name_en',
            'position',
            'position_en',
            'prefix_id',
        ],
    ]) ?>

</div>
