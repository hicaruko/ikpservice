<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobType */

$this->title = 'Create ประเภทงาน';
$this->params['breadcrumbs'][] = ['label' => 'ประเภทงาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
