
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NationalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แจ้งเตือน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nationality-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'title',
            'create_at',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
