<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directory_temp".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 * @property int $order
 * @property int $bt23
 * @property string $key
 *
 * @property Directory[] $directories
 */
class DirectoryTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directory_temp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'order'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'order' => 'Order',
            'bt23' => 'bt23',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectories()
    {
        return $this->hasMany(Directory::className(), ['directory_temp_id' => 'id']);
    }
}
