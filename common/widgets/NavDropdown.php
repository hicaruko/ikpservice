<?php

namespace common\widgets;

use yii\bootstrap\Dropdown;
use yii\bootstrap\Html;

/**
 * Description of NavDropdown
 *
 * @author Leda Ferreira <ledat.ferreira@gmail.com>
 */
class NavDropdown extends Dropdown
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Html::removeCssClass($this->options, ['widget' => 'dropdown-menu']);
        Html::addCssClass($this->options, ['widget' => 'treeview-menu']);
    }
}
