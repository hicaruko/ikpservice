<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\WitnessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Witnesses';
$this->params['breadcrumbs'][] = $this->title;

$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}

?>
<div class="witness-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Witness', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'first_name_en',
            'last_name',
            'last_name_en',
            //'prefix_id',
            [
                'label' => 'Create By',
                'attribute'=>'user_id',
                'headerOptions' => ['width' => '80'],
                'filter'=>$datalists,
                'value' => function($model, $key, $index)
                {
                     $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                     if($datas){
                         return $datas->first_name." ".$datas->last_name;
                     }


                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
