<?php

namespace frontend\controllers;

use kartik\mpdf\Pdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
class PdfsController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function actionIndex()
    {
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre33.pdf');
//
//        Settings::setTempDir(Yii::$app->basePath.'/web/doc/tmp/'); //Path ของ Folder temp ที่สร้างเอาไว้
        $templateProcessor = new TemplateProcessor(Yii::$app->basePath.'/web/doc/Doc2.docx'); //Path ของ template ที่สร้างเอาไว้
        $templateProcessor->setValue('test', 'testtesttesttesttest');
        $templateProcessor->saveAs(Yii::$app->basePath.'/web/doc/Doc12345678.docx'); //กำหนด Path ที่จะสร้างไฟล์
//

        Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
//      Settings::setPdfRendererPath( '/Applications/MAMP/htdocs/ikpservice/vendor/Mpdf/Mpdf');
        Settings::setPdfRendererPath( '/Applications/MAMP/htdocs/ikpservice/vendor/Mpdf/Mpdf');
//        $phpWords = \PhpOffice\PhpWord\IOFactory::load(Yii::$app->basePath.'/web/doc/Doc2.docx', 'Word2007');
//        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWords , 'PDF');


          $phpWords = IOFactory::load(Yii::$app->basePath.'/web/doc/Doc12345678.docx', 'Word2007');
          $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWords,'PDF');
//        $xmlWriter->save('tests.pdf');  // Save to PDF
            $xmlWriter->save(Yii::$app->basePath.'/web/doc/Doc12346789.pdf', TRUE);



//        $phpWord = new \PhpOffice\PhpWord\PhpWord();
//        \PhpOffice\PhpWord\Settings::setPdfRendererPath('vendor/dompdf/dompdf');
//        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
//
//        $document = $phpWord->loadTemplate($phpWords);
//        $document->saveAs($phpWords);
//        $phpWord = \PhpOffice\PhpWord\IOFactory::load($phpWords);
//        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'PDF');
//        $xmlWriter->save('tests.pdf');  // Save to PDF
    }

}
