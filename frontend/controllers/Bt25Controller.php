<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Bt25Controller extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }
    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = MouNew::find()->where(['id'=>$_GET['id']])->one();

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleLeft = "text-align: left;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";

        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/bt25_new.pdf');

//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            if($pageNo ==  1){ // 15
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);








                $y1 = 101;
                $start = 59;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 109;
                $start = 28;
                $end =67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->phone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

  $y1 = 109;
                $start = 154;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 210;
                $start = 73;
                $end =141;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 210;
                $start = 156;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 217;
                $start = 33;
                $end =100;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 242;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 265;
                $start = 44;
                $end =82;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 265;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


            }  // 15

            if($pageNo ==  2){ // 10
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";


                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);




                $y1 = 79;
                $start = 45;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

//
//                $y1 = 79;
//                $start = 45;
//                $end =183;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');



//                $y1 = 86;
//                $start = 135;
//                $end =183;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->position.'</div>
//', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 86;
                $start = 104;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 94;
                $start = 29;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.' '.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 101;
                $start = 121;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->telephone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $m = 0;
                $p  = 0;
                $l  = 0;


                $y1 = 132;
                $start = 97;
                $end =186;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '"> กัมพูชา '.$m.' คน , ลาว '.$p.' คน , พม่า '.$l.' คน</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 256;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 279;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');




                $y1 = 279;
                $start = 117;
                $end =160;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



            }  // 16

            if($pageNo >=  3){ // 11
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $pdf->SetWatermarkText("ใช้ดำเนินการ แจ้งคนงานต่างด้าวเข้าทำงาน ตามเอกสารแจ้งเข้าแรงงาน เลขที่ ".$mou->code." ให้กับนายจ้าง บริษัท ".$mou->company->name." โดยมอบผู้รับมอบอำนาจดำเนินการ ชื่อ ".$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name);

//        $pdf->SetWatermarkText($wm);
                $pdf->showWatermarkText = true;


            }  // 16


        }

//
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 90, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 129, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  50, 1, 90, 'auto');
//
//        }
//$paydate
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//        }
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//        }


//        $ttt = htmlentities("ใช้ดำเนินกา แจ้งคนงานต่างด้าวเข้าทำงาน <br/>
//ตามเอกสารแจ้งเข้าแรงงาน เลขที่ LE20070024   <br/>
//ให้กับนายจ้าง บริษัท xxxxxx   <br/>
//โดยมอบผู้รับมอบอำนาจดำเนินการ ชื่อ xxxxx");
//        $wm = \Mpdf\Utils\UtfString::strcode2utf($ttt);

//

//        $pdf->AddPage();
//        $pdf->Image(Yii::$app->basePath.'/web/doc/nj-17.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
//        $y1 = 137;
//        $start = 48;
//        $end =179;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 117;
//        $end =125;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->number_price.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 161;
//        $end =183;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//        $y1 = 151;
//        $start = 30;
//        $end =175;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$this->Convert($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 238;
//        $start = 39;
//        $end =85;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
