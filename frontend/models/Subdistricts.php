<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subdistricts".
 *
 * @property int $id
 * @property int $code
 * @property string $name_in_thai
 * @property string $name_in_english
 * @property string $latitude
 * @property string $longitude
 * @property int $district_id
 * @property int $zip_code
 *
 * @property Company[] $companies
 * @property Districts $district
 */
class Subdistricts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subdistricts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name_in_thai', 'latitude', 'longitude', 'district_id'], 'required'],
            [['code', 'district_id', 'zip_code'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['name_in_thai', 'name_in_english'], 'string', 'max' => 150],
            [['code'], 'unique'],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['district_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name_in_thai' => 'Name In Thai',
            'name_in_english' => 'Name In English',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'district_id' => 'District ID',
            'zip_code' => 'Zip Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['subdistricts_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['id' => 'district_id']);
    }

    /**
     * {@inheritdoc}
     * @return SubdistrictsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubdistrictsQuery(get_called_class());
    }
}
