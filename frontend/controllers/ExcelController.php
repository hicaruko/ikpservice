<?php

namespace frontend\controllers;

use kartik\mpdf\Pdf;

class ExcelController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $pdf = new Pdf(); // or new Pdf();
        $mpdf = $pdf->api; // fetches mpdf api
        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
        $mpdf->WriteHtml('test'); // call mpdf write html
        echo $mpdf->Output('filename', 'D'); // call the mpdf api output as needed
    }

}
