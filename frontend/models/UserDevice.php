<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_device".
 *
 * @property int $id
 * @property string $device_token
 * @property int $status
 * @property string $device
 * @property string $info
 * @property string $model
 * @property int $user_id
 *
 * @property User $user
 */
class UserDevice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'user_id'], 'integer'],
            [['device', 'info'], 'string'],
            [['user_id'], 'required'],
            [['device_token', 'model'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_token' => 'Device Token',
            'status' => 'Status',
            'device' => 'Device',
            'info' => 'Info',
            'model' => 'Model',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
