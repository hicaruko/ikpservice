<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "training".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 *
 * @property Mou[] $mous
 */
class Training extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'title_en'], 'required'],
            [['title', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_en' => 'Title En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMous()
    {
        return $this->hasMany(Mou::className(), ['training_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return TrainingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingQuery(get_called_class());
    }
}
