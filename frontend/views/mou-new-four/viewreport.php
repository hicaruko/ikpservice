<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'รายงาน '.$model->company->name." / ครบ 4 ปี ";
$this->params['breadcrumbs'][] = ['label' => 'ครบ 4 ปี', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">

        </div>
        <div class="col-md-3">
            <?php
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            $ShowPrint  = false;
            $Directory = \app\models\Directory::find()->where(['key'=>'c-doc','mou_id'=>$model->id])->one();
            if($Directory){
                if($Directory->status == 2 ){
                    $Directorys = \app\models\Directory::find()->where(['key'=>'c-slip','mou_id'=>$model->id])->one();
                    if($Directorys->status == 2 ){
                        $ShowPrint = true;
                    }
                }
            }
            if($user->type == 0){
                $ShowPrint = true;
            }

            ?>

            <?= Html::a('รายละเอียดครบ 4 ปี', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?php if($ShowPrint) { ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?php } ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>

            <?php if($ShowPrint) { ?>
                <?= Html::a('พิมพ์ครบ 4 ปี ', ['bt25/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?php } ?>
            <?= Html::a('แก้ไขครบ 4 ปี', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>
            <?= Html::a('อัพเดตสถานะ', ['view', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>


        </div>

    </div>

</div>
