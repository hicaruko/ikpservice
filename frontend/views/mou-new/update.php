<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'Update แจ้งเข้า / ต่ออายุ: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'แจ้งเข้า / ต่ออายุ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mou-update">


    <?= $this->render('_form', [
        'model' => $model,
        'isUpdate' => 1,
    ]) ?>

</div>
