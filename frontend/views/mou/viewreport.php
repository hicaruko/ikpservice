<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'รายงาน '.$model->company->name." / Mou ".$model->nationality->title;
$this->params['breadcrumbs'][] = ['label' => 'Mou', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">

        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดคำร้อง', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 1], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc5/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('พิมพ์คำร้อง ', [$model->nationality->url.'/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('แก้ไขคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
