<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CauseOut */

$this->title = 'Update Cause Out: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cause Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cause-out-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
