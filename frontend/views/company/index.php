<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;

$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}


?>
<div class="company-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'code',
            'name',
            'name_en',
            [
                'label' => 'สาขา',
                'attribute'=>'branch',
                'headerOptions' => ['width' => '80'],
                'value' => function($model, $key, $index)
                {
                    if($model->branch == null){
                        return "";

                    } else {
                        return $model->branch;

                    }


                },
            ],
            'address_no',
            [
                'label' => 'Create By',
                'attribute'=>'user_id',
                'headerOptions' => ['width' => '80'],
                'filter'=>$datalists,
                'value' => function($model, $key, $index)
                {
                    $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                    if($datas){
                        return $datas->first_name." ".$datas->last_name;
                    }


                },
            ],
            //'moo',
            //'soi',
            //'road',
            //'telephone',
            //'fax',
            //'business_type_id',
            //'type',
            //'first_name',
            //'first_name_en',
            //'last_name',
            //'last_name_th',
            //'position',
            //'position_en',
            //'prefix_id',
            //'provinces_id',
            //'districts_id',
            //'subdistricts_id',
            //'license',
            //'license_en',
            //'soi_n',
            //'road_en',
            //'province',
            //'province_en',
            //'districts',
            //'districts_en',
            //'subdistricts',
            //'subdistricts_en',
            //'zipcode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
