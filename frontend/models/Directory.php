<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directory".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property string $create_at
 * @property string $approve_date
 * @property int $mou_id
 * @property string $update_at
 * @property int $order
 * @property int $type
 * @property int $bt23
 * @property int $key
 * @property int $doc_type
 * @property int $out_id
 * @property int $directory_temp_id
 *
 * @property DirectoryTemp $directoryTemp
// * @property Mou $mou
 * @property Document[] $documents
 */
class Directory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['status', 'mou_id', 'order', 'type', 'directory_temp_id'], 'integer'],
            [['create_at', 'approve_date', 'update_at','bt23','key','doc_type','out_id'], 'safe'],
            [['directory_temp_id'], 'required'],
            [['directory_temp_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirectoryTemp::className(), 'targetAttribute' => ['directory_temp_id' => 'id']],
//            [['mou_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mou::className(), 'targetAttribute' => ['mou_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'create_at' => 'Create At',
            'approve_date' => 'Approve Date',
            'mou_id' => 'Mou ID',
            'update_at' => 'Update At',
            'order' => 'Order',
            'type' => 'Type',
            'directory_temp_id' => 'Directory Temp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectoryTemp()
    {
        return $this->hasOne(DirectoryTemp::className(), ['id' => 'directory_temp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getMou()
//    {
//        return $this->hasOne(Mou::className(), ['id' => 'mou_id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['directory_id' => 'id']);
    }
}
