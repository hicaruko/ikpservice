<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Witness */

$this->title = 'Create Witness';
$this->params['breadcrumbs'][] = ['label' => 'Witnesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="witness-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
