<?php

namespace frontend\controllers;

use app\models\BusinessType;
use app\models\Company;
use app\models\Employee;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Pdfbt13Controller extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }
    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
        $emp = Employee::find()->where(['mou_id'=>$_GET['id']])->all();

//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";


        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/bt13.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            $mail = 0;
            $fmail = 0;

            if ($pageNo == 1) { // 5

                $blockStyle = "text-align: center;font-family: sarabun;font-size: 18px";
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], TRUE);

//
//                for ($i=0;$i<300;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }  else if($i > 300 ){
//                        $cdd = $i -300;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 49, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<300;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }  else if($i > 300 ){
//                        $cdd = $i -300;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 97, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<300;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }  else if($i > 300 ){
//                        $cdd = $i -300;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  160, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 100, $i, 1, 1, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 200, $i, 1, 1, 'auto');
//
//                }

                $y1 = 38;
                $start = 58;
                $end = 147;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->name . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 38;
                $start = 226;
                $end = 290;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->license . '</div>
', $start, $y1, $end - $start, 90, 'auto');




                $y1 = 46;
                $start = 181;
                $end = 205;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->address_no.'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 46;
                $start = 263;
                $end = 290;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->subdistricts . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 54;
                $start = 32;
                $end = 72;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->districts . '</div>
', $start, $y1, $end - $start, 90, 'auto');



                $y1 = 54;
                $start = 85;
                $end = 125;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->province . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 54;
                $start = 154;
                $end = 193;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->telephone . '</div>
', $start, $y1, $end - $start, 90, 'auto');





                foreach ($emp as $key=>$value){
                    $add = 0;

                    if($key == 0) {
                        $y1 = 77;
                    } else
                        $y1 = $y1+4.5;


                    $start = 13;
                    $end = 25;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.($key+1). '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $start = 26;
                $end = 83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $start = 85;
                $end = 98;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->nationality->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $sex = "ชาย";
                    if($value->sex ==  2){
                        $fmail++;
                        $sex = "หญิง";
                    } else {
                        $mail++;
                    }
                $start = 73;
                $end = 84;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $sex. '</div>
', $start, $y1, $end - $start, 90, 'auto');


                 $start = 98;
                  $end = 153;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->passport . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $start = 155;
                $end = 192;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->work_permit_number . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $start = 194;
                    $end = 262;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->job_des . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $start = 263;
                    $end = 290;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .  2 . '</div>
', $start, $y1, $end - $start, 90, 'auto');
                }


                $y1 = 138;
                $start = 91;
                $end = 105;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . ($mou->male+$mou->female) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 144;
                $start = 91;
                $end = 105;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . ($mail+$fmail) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 144;
                $start = 133;
                $end = 154;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . ($mail) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 144;
                $start = 168;
                $end = 181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . ($fmail) . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 178;
                $start = 26;
                $end = 78;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->prefix->title . $mou->company->first_name . ' ' . $mou->company->last_name . '</div>
', $start, $y1, $end - $start, 90, 'auto');

            }   // 5



        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
