<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Authorize */
/* @var $form yii\widgets\ActiveForm */

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="authorize-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group" style="display: none;">
    <?= $form->field($model, 'mou_id')->textInput(['value'=>$_GET['mou']]) ?>
    <?= $form->field($model, 'type')->textInput(['value'=>$_GET['type']]) ?>
    </div>
    <?php
    $datas = app\models\User::find()->all();
    $datalists = [];
    foreach ($datas as $values) {
        $datalists[$values->id] = $values->first_name . " " . $values->last_name;
    }

    echo $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $datalists,
        'options' => ['placeholder' => '... เลือกผู้รับมอบอำนาจ ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);


//    $datalists = [];
//    foreach ($datas as $values) {
//        $datalists[$values->id] = $values->first_name . " " . $values->last_name;
//    }

//    $Witness = app\models\Witness::find()->all();
    $datalists = [];

    $this->registerJs("
    $('#authorize-user_id').on('change', function() {
    var districtId = $(this).val();
    console.log(districtId);
    $('#authorize-witness_id').html('');
    $('#authorize-witness_id_two').html('');
    $.get(
        '".Yii::$app->request->baseUrl."/index.php/authorize/witness',
        {
            id: districtId
        },
        function(response) {
                console.log(response);
                $('#authorize-witness_id').append(new Option('', ''));
                $('#authorize-witness_id_two').append(new Option('', ''));
                $.each(JSON.parse(response), function() {
                    console.log(this.id + '--' + this.title);
                    var newOption = new Option(this.title, this.id);
                    var newOptions = new Option(this.title, this.id);
                    $('#authorize-witness_id').append(newOption);
                    $('#authorize-witness_id_two').append(newOptions);
                });
            
        }
   );
});
    ", View::POS_END);
    echo $form->field($model, 'witness_id')->widget(Select2::classname(), [
        'data' => $datalists,
        'options' => ['placeholder' => '... เลือกพยานคนที่ 1 ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

//
//
    echo $form->field($model, 'witness_id_two')->widget(Select2::classname(), [
        'data' => $datalists,
        'options' => ['placeholder' => '... เลือกพยานคนที่ 2 ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
