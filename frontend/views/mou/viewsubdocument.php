<?php

use kartik\file\FileInput;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */


$this->title = 'โฟลเดอร์  '.$dir->name;
$this->params['breadcrumbs'][] = ['label' => 'Mou', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="mou-view">

    <div class="row">
        <div class="col-md-9">
            <div class="documents-form">


                <?php

                $doc = \app\models\Documents::find()->where(['directory_id'=>$dir->id])->all();
                $doc_list = array();
                $doc_config = array();
                foreach ($doc as $value){

                    $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
                    array_push($doc_list,Url::to('@web/'.$value->file, true));
                    if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                        array_push($doc_config,array(
                            'caption'=>$value->file,
                            'type'=>'image',
                            'url' => Url::to(['documents/delete','id'=>$value->id,'mou'=>$model->id,'dir'=>$dir->id]),
                            'key'=>$value->id,
                        ));
                    } else {
                        array_push($doc_config,array(
                            'caption'=>$value->file,
                            'type'=>$ext.'',
                            'url' => Url::to(['documents/delete','id'=>$value->id,'mou'=>$model->id,'dir'=>$dir->id]),
                            'key'=>$value->id,

                        ));
                    }

                }

                ActiveForm::begin([
                    'options'=>['enctype'=>'multipart/form-data'] // important
                ]);
                echo FileInput::widget([
                    'name'=>'input_file',
                    'options' => ['multiple' => true],

                    'pluginOptions' => [
                        'previewFileType' => 'any',
                        'initialPreviewFileType' => 'image',
                        'uploadUrl' => Url::toRoute(["documents/upload-file",'id'=>$dir->id,'mou'=>$model->id]),
                        'initialPreview'=>$doc_list,
                        'initialPreviewAsData'=>true,
                        'overwriteInitial'=>false,
                        'initialPreviewConfig'=>$doc_config,
                        'showCaption' => true,
                        'showPreview' => true,

//                         'purifyHtml'=> true,

                    ],
                    'pluginEvents'=>[
                        "filebatchuploadcomplete" => "function() { window.location.reload(true);
                            }",
                    ]
                ]);

                ActiveForm::end();

                ?>

            </div>
            <br>

            <br>
            <br>

        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดคำร้อง', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 1], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc5/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('พิมพ์คำร้อง ', [$model->nationality->url.'/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('แก้ไขคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>
 
        </div>

    </div>

</div>
