<?php

namespace frontend\controllers;

use app\models\Witness;
use Yii;
use app\models\Authorize;
use frontend\models\AuthorizeSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthorizeController implements the CRUD actions for Authorize model.
 */
class AuthorizeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionWitness()
    {
        $Witness = Witness::find()->where(['user_id'=>$_GET['id']])->all();
        $datalists = [];
        foreach ($Witness as $values) {
            array_push($datalists,array(
                'id'=>$values->id,
                'title'=>$values->first_name . " " . $values->last_name,
            ));
        }

        echo Json::encode($datalists);

    }


    /**
     * Lists all Authorize models.
     * @return mixed
     */


    public function actionIndex()
    {
        $searchModel = new AuthorizeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Authorize model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Authorize model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Authorize();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->type == 1){

                return $this->redirect(['mou/view', 'id' => $model->mou_id]);

            } else {
                return $this->redirect(['mou-new/view', 'id' => $model->mou_id]);

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Authorize model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Authorize model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if($model->type == 1){
            return $this->redirect(['mou/view','id'=>$_GET['mou']]);
        } else {
            return $this->redirect(['mou-new/view','id'=>$_GET['mou']]);

        }

    }

    /**
     * Finds the Authorize model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Authorize the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Authorize::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
