<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "other_document_type".
 *
 * @property int $id
 * @property string $title
 */
class OtherDocumentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'other_document_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'ประเภทเอกสารอื่นๆ',
        ];
    }
}
