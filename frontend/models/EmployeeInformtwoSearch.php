<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmployeeInformtwo;

/**
 * EmployeeInformSearch represents the model behind the search form of `app\models\EmployeeInform`.
 */
class EmployeeInformtwoSearch extends EmployeeInformtwo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'report_status', 'mou_new_id', 'type', 'company_id', 'nationality_id', 'prefix_id'], 'integer'],
            [['first_name', 'last_name', 'first_name_en', 'last_name_en', 'nickname', 'blood', 'date_of_birth', 'sex', 'contact_number', 'age', 'email', 'foreign_address', 'photo', 'passport', 'passport_ext', 'passport_is', 'passport_ext_alert', 'issue_location', 'visa_is', 'visa_ext', 'visa_number', 'visa_type', 'stamped', 'entrance_card', 'workpermit_is', 'workpermit_ext', 'tt_number', 'work_permit_number', 'job_description', 'position', 'report_tm_date', 'address','mou_new_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeeInform::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $user_login = User::findOne(Yii::$app->user->id);

        if($user_login){

            if($user_login->type != 0){
                $query->andFilterWhere([
                    'id' => $this->id,
                    'date_of_birth' => $this->date_of_birth,
                    'passport_ext' => $this->passport_ext,
                    'passport_is' => $this->passport_is,
                    'visa_is' => $this->visa_is,
                    'visa_ext' => $this->visa_ext,
                    'workpermit_is' => $this->workpermit_is,
                    'workpermit_ext' => $this->workpermit_ext,
                    'report_status' => $this->report_status,
                    'mou_new_id' => $this->mou_new_id,
                    'type' => $this->type,
                    'company_id' => $this->company_id,
                    'nationality_id' => $this->nationality_id,
                    'prefix_id' => $this->prefix_id,
                    'user_id' => Yii::$app->user->id,
                ]);
            } else {
                $query->andFilterWhere([
                    'id' => $this->id,
                    'date_of_birth' => $this->date_of_birth,
                    'passport_ext' => $this->passport_ext,
                    'passport_is' => $this->passport_is,
                    'visa_is' => $this->visa_is,
                    'visa_ext' => $this->visa_ext,
                    'workpermit_is' => $this->workpermit_is,
                    'workpermit_ext' => $this->workpermit_ext,
                    'report_status' => $this->report_status,
                    'mou_new_id' => $this->mou_new_id,
                    'type' => $this->type,
                    'company_id' => $this->company_id,
                    'nationality_id' => $this->nationality_id,
                    'prefix_id' => $this->prefix_id,
                ]);
            }
        } else {
            $query->andFilterWhere([
                'id' => $this->id,
                'date_of_birth' => $this->date_of_birth,
                'passport_ext' => $this->passport_ext,
                'passport_is' => $this->passport_is,
                'visa_is' => $this->visa_is,
                'visa_ext' => $this->visa_ext,
                'workpermit_is' => $this->workpermit_is,
                'workpermit_ext' => $this->workpermit_ext,
                'report_status' => $this->report_status,
                'mou_new_id' => $this->mou_new_id,
                'type' => $this->type,
                'company_id' => $this->company_id,
                'nationality_id' => $this->nationality_id,
                'prefix_id' => $this->prefix_id,
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([

        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name_en', $this->last_name_en])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'blood', $this->blood])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'foreign_address', $this->foreign_address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'passport_ext_alert', $this->passport_ext_alert])
            ->andFilterWhere(['like', 'issue_location', $this->issue_location])
            ->andFilterWhere(['like', 'visa_number', $this->visa_number])
            ->andFilterWhere(['like', 'visa_type', $this->visa_type])
            ->andFilterWhere(['like', 'stamped', $this->stamped])
            ->andFilterWhere(['like', 'entrance_card', $this->entrance_card])
            ->andFilterWhere(['like', 'tt_number', $this->tt_number])
            ->andFilterWhere(['like', 'work_permit_number', $this->work_permit_number])
            ->andFilterWhere(['like', 'job_description', $this->job_description])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'report_tm_date', $this->report_tm_date])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
    public function searchBymou($params,$id)
    {
        $query = EmployeeInform::find();
        $query->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_of_birth' => $this->date_of_birth,
            'passport_ext' => $this->passport_ext,
            'passport_is' => $this->passport_is,
            'visa_is' => $this->visa_is,
            'visa_ext' => $this->visa_ext,
            'workpermit_is' => $this->workpermit_is,
            'workpermit_ext' => $this->workpermit_ext,
            'report_status' => $this->report_status,
            'mou_new_id' => $id,
            'type' => $this->type,
            'company_id' => $this->company_id,
            'nationality_id' => $this->nationality_id,
            'prefix_id' => $this->prefix_id,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name_en', $this->last_name_en])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'blood', $this->blood])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'foreign_address', $this->foreign_address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'passport_ext_alert', $this->passport_ext_alert])
            ->andFilterWhere(['like', 'issue_location', $this->issue_location])
            ->andFilterWhere(['like', 'visa_number', $this->visa_number])
            ->andFilterWhere(['like', 'visa_type', $this->visa_type])
            ->andFilterWhere(['like', 'stamped', $this->stamped])
            ->andFilterWhere(['like', 'entrance_card', $this->entrance_card])
            ->andFilterWhere(['like', 'tt_number', $this->tt_number])
            ->andFilterWhere(['like', 'work_permit_number', $this->work_permit_number])
            ->andFilterWhere(['like', 'job_description', $this->job_description])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'report_tm_date', $this->report_tm_date])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
