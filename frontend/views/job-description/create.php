<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JobDescription */

$this->title = 'Create ลักษณะงาน';
$this->params['breadcrumbs'][] = ['label' => 'ลักษณะงาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-description-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
