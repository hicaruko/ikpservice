<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = $model->company->name." / Mou ".$model->nationality->title;
$this->params['breadcrumbs'][] = ['label' => 'Mou', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mou-view">




    <div class="row">
        <div class="col-md-9">

            <div class="row">
                <div class="col-md-6">
                    <h4>ข้อมูล Mou</h4>

                    <?php echo "<b>".$model->getAttributeLabel('code').'</b>: '.$model->code ?>


                </div>
                <div class="col-md-6">
                    <h4>ข้อมูล นายจ้าง</h4>

                    <?php $Company =  \app\models\Company::find()->where(['id'=>$model->company_id])->one() ?>
                    <?php if($Company) { ?>
                        <?php echo "<b>".$model->getAttributeLabel('company_id').'</b>: '.$Company->name ?>
                    <?php } ?>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>ประสงค์จะจ้างคนต่างด้าว</h4>
                    <?php echo "<b>".$model->getAttributeLabel('male').'</b>: '.$model->male ?>
                    <br>
                    <?php echo "<b>".$model->getAttributeLabel('female').'</b>: '.$model->female ?>

                </div>
                <div class="col-md-12">
                    <h4>คุณสมบัติที่ต้องการ</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo "<b>".$model->getAttributeLabel('qualification_age').'</b>: '.$model->qualification_age ?>


                        </div>
                        <div class="col-md-4">
                            <?php echo "<b>".$model->getAttributeLabel('qualification_height').'</b>: '.$model->qualification_height ?>

                        </div>
                        <div class="col-md-4">

                            <?php echo "<b>".$model->getAttributeLabel('qualification_weight').'</b>: '.$model->qualification_weight ?>


                        </div>
                    </div>
                </div>
            </div>
            <h4>ข้อมูลสถานที่ทำงาน
            </h4>

            <div class="row">
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('telephone').'</b>: '.$model->telephone ?>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('fax').'</b>: '.$model->fax ?>

                </div>

                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('address_no').'</b>: '.$model->address_no ?>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('moo').'</b>: '.$model->moo ?>

                </div>

                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('soi').'</b>: '.$model->soi ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('road').'</b>: '.$model->road ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('province').'</b>: '.$model->province ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('districts').'</b>: '.$model->districts ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('subdistricts').'</b>: '.$model->subdistricts ?> <br>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('soi_n').'</b>: '.$model->soi_n ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('road_en').'</b>: '.$model->road_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('province_en').'</b>: '.$model->province_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('districts_en').'</b>: '.$model->districts_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('subdistricts_en').'</b>: '.$model->subdistricts_en ?> <br>

                </div>
                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('zipcode').'</b>: '.$model->zipcode ?> <br>


                </div>
            </div>
            <div class="row">
                <div class="col-md-4">

                    <?php echo "<b>".$model->getAttributeLabel('preriod_year').'</b>: '.$model->preriod_year ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('preriod_month').'</b>: '.$model->preriod_month ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('wage_per_day').'</b>: '.$model->wage_per_day ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('week_holiday').'</b>: '.$model->week_holiday ?> <br>

                    <!--            --><?php //echo "<b>".$model->getAttributeLabel('user_id').'</b>: '.$model->user_id ?><!-- <br>-->

                </div>
                <div class="col-md-4">

                    <?php echo "<b>".$model->getAttributeLabel('annual').'</b>: '.$model->annual ?> <br>

                    <!--            --><?php
                    //            $natiC = ArrayHelper::map(\app\models\Holiday::find()->all(), 'id', 'title');
                    //            echo $form->field($model, 'holiday_id')->dropDownList(
                    //                $natiC,
                    //                ['prompt'=>'เลือกวันหยุด']
                    //
                    //            ); ?>
                    <?php echo "<b>".$model->getAttributeLabel('work_time').'</b>: '.$model->work_time ?> <br>

                    <?php echo "<b>".$model->getAttributeLabel('paydate').'</b>: '.$model->paydate ?> <br>

                </div>
                <div class="col-md-4">
                    <?php echo "<b>".$model->getAttributeLabel('price').'</b>: '.$model->price ?> <br>

                </div>
            </div>

            <h4>ข้อมูลลายเซนเอกสาร</h4>

            <div class="row">
                <div class="col-md-4">

                    <?php $Agent = app\models\Agent::find()->where(['id'=>$model->agent_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('agent_id').'</b>: '.$Agent->name ?> <br>

                    <?php $Grantee = app\models\User::find()->where(['id'=>$model->user_id1])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_id1').'</b>: '.$Grantee->first_name." ".$Grantee->last_name ?> <br>

                </div>
                <div class="col-md-4">

                    <?php $Witness = app\models\Witness::find()->where(['id'=>$model->witness_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id').'</b>: '.$Witness->first_name." ".$Witness->last_name ?> <br>

                    <?php $witness_id_two = app\models\Witness::find()->where(['id'=>$model->witness_id_two])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id_two').'</b>: '.$witness_id_two->first_name." ".$witness_id_two->last_name ?> <br>


                </div>
                <div class="col-md-4">

                    <?php $DanImmigration = app\models\DanImmigration::find()->where(['id'=>$model->dan_Immigration_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('dan_Immigration_id').'</b>: '.$DanImmigration->title ?> <br>

                    <?php $Training = app\models\Training::find()->where(['id'=>$model->training_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('training_id').'</b>: '.$Training->title ?> <br>


                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h4>ผู้รับผิดชอบงาน</h4>

                    <?php $User = app\models\User::find()->where(['id'=>$model->user_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_id').'</b>: '.$User->first_name.' '.$User->last_name ?> <br>


                </div>
            </div>
            <?php if($model->user_other != NULL) { ?>
            <div class="row">
                <div class="col-md-12">
                    <h4>ผู้รับมอบอำนาจเพิ่มเติม</h4>


                    <?php $user_other = app\models\User::find()->where(['id'=>$model->user_other])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_other').'</b>: '.$user_other->first_name.' '.$user_other->last_name ?> <br>



                </div>
            </div>
            <?php } ?>
            <br>
            <br>

            <h1>มอบอำนาจเพิ่มเติม</h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('สร้าง มอบอำนาจเพิ่มเติม', ['authorize/create', 'mou' => $model->id, 'type' => 1], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProviderP,
                'filterModel' => $searchModelP,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'user.first_name',
                    'user.last_name',
                    [
                        'label' => 'พิมพ์มอบอำนาจ',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {

                                return '<a href="'.Url::to(['empdoc/index','id' => $model->mou_id, 'uid' => $model->user_id, 'type' => 1]).'">พิมพ์มอบอำนาจ</a>';

                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{delete}</div>',
                        'urlCreator' => function( $action, $modelA, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['authorize/view', 'id' => $modelA->id]);

                            }
                            if ($action == "update") {

                                return Url::to(['authorize/update', 'id' => $modelA->id, 'mou' => $_GET['id']]);

                            }
                            if ($action == "delete") {
                                return Url::to(['authorize/delete', 'id' => $modelA->id, 'mou' => $_GET['id']]);
                            }
                        }

                    ],
                ],
            ]); ?>

            <br>

            <h1>แจ้งออก Mou</h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <p>
                <?= Html::a('สร้างแจ้งออก', ['out/create','type'=>2,'mou'=>$model->id], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProviderO,
                'filterModel' => $searchModelO,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'code',
                    'mou.code',
                    'out_date',
                    [
                        'attribute'=>'company_id',
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            return $model->company->name;
                        }

                    ],
                    'nationality.title',
                    'note:ntext',
//
//            'id',
//            'company_id',
//            'out_date',
//            'out_total',
//            'witness_id',
                    //'witness_id2',
                    //'user_id',
                    //'unit_price',
                    //'note:ntext',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{view} {update} {delete}</div>',
                        'urlCreator' => function( $action, $modelA, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['out/viewemp', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 2]);
                            }
                            if ($action == "update") {
                                return Url::to(['out/update', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 2]);
                            }
                            if ($action == "delete") {
                                return Url::to(['out/delete', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 2]);
                            }
                        }

                    ],
                ],
            ]); ?>

        </div>
        <div class="col-md-3">
                <?= Html::a('รายละเอียดคำร้อง', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
                <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
                <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 1], ['class' => 'btn btn-primary btn-block']) ?>
                <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
        <br>
        <br>
            <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc5/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('พิมพ์คำร้อง ', [$model->nationality->url.'/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('แก้ไขคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>
<!--            --><?//= Html::a('อัพเดตสถานะ', ['view', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
