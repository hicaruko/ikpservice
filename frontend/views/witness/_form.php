<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Witness */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="witness-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
    echo $form->field($model, 'prefix_id')->dropDownList(
        $natiC,
        ['prompt'=>'เลือกคำนำหน้าชื่อ']

    ); ?>
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name_en')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
