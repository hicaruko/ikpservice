<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status_doc".
 *
 * @property int $id
 * @property int $mou_status_id
 * @property int $type
 * @property string $path
 */
class StatusDoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_doc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mou_status_id', 'type'], 'integer'],
            [['path'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mou_status_id' => 'Mou Status ID',
            'type' => 'Type',
            'path' => 'Path',
        ];
    }
}
