<?php

namespace frontend\controllers;

use app\models\Mou;
use app\models\MouNew;
use Yii;
use app\models\Documents;
use app\models\DocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DocumentsController implements the CRUD actions for Documents model.
 */
class DocumentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-em' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Documents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/documents/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/documents/';
    }

    public function actionUploadFile()
    {
        $path = $this->getUploadPath();
        $imageFile = UploadedFile::getInstanceByName('input_file[0]');
        $mou = Mou::find()->where(['id'=>$_GET['mou']])->one();
        $fileName = $mou->code."-".$imageFile->baseName . '.' . $imageFile->extension;
        if($imageFile->saveAs( $path.$fileName)){
            $model = new Documents();
            $model->file = 'documents/'.$fileName;
            $model->directory_id = intval($_GET['id']);
            if($model->save()){
                echo json_encode(['success' => 'true']);
            } else {
                print_r($model->getErrors());
            }
        }

    }

    public function actionUploadFileNew()
    {
        $path = $this->getUploadPath();
        $imageFile = UploadedFile::getInstanceByName('input_file[0]');
        $mou = MouNew::find()->where(['id'=>$_GET['mou']])->one();
        $fileName = $mou->code."-".$imageFile->baseName . '.' . $imageFile->extension;
        if($imageFile->saveAs( $path.$fileName)){
            $model = new Documents();
            $model->file = 'documents/'.$fileName;
            $model->directory_id = intval($_GET['id']);
            if($model->save()){
                echo json_encode(['success' => 'true']);
            } else {
                print_r($model->getErrors());
            }
        }

    }
        public function actionUploadFileEm()
    {
        $path = $this->getUploadPath();
        $imageFile = UploadedFile::getInstanceByName('input_file[0]');
        $mou = MouNew::find()->where(['id'=>$_GET['mou']])->one();
        $fileName = $mou->code."-".$imageFile->baseName . '.' . $imageFile->extension;
        if($imageFile->saveAs( $path.$fileName)){
            $model = new Documents();
            $model->file = 'documents/'.$fileName;
            $model->directory_id = intval($_GET['id']);
            $model->doc_type = 1;
            $model->ref_id = intval($_GET['em']);
            if($model->save()){
                echo json_encode(['success' => 'true']);
            } else {
                print_r($model->getErrors());
            }
        }

    }

    /**
     * Displays a single Documents model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Documents();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['mou/viewsubdocument','id'=>$_GET['mou'],'dir'=>$_GET['dir']]);
    }
    public function actionDeleteEm($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['employee-inform/view','id'=>$_GET['em_id']]);

    }
    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
