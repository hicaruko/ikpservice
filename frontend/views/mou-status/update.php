<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MouStatus */

$this->title = 'Update Mou Status: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mou Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mou-status-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
