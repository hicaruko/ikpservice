<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directory */

$this->title = 'Create Directory';
$this->params['breadcrumbs'][] = ['label' => 'Directories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directory-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<select class="form-control select2 select2-hidden-accessible" style="width:30%;" id="status_id" name="status_id" tabindex="-1" aria-hidden="true">
    <option value="00">รอดำเนินการ</option>
    <option value="5">1.เตรียมเอกสารคำร้อง</option>
    <option value="1">2.ยื่นคำร้องขอนำเข้าแรงงาน</option>
    <option value="3">3. รับคำร้องจากจัดหางานพื้นที่</option>
    <option value="2">4.ส่งเอกสารเข้าสถานทูต(ประเทศต้นทาง)</option>
    <option value="6">5.เซ็นสัญญาประเทศต้นทาง(คัดแรงงาน)</option>
    <option value="7">6.รับ name List ระบุตัวตนแรงงาน</option>
    <option value="8">7.ยื่น work permit ตาม name List</option>
    <option value="20">8.รับคำร้องเตรียมส่ง (CENTER )</option>
    <option value="21">9.CENTER รับเอกสาร</option>
    <option value="22">10.CENTER ส่งเอกสารเข้ากระทรวง</option>
    <option value="10">11.CENTER  รับเอกสารจากกระทรวงแรงงาน</option>
    <option value="11">12.CENTER ส่งเอกสาร-ผู้ดูแล</option>
    <option value="9">13.ผู้ดูแลงาน รับเอกสาร</option>
    <option value="12">14.นัดวันเดินทาง รับแรงงานหรือ รับวีซ่า</option>
    <option value="13">15. เดินทางเข้าไทย+ อบรมณศูนย์แรกรับ</option>
    <option value="23">16.เดินทางถึงสถานประกอบการ </option>
    <option value="16" selected="">17. แจ้งเข้าแรงงาน 15 วัน ตามกฎหมาย</option>
    <option value="17">18. ตรวจโรคและรับรอง โรคต้องห้ามตาม(กฎหมายแรงงาน)</option>
    <option value="18">19.ยื่นปลดล็อค ใบอนุญาตทำงาน 30 วัน</option>
    <option value="14">20.งานเสร็จสมบูรณ์^^....</option>
    <option value="15">TM แจ้งที่พักอาศัย 24 ชม.</option>
    <option value="24">Plan A</option>
    <option value="25">Plan B </option>
    <option value="26">ส่งยกเลิกคำร้อง</option>
</select>