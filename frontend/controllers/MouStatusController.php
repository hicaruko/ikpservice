<?php

namespace frontend\controllers;

use app\models\User;
use Yii;
use app\models\MouStatus;
use app\models\MouStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MouStatusController implements the CRUD actions for MouStatus model.
 */
class MouStatusController extends Controller
{


    /**
     * Lists all MouStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MouStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MouStatus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MouStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_login = User::findOne(Yii::$app->user->id);
        $model = new MouStatus();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = $user_login->id;
            if($model->save()){
                if($model->type == 1){
                    return $this->redirect(['mou/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                } else if($model->type == 2){
                    return $this->redirect(['mou-new/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                } else {
                    return $this->redirect(['out/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                }
            }  else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);

        }

    }

    /**
     * Updates an existing MouStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                if($model->type == 1){
                    return $this->redirect(['mou/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                } else if($model->type == 2){
                    return $this->redirect(['mou-new/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                } else {
                    return $this->redirect(['out/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
                }

            }  else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {

            return $this->render('update', [
                'model' => $model,
            ]);

        }
    }

    /**
     * Deletes an existing MouStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        if($model->type == 1){
            return $this->redirect(['mou/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
        } else if($model->type == 2){
            return $this->redirect(['mou-new/viewstatus', 'id' => $model->ref_id,'type' =>$_GET['type']]);
        } else {
            return $this->redirect(['mou-new/out', 'id' => $model->ref_id,'type' =>$_GET['type']]);
        }
     }

    /**
     * Finds the MouStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MouStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MouStatus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
