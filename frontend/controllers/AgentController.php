<?php

namespace frontend\controllers;

use Yii;
use app\models\Agent;
use app\models\AgentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AgentController implements the CRUD actions for Agent model.
 */
class AgentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Agent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agent();

        if ($model->load(Yii::$app->request->post())) {
            $photo  = UploadedFile::getInstance($model, 'license_file');
            $path = $this->getUploadPath();
            $pathUrl = $this->getUploadUrl();
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->license_file = 'employee/'.$fileName;
                    $model->save();

                    return $this->redirect(['view', 'id' => $model->id]);

                } else {
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }

         }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPath = $model->license_file;
        if ($model->load(Yii::$app->request->post())) {
            $photo  = UploadedFile::getInstance($model, 'license_file');
            $path = $this->getUploadPath();
            $pathUrl = $this->getUploadUrl();
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->license_file = 'employee/'.$fileName;

                    $model->save();

                    return $this->redirect(['view', 'id' => $model->id]);

                } else {
                    $model->license_file = $oldPath;
                    $model->save();

                        return $this->redirect(['view', 'id' => $model->id]);

                }
            } else {
                $model->license_file = $oldPath;
                $model->save();

                    return $this->redirect(['view', 'id' => $model->id]);

            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/employee/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/employee/';
    }

    /**
     * Deletes an existing Agent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
