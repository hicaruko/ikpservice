<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = $model->company->name." / ครบ 4 ปี ";
$this->params['breadcrumbs'][] = ['label' => 'ครบ 4 ปี', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);



?>
<div class="mou-view">




    <div class="row">
        <div class="col-md-9">

            <div class="row">
                <div class="col-md-6">
                    <h4>ข้อมูล Mou</h4>

                    <?php echo "<b>".$model->getAttributeLabel('code').'</b>: '.$model->code ?>


                </div>
                <div class="col-md-6">
                    <h4>ข้อมูล นายจ้าง</h4>

                    <?php $Company =  \app\models\Company::find()->where(['id'=>$model->company_id])->one() ?>
                    <?php if($Company) { ?>
                        <?php echo "<b>".$model->getAttributeLabel('company_id').'</b>: '.$Company->name ?>
                    <?php } ?>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>ประสงค์จะจ้างคนต่างด้าว</h4>
                    <?php echo "<b>".$model->getAttributeLabel('male').'</b>: '.$model->male ?>
                    <br>
                    <?php echo "<b>".$model->getAttributeLabel('female').'</b>: '.$model->female ?>

                </div>
                <div class="col-md-12">
                    <h4>คุณสมบัติที่ต้องการ</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo "<b>".$model->getAttributeLabel('qualification_age').'</b>: '.$model->qualification_age ?>


                        </div>
                        <div class="col-md-4">
                            <?php echo "<b>".$model->getAttributeLabel('qualification_height').'</b>: '.$model->qualification_height ?>

                        </div>
                        <div class="col-md-4">

                            <?php echo "<b>".$model->getAttributeLabel('qualification_weight').'</b>: '.$model->qualification_weight ?>


                        </div>
                    </div>
                </div>
            </div>
            <h4>ข้อมูลสถานที่ทำงาน
            </h4>

            <div class="row">
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('telephone').'</b>: '.$model->telephone ?>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('fax').'</b>: '.$model->fax ?>

                </div>

                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('address_no').'</b>: '.$model->address_no ?>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('moo').'</b>: '.$model->moo ?>

                </div>

                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('soi').'</b>: '.$model->soi ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('road').'</b>: '.$model->road ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('province').'</b>: '.$model->province ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('districts').'</b>: '.$model->districts ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('subdistricts').'</b>: '.$model->subdistricts ?> <br>

                </div>
                <div class="col-md-6">

                    <?php echo "<b>".$model->getAttributeLabel('soi_n').'</b>: '.$model->soi_n ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('road_en').'</b>: '.$model->road_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('province_en').'</b>: '.$model->province_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('districts_en').'</b>: '.$model->districts_en ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('subdistricts_en').'</b>: '.$model->subdistricts_en ?> <br>

                </div>
                <div class="col-md-6">
                    <?php echo "<b>".$model->getAttributeLabel('zipcode').'</b>: '.$model->zipcode ?> <br>


                </div>
            </div>
            <div class="row">
                <div class="col-md-4">

                    <?php echo "<b>".$model->getAttributeLabel('preriod_year').'</b>: '.$model->preriod_year ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('preriod_month').'</b>: '.$model->preriod_month ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('wage_per_day').'</b>: '.$model->wage_per_day ?> <br>
                    <?php echo "<b>".$model->getAttributeLabel('week_holiday').'</b>: '.$model->week_holiday ?> <br>

                    <!--            --><?php //echo "<b>".$model->getAttributeLabel('user_id').'</b>: '.$model->user_id ?><!-- <br>-->

                </div>
                <div class="col-md-4">

                    <?php echo "<b>".$model->getAttributeLabel('annual').'</b>: '.$model->annual ?> <br>

                    <!--            --><?php
                    //            $natiC = ArrayHelper::map(\app\models\Holiday::find()->all(), 'id', 'title');
                    //            echo $form->field($model, 'holiday_id')->dropDownList(
                    //                $natiC,
                    //                ['prompt'=>'เลือกวันหยุด']
                    //
                    //            ); ?>
                    <?php echo "<b>".$model->getAttributeLabel('work_time').'</b>: '.$model->work_time ?> <br>

                    <?php echo "<b>".$model->getAttributeLabel('paydate').'</b>: '.$model->paydate ?> <br>

                </div>
                <div class="col-md-4">
                    <?php echo "<b>".$model->getAttributeLabel('price').'</b>: '.$model->price ?> <br>

                </div>
            </div>

            <h4>ข้อมูลลายเซนเอกสาร</h4>

            <div class="row">
                <div class="col-md-4">

                    <?php $Grantee = app\models\User::find()->where(['id'=>$model->user_id1])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_id1').'</b>: '.$Grantee->first_name." ".$Grantee->last_name ?> <br>

                </div>
                <div class="col-md-4">

                    <?php $Witness = app\models\Witness::find()->where(['id'=>$model->witness_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id').'</b>: '.$Witness->first_name." ".$Witness->last_name ?> <br>

                    <?php $witness_id_two = app\models\Witness::find()->where(['id'=>$model->witness_id_two])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id_two').'</b>: '.$witness_id_two->first_name." ".$witness_id_two->last_name ?> <br>


                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <h4>ผู้รับผิดชอบงาน</h4>

                    <?php $User = app\models\User::find()->where(['id'=>$model->user_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_id').'</b>: '.$User->first_name.' '.$User->last_name ?> <br>


                </div>
            </div>
            <br>
            <br>

        </div>
        <div class="col-md-3">
            <?php
            $user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
            $ShowPrint  = false;
            $Directory = \app\models\Directory::find()->where(['key'=>'c-doc','mou_id'=>$model->id])->one();
            if($Directory){
                if($Directory->status == 2 ){
                    $Directorys = \app\models\Directory::find()->where(['key'=>'c-slip','mou_id'=>$model->id])->one();
                    if($Directorys->status == 2 ){
                        $ShowPrint = true;
                    }
                }
            }
            if($user->type == 0){
                $ShowPrint = true;
            }

            ?>

            <?= Html::a('รายละเอียดครบ 4 ปี', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?php if($ShowPrint) { ?>
                <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?php } ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>

            <br>
            <br>

            <?php if($ShowPrint) { ?>
                <?= Html::a('พิมพ์ครบ 4 ปี ', ['bt25/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?php } ?>
            <?= Html::a('แก้ไขครบ 4 ปี', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>
            <?= Html::a('อัพเดตสถานะ', ['view', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>


        </div>

    </div>

</div>
