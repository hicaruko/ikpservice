<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'อัพเดทสถานะ '.$model->company->name." / Mou ".$model->nationality->title;
$this->params['breadcrumbs'][] = ['label' => 'Mou', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}
?>
<div class="mou-view">


    <div class="row">
        <div class="col-md-9">
            <p>
                <?= Html::a('อัพเดทสถานะ', ['mou-status/create', 'mou' => $model->id, 'type' => 1], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'หมายเลขเอกสาร',
                        'attribute'=>'refID.code',
                        'headerOptions' => ['width' => '80'],

                    ],
                    [
                        'label' => 'สถานะ',
                        'attribute'=>'jobStatus.title',
                        'headerOptions' => ['width' => '80'],

                    ],
                    'create_at',
                    'detail:ntext',
                    'job_date',
                    [
                        'label' => 'Create By',
                        'attribute'=>'user_id',
                        'headerOptions' => ['width' => '80'],
                        'filter'=>$datalists,
                        'value' => function($model, $key, $index)
                        {
                            $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                            if($datas){
                                return $datas->first_name." ".$datas->last_name;
                            }

                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{view} {update} {delete}</div>',
                        'urlCreator' => function( $action, $modelA, $key, $index ){
                            if ($action == "view") {
                                return Url::to(['mou-status/view', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                            if ($action == "update") {
                                return Url::to(['mou-status/update', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                            if ($action == "delete") {
                                return Url::to(['mou-status/delete', 'id' => $modelA->id, 'mou' => $_GET['id'], 'type' => 1]);
                            }
                        }

                    ],
                ],
            ]); ?>
            </div>

        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดคำร้อง', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 1], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc5/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('พิมพ์คำร้อง ', [$model->nationality->url.'/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('แก้ไขคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
