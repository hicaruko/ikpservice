<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'ลูกจ้าง '.$model->company->name." / Mou ".$model->nationality->title;
$this->params['breadcrumbs'][] = ['label' => 'Mou', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
$ActionColumn = '{view} {update}';
if($user->type == 0) {
    $ActionColumn = '{view} {update} {delete}';
}


?>
<div class="mou-view">
    <div class="row">
        <div class="col-md-9">
            <p>
                <?= Html::a('เพิ่มลูกจ้าง', ['employee/create', 'mou' => $model->id], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="table-responsive">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Photo',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            return '<img src="'.Url::base().'/'.$model->photo.'" class="" alt="User Image" width="100">';
                        },
                    ],
                    'code',
                    'first_name_en',
                    'last_name_en',
                    'passport',
                    'visa_number',
                    'work_permit_number',
                    [
                        'label' => 'status',
                        'attribute'=>'status',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->status == 2){
                                return 'แจ้งออก';
                            } else{
                                return 'ปกติ';
                            }
                        },
                    ],
//                    'report_status',
                    //'nickname',
                    //'blood',
                    //'date_of_birth',
                    //'sex',
                    //'contact_number',
                    //'age',
                    //'email:email',
                    //'foreign_address:ntext',
                    //'photo:ntext',
                    //'passport',
                    //'passport_ext',
                    //'passport_is',
                    //'passport_ext_alert:ntext',
                    //'issue_location',
                    //'visa_is',
                    //'visa_ext',
                    //'visa_number',
                    //'visa_type',
                    //'stamped',
                    //'entrance_card',
                    //'workpermit_is',
                    //'workpermit_ext',
                    //'tt_number',
                    //'work_permit_number',
                    //'job_description',
                    //'position',
                    //'report_tm_date',
                    //'report_status',
                    //'mou_id',
                    //'type',
                    //'address:ntext',
                    //'company_id',
                    //'nationality_id',
                    //'prefix_id',
                    [
                        'label' => 'บต 30',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt30/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 30</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt30/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 30</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt30/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 30</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'บต 44',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt44mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 44</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt44mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 44</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt44mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 44</a>';
                            }
                        },
                    ],

//                    [
//                        'label' => 'บต 13',
//                        'attribute'=>'photo',
//                        'headerOptions' => ['width' => '80'],
//                        'format'=>'raw',
//                        'value' => function($model, $key, $index)
//                        {
//                            if($model->nationality->title_en == "Cambodian"){
//                                return '<a href="'.Url::to(['pdfbt13/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'">บต 13</a>';
//                            } else   if($model->nationality->title_en == "Myanmar"){
//                                return '<a href="'.Url::to(['pdfbt13/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'">บต 13</a>';
//                            } else {
//                                return '<a href="'.Url::to(['pdfbt13/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'">บต 13</a>';
//                            }
//                        },
//                    ],

                    [
                        'label' => 'บต 46',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt46mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 46</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt46mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 46</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt46mou/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 46</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'สัญญาจ้าง',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfmouemp/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">สัญญาจ้าง</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfmouemp/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">สัญญาจ้าง</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfemlmouemp/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">สัญญาจ้าง</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'มอบอำนาจต่างด้าว',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfemcmouassign/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfemmouassign/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfemlmouassign/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">มอบอำนาจต่างด้าว</a>';
                            }
                        },
                    ],

                    [
                        'label' => 'บต 11',
                        'attribute'=>'photo',
                        'headerOptions' => ['width' => '80'],
                        'format'=>'raw',
                        'value' => function($model, $key, $index)
                        {
                            if($model->nationality->title_en == "Cambodian"){
                                return '<a href="'.Url::to(['pdfbt11/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 11</a>';
                            } else   if($model->nationality->title_en == "Myanmar"){
                                return '<a href="'.Url::to(['pdfbt11/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 11</a>';
                            } else {
                                return '<a href="'.Url::to(['pdfbt11/index', 'em_id' => $model->id, 'id' => $model->mou_id]).'" target="_blank">บต 11</a>';
                            }
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> '.$ActionColumn.'</div>',
                        'urlCreator' => function( $action, $model, $key, $index ){
                            if($model->status != 2){
                                if ($action == "view") {
                                    return Url::to(['employee/view', 'id' => $model->id]);

                                }
                                if ($action == "update") {
                                    return Url::to(['employee/update', 'id' => $model->id, 'mou' => $_GET['id']]);

                                }
                                if ($action == "delete") {
                                    return Url::to(['employee/delete', 'id' => $model->id, 'mou' => $_GET['id']]);
                                }
                            }
                        },
                        'buttons' => [

                            //buttons

                            'view' => function ($url, $model) {
                                if($model->status != 2){
                                    return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('yii', 'View'),

                                    ]);
                                } else {
                                    return "";
                                }
                            },

                            'update' => function ($url,$model) {
                                if($model->status != 2){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'Update'),]);
                                } else {
                                    return "";
                                }


                            },

                            'delete' => function ($url,$model) {
                                if($model->status != 2){
                                    return  Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('yii', 'Delete'),]);
                                } else {
                                    return "";
                                }


                            },




                        ]


                    ],
                ],
            ]); ?>

        </div>
        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดคำร้อง', ['view', 'id' => $model->id], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 1], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('รายงาน', ['viewreport', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งเข้า', ['pdfbtin/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสาร บต. 13', ['pdfbt13/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจนายจ้าง', ['companydoc/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจลูกจ้าง', ['empdoc5/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>

            <br>
            <?= Html::a('พิมพ์คำร้อง ', [$model->nationality->url.'/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('แก้ไขคำร้อง', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>

</div>
