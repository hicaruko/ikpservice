// * Here is is the code snippet to initialize Firebase Messaging in the Service
// * Worker when your app is not hosted on Firebase Hosting.
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"
);
// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: "AIzaSyAzKKifW-uYj3_P37sX5LHWTnkeEd7KBJw",
  authDomain: "ikpservice-62544.firebaseapp.com",
  databaseURL: "https://ikpservice-62544-default-rtdb.firebaseio.com",
  projectId: "ikpservice-62544",
  storageBucket: "ikpservice-62544.appspot.com",
  messagingSenderId: "1004205395694",
  appId: "1:1004205395694:web:d3814c8366c79523eaedff",
  measurementId: "G-2BQZLHMFC6",
});
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
var initMessaging = firebase.messaging();

//Background push notification handler. It fires up when the browser or web page in which push notification is activated are closed.
initMessaging.setBackgroundMessageHandler(function (payload) {
  // console.log("Background message Received:", payload);
  // const notificationTitle = payload.notification.title;
  // const notificationBody = payload.notification.body;
  //
  // const notificationOptions = {
  //   body: notificationBody,
  //   icon: "notification-icon.png",
  // };
  // return self.registration.showNotification(
  //     notificationTitle,
  //     notificationOptions
  // );
});

//Displays incoming push notification
self.addEventListener("push", function (event) {
  console.log("Push Notification Received.");
  console.log('Push Notification had this data: ',event);

  var eventData = event.data.text();
  var obj = JSON.parse(eventData); //Parse the received JSON object.

  const title = obj.notification.title;
  const options = {
    body: obj.notification.body,
    icon: "notification-icon.png",
    badge: "notification-icon.png",
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

//Take action when a user clicks on the received notification.
self.addEventListener("notificationclick", function (event) {
  event.notification.close();
  // event.waitUntil(
  //     clients.openWindow("https://www.facebook.com/rakeshdadamatti")
  // );
});

//Push subscription change
self.addEventListener("pushsubscriptionchange", function (event) {
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  event.waitUntil(
      self.registration.pushManager
          .subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey,
          })
          .then(function (newSubscription) {
            console.log(
                "Push subscription is changed. New Subscription is: ",
                newSubscription
            );
          })
  );
});
