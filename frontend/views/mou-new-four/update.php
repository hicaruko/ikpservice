<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */

$this->title = 'Update ครบ 4 ปี: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'ครบ 4 ปี', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mou-update">


    <?= $this->render('_form', [
        'model' => $model,
        'isUpdate' => 1,
    ]) ?>

</div>
