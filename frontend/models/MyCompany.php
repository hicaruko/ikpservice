<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "my_company".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $address_no
 * @property string $soi
 * @property string $soi_en
 * @property string $road
 * @property string $road_en
 * @property string $districts
 * @property string $districts_en
 * @property string $sub_districts
 * @property string $sub_districts_en
 * @property string $province
 * @property string $province_en
 * @property string $phone
 * @property string $license
 * @property string $first_name
 * @property string $first_name_en
 * @property string $last_name
 * @property string $last_name_en
 * @property string $position
 * @property string $position_en
 * @property int $prefix_id
 *
 * @property Prefix $prefix
 */
class MyCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'my_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prefix_id'], 'required'],
            [['prefix_id'], 'integer'],
            [['name', 'name_en', 'address_no', 'soi', 'soi_en', 'road', 'road_en', 'districts', 'districts_en', 'sub_ districts', 'sub_ districts_en', 'province', 'province_en', 'phone', 'license', 'first_name', 'first_name_en', 'last_name', 'last_name_en', 'position', 'position_en'], 'string', 'max' => 255],
            [['prefix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prefix::className(), 'targetAttribute' => ['prefix_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_en' => 'Name En',
            'address_no' => 'Address No',
            'soi' => 'Soi',
            'soi_en' => 'Soi En',
            'road' => 'Road',
            'road_en' => 'Road En',
            'districts' => 'Districts',
            'districts_en' => 'Districts En',
            'sub_ districts' => 'Sub Districts',
            'sub_ districts_en' => 'Sub Districts En',
            'province' => 'Province',
            'province_en' => 'Province En',
            'phone' => 'Phone',
            'license' => 'License',
            'first_name' => 'First Name',
            'first_name_en' => 'First Name En',
            'last_name' => 'Last Name',
            'last_name_en' => 'Last Name En',
            'position' => 'Position',
            'position_en' => 'Position En',
            'prefix_id' => 'Prefix ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefix()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id']);
    }

    /**
     * {@inheritdoc}
     * @return MyCompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MyCompanyQuery(get_called_class());
    }
}
