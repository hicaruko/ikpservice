<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\Mou;
use app\models\Nationality;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Yii;

class PdfssssController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function Convert($amount_number)
        {
            $amount_number = number_format($amount_number, 2, ".","");
            $pt = strpos($amount_number , ".");
            $number = $fraction = "";
            if ($pt === false)
                $number = $amount_number;
            else
            {
                $number = substr($amount_number, 0, $pt);
                $fraction = substr($amount_number, $pt + 1);
            }

            $ret = "";
            $baht = $this->ReadNumber ($number);
            if ($baht != "")
                $ret .= $baht . "บาท";

            $satang = $this->ReadNumber($fraction);
            if ($satang != "")
                $ret .=  $satang . "สตางค์";
            else
                $ret .= "ถ้วน";
            return $ret;
        }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";
        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }


    public function actionIndex()
    {
        $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);
        $pdfs = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";

        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/MOULaosP12.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table name='footer' width=\"100%\">
           <tr>
             <td style='font-size: 16px; padding-bottom: -30px;' align=\"left\">".$mou->code." Demand</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $tplIdx = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($pageNo);
            if($pageNo == 1) {  // 1

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 16;
                $start = 29;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 23;
                $start = 22;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 29;
                $start = 42;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 36;
                $start = 32;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->businessType->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 42;
                $start = 19;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.' '.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 48;
                $start = 19;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->telephone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 55;
                $start = 30;
                $end =110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->agent->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 48;
                $start = 168;
                $end =189;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 56;
                $start = 158;
                $end =189;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 62;
                $start = 158;
                $end =189;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">ช '.$mou->male.' ญ '.$mou->female.' รวม '.($mou->male+$mou->female).' คน </div>
', $start, $y1, $end-$start, 90, 'auto');


            } //1

            if($pageNo == 2){  // 2

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


                $y1 = 24;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title.'</div>
', 78, $y1, 32, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title_en.'</div>
', 132, $y1, 25, 90, 'auto');

                $y1 = 43;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name.'</div>
', 20, $y1, 80, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name_en.'</div>
', 107, $y1, 96, 90, 'auto');

                //   company type
                $y2 = 49;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->businessType->title.'</div>
', 48, $y2, 52, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->businessType->title_en.'</div>
', 134, $y2, 69, 90, 'auto');

                //   no moo soi
                $y3 = 49+7;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.'</div>
', 26, $y3, 13, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->moo.'</div>
', 46, $y3, 10, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->soi.'</div>
', 71, $y3, 29, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.'</div>
', 125, $y3, 17, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->moo.'</div>
', 150, $y3, 8, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->soi_n.'</div>
', 164, $y3, 39, 90, 'auto');

                // line 4
                $y4 = $y3+6;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->road.'</div>
', 26, $y4, 30, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->subdistricts.'</div>
', 73, $y4, 25, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->road_en.'</div>
', 115, $y4, 36, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->subdistricts_en.'</div>
', 169, $y4, 34, 90, 'auto');

                // road
                $y5 = $y4+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->districts.'</div>
', 34, $y5, 22, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->province.'</div>
', 66, $y5, 33, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->districts_en.'</div>
', 118, $y5, 33, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->province_en.'</div>
', 165, $y5, 38, 90, 'auto');

// province
                $y6 = $y5+($y2-$y1);
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->telephone.'</div>
', 31, $y6, 25, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->fax.'</div>
', 67, $y6, 32, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->telephone.'</div>
', 124, $y6, 27, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->fax.'</div>
', 158, $y6, 45, 90, 'auto');

// province
                $y66 = $y6+($y5-$y4);
                $y7 = $y66+($y5-$y4);
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->name.'</div>
', 20, $y7 , 79, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->name.'</div>
', 108, $y7 , 95, 90, 'auto');


                // address 1
                $y8 = $y7+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->address.'</div>
', 27, $y8 , 72, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->address.'</div>
', 120, $y8 , 83, 90, 'auto');
                // address 2
                $y9 = $y8+6;
//                $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyle.'">testt</div>
//', 27, $y9 , 72, 90, 'auto');

//
//                $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyle.'">testt</div>
//', 120, $y9 , 83, 90, 'auto');

//date
                $y10 = $y9+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('d',strtotime($mou->document_date)).'</div>
', 54, $y10 , 11, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('m',strtotime($mou->document_date)).'</div>
', 67, $y10 , 14, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('Y',strtotime($mou->document_date)).'</div>
', 83, $y10 , 16, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('d',strtotime($mou->document_date)).'</div>
', 173, $y10 , 7, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('m',strtotime($mou->document_date)).'</div>
', 182, $y10 , 9, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('Y',strtotime($mou->document_date)).'</div>
', 193, $y10 , 10, 90, 'auto');


// amount number
                $y11 = $y10+6;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.($mou->male+$mou->female).'</div>
', 66, $y11 , 20, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.($mou->male+$mou->female).'</div>
', 140, $y11 , 15, 90, 'auto');

// number check
                $y12 = $y11+8;
                $pdf->WriteFixedPosHTML('
<div style="font-family:Arial">✓</div>
', 19, $y12 , 3, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="font-family:Arial">✓</div>
', 107, $y12 , 3, 90, 'auto');

// person amount
                $y13 = $y12+5.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->male.'</div>
', 44, $y13 , 9, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->female.'</div>
', 83, $y13 , 8, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->male.'</div>
', 120, $y13 , 11, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->female.'</div>
', 173, $y13 , 12, 90, 'auto');


                $y14 = $y13+44;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->qualification_age.'</div>
', 50, 168 , 10, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->qualification_height.'</div>
', 71, $y14 , 9, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->qualification_weight.'</div>
', 91, $y14 , 8, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->qualification_age.'</div>
', 134, $y14 , 9, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->qualification_height.'</div>
', 166, $y14 , 12, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->qualification_weight.'</div>
', 191, $y14 , 12, 90, 'auto');

                $y15 = $y14+13;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->address_no.'</div>
', 27, $y15 , 12, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->moo.'</div>
', 46, $y15 , 9, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->soi.'</div>
', 71, $y15 , 28, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->address_no.'</div>
', 125, $y15 , 19, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->moo.'</div>
', 151, $y15 , 9, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->soi_n.'</div>
', 166, $y15 , 37, 90, 'auto');

                $y16 = $y15+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->road.'</div>
', 27, $y16 , 28, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->subdistricts.'</div>
', 72, $y16 , 21, 90, 'auto');




                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->road_en.'</div>
', 116, $y16 , 29, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->subdistricts_en.'</div>
', 163, $y16 , 40, 90, 'auto');


                $y17 = $y16+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->districts.'</div>
', 35, $y17 , 20, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->province.'</div>
', 65, $y17 , 34, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->districts_en.'</div>
', 118, $y17 , 28, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->province_en.'</div>
', 160, $y17 , 43, 90, 'auto');


                $y18 = $y17+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->preriod_year.'</div>
', 43, $y18 , 12, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->preriod_month.'</div>
', 58, $y18 , 14, 90, 'auto');



                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->preriod_year.'</div>
', 142, $y18 , 13, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->preriod_month.'</div>
', 167, $y18 , 13, 90, 'auto');

                $y19 = $y18+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->wage_per_day.'</div>
', 29, $y19 , 18, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->wage_per_day.'</div>
', 117, $y19 , 14, 90, 'auto');

                $y20 = $y19+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->work_time.'</div>
', 49, $y20 , 14, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->work_time.'</div>
', 157, $y20 , 13, 90, 'auto');


                $y21 = $y20+6;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->week_holiday.'</div>
', 49, $y21 , 5, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->annual.'</div>
', 90, $y21 , 4, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->week_holiday.'</div>
', 131, $y21 , 14, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->annual.'</div>
', 181, $y21 , 11, 90, 'auto');

                $y22 = $y21+6;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">ตามกฏหมายคุม้ครองแรงงาน</div>
', 30, 225 , 99-57, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">The Labour Protection Law</div>
', 124, 225 , 181-136, 90, 'auto');

                $y23 = $y22+13;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->danImmigration->title.'</div>
', 20, $y23 , 79, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->danImmigration->title_en.'</div>
', 108, $y23 , 95, 90, 'auto');

                $y24 = $y23+13;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->training->title.'</div>
', 42, $y24 , 57, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->training->title_en.'</div>
', 119, $y24 , 66, 90, 'auto');

                $y25 = $y24+25;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', 43, $y25 , 57, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', 132, $y25 , 68, 90, 'auto');

                $y26 = $y25+6;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('d',strtotime($mou->document_date)).'</div>
', 48, $y26 , 8, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('m',strtotime($mou->document_date)).'</div>
', 63, $y26 , 20, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.(date('Y',strtotime($mou->document_date))+543).'</div>
', 88, $y26 , 12, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('d',strtotime($mou->document_date)).'</div>
', 140, $y26 , 7, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('m',strtotime($mou->document_date)).'</div>
', 157, $y26 , 15, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('Y',strtotime($mou->document_date)).'</div>
', 180, $y26 , 12, 90, 'auto');


            }   // 2
            if($pageNo == 3){  //3
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);




                $y1 = 39;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThai($mou->document_date).'</div>
', 22, $y1, 68-22, 90, 'auto');

                $y1 = 39;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.date('d M-Y',strtotime($mou->document_date)).'</div>
', 120, $y1, 166-120, 90, 'auto');



                $y1 = 61;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name.'</div>
', 16, $y1, 82, 90, 'auto');

                $y2 = 60;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name_en.'</div>
', 112, $y2, 82, 90, 'auto');


                $y1 = $y1+6.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.'</div>
', 31, $y1+1, 67, 90, 'auto');
                $y2 = $y2+7.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.'</div>
', 132, $y2, 62, 90, 'auto');

                $y1 = $y1+8.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->moo.'</div>
', 23, $y1, 21, 90, 'auto');


                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->soi.'</div>
', 52, $y1, 46, 90, 'auto');




                $y2 = $y2+7.5;

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->moo.'</div>
', 120, $y2, 20, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->soi_n.'</div>
', 157, $y2, 37, 90, 'auto');

                $y1 = $y1+7.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->road.'</div>
', 23, $y1, 29, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->subdistricts.'</div>
', 61, $y1, 37, 90, 'auto');



                $y2 = $y2+7.5;

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->road_en.'</div>
', 120, $y2, 25, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->subdistricts_en.'</div>
', 163, $y2, 31, 90, 'auto');

                $y1 = $y1+7.5;;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->districts.'</div>
', 33, $y1, 26, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->province.'</div>
', 70, $y1, 98-70, 90, 'auto');


                $y2 = $y2+7.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->districts_en.'</div>
', 123, $y2, 24, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->province_en.'</div>
', 162, $y2, 194-162, 90, 'auto');

                $y1 = $y1+7.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->telephone.'</div>
', 28, $y1, 52-28, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->fax.'</div>
', 69, $y1, 98-69, 90, 'auto');


                $y2 = $y2+7.5;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->telephone.'</div>
', 117, $y2, 147-117, 90, 'auto');
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->fax.'</div>
', 154, $y2, 194-154, 90, 'auto');

                $y1 = $y1+14;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->name.'</div>
', 16, $y1, 98-16, 90, 'auto');




                $y2 = $y2+14;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->name.'</div>
', 112, $y2, 194-112, 90, 'auto');

                $y1 = $y1+7;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->address.'</div>
', 23, $y1, 98-23, 90, 'auto');


                $y2 = $y2+7;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->address.'</div>
', 125, $y2, 194-125, 90, 'auto');

                $y1 = $y1+8;
//                $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyle.'">ที่อยู่ 2</div>
//', 16, $y1, 98-16, 90, 'auto');


                $y2 = $y2+8;
//                $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyle.'">ที่อยู่ 2</div>
//', 112, $y2, 194-112, 90, 'auto');

                $y1 = $y1+8;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->license.'</div>
', 39, $y1, 98-39, 90, 'auto');


                $y2 = $y2+8;

                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->license.'</div>
', 130, $y2, 194-130, 90, 'auto');

                $y1 = $y1+32;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title.'</div>
', 35, $y1, 72-35, 90, 'auto');


                $y2 = $y2+32;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title_en.'</div>
', 112, $y2, 131-112, 90, 'auto');




                $y1 = $y1+65;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', 36, $y1, 83-36, 90, 'auto');

                $y2 = $y2+65;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', 132, $y2, 179-132, 90, 'auto');

                $y1 = $y1+7;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->position.'</div>
', 40, $y1, 84-40, 90, 'auto');


                $y2 = $y2+7;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->position_en.'</div>
', 137, $y2, 181-137, 90, 'auto');

                $y1 = $y1+19;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', 36, $y1, 83-36, 90, 'auto');

                $y2 = $y2+19;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->witness->prefix->title_en.$mou->witness->first_name_en.' '.$mou->witness->last_name_en.'</div>
', 132, $y2, 179-132, 90, 'auto');



            }   // 3
            if($pageNo == 4){ // 4
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);



                $y1 = 41;
                $start = 72;
                $end = 178;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 47;
                $start = 74;
                $end = 189;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 53;
                $start = 90;
                $end = 144;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 53;
                $start = 154;
                $end = 185;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.($mou->male+$mou->female).'</div>
', $start, $y1, $end-$start, 90, 'auto');


                 $y1 = 87;
                 $start = 33;
                $end = 36;
                $pdf->WriteFixedPosHTML('
<div style="font-family:Arial">✓</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 86;
                $start = 85;
                $end = 190;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">บริษัท นำคนต่างด้าวมาทำงานในประเทศ ไอเคพี ซุปเปอร์ เซอร์วิส จำกัด</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 107.5;
                $start = 167;
                $end = 190;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 113;
                $start = 24;
                $end = 119;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->agent->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 150;
                $start = 67;
                $end = 120;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 160;
                $start = 66;
                $end = 75;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 160;
                $start = 78;
                $end = 106;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 160;
                $start = 112;
                $end = 121;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'year').'</div>
', $start, $y1, $end-$start, 90, 'auto');



            }   // 4
            if($pageNo == 5){ // 5



                $blockStyle = "text-align: center;font-family: sarabun;font-size: 18px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);



                $y1 = 64;
                $start = 91;
                $end = 104;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 64;
                $start = 111;
                $end = 135;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 64;
                $start = 141;
                $end = 156;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$this->DateThaiType($mou->document_date,'year').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 58;
                $start = 92;
                $end = 131;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 70;
                $start = 67;
                $end = 140;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 76;
                $start = 23;
                $end = 119;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 76;
                $start = 131;
                $end = 182;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->position.'</div>
', $start, $y1, $end-$start, 90, 'auto');




                $y1 = 82;
                $start = 42;
                $end = 182;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.' '.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 88;
                $start = 98;
                $end = 131;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->telephone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 88;
                $start = 149;
                $end = 181;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->company->businessType->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 132;
                $start = 117;
                $end = 138;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 138;
                $start = 108;
                $end = 129;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.($mou->male+$mou->female).'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 138;
                $start = 167;
                $end = 180;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->male.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 144;
                $start = 41;
                $end = 55;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->female.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 223;
                $start = 112;
                $end = 138;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->wage_per_day.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 231;
                $start = 118;
                $end = 133;
                $paydate = explode(",",$mou->paydate);
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$paydate[0].'</div>
', $start, $y1, $end-$start, 90, 'auto');

              if(count($paydate) > 1){
                  $y1 = 231;
                  $start = 139;
                  $end = 152;
                  $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$paydate[1].'</div>
', $start, $y1, $end-$start, 90, 'auto');
              }




                $y1 = 243;
                $start = 74;
                $end = 187;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.' '.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 249;
                $start = 113;
                $end = 131;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 255;
                $start = 104;
                $end = 122;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');



            }   // 5
            if($pageNo == 6){ // 6
                $blockStyle = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 18px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            }   // 6
            if($pageNo == 7){ // 7
                $blockStyle = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 18px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            }   // 7
            if($pageNo == 8){ // 8
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 18px";
                $pdf -> AddPage();
                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                 $y1 = 107;
                 $start = 29;
                 $end = 73;
                 $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                 $y1 = 107;
                 $start = 118;
                 $end = 163;
                 $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">นางสาววิไลพร งามเจริญ</div>
', $start, $y1, $end-$start, 90, 'auto');

                 $y1 = 138;
                 $start = 29;
                 $end = 73;
                 $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                 $y1 = 138;
                 $start = 118;
                 $end = 163;
                 $pdf->WriteFixedPosHTML('
<div style="'.$blockStyle.'">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


             }   // 8
            if($pageNo == 9) { // 9

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 46;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->province.'</div>
', 24, $y1, 26, 90, 'auto');
                $y2 = 42;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->province_en.'</div>
', 118, $y2, 23, 90, 'auto');

                $y1 = 57;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->name.'</div>
', 20, $y1, 79, 90, 'auto');
                $y2 = 47;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->name_en.'</div>
', 112, $y2, 77, 90, 'auto');

                $y1 = 67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.'</div>
', 34, $y1, 99-34, 90, 'auto');
                $y2 = $y2+5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.','.$mou->company->moo.','.$mou->company->soi_n.' </div>
', 124, $y2, 65, 90, 'auto');



                $y1 = 84;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', 20, $y1, 79, 90, 'auto');

                $y2 = 57;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts_en.','.$mou->company->districts_en.','.$mou->company->province_en.','.$mou->company->zipcode.'</div>
', 112, $y2, 77, 90, 'auto');




                $y2 = 77;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->province_en.'</div>
', 115, $y2, 30, 90, 'auto');

                $y2 = 81.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->name_en.'</div>
', 112, $y2, 77, 90, 'auto');
                $y2 = $y2+6.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.','.$mou->company->moo.','.$mou->company->soi_n.' </div>
', 128, $y2, 189-128, 90, 'auto');

                $y2 = $y2+4;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->subdistricts_en.','.$mou->company->districts_en.','.$mou->company->province_en.','.$mou->company->zipcode.'</div>
', 112, $y2, 77, 90, 'auto');


                $y1 = 172;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">งานกรรมกร</div>
', 72, $y1, 100-72, 90, 'auto');

                $y1 = 182;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 33, $y1, 62-33, 90, 'auto');

                $y1 = 192;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 54, $y1, 76-54, 90, 'auto');


                $y2 = 167;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">Labour</div>
', 140, $y2, 193-140, 90, 'auto');

                $y2 = $y2+5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 133, $y2, 161-133, 90, 'auto');

                $y2 = $y2+4;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 151, $y2, 167-151, 90, 'auto');




                $y2 = $y2+10;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">Labour</div>
', 158, $y2, 192-158, 90, 'auto');

                $y2 = $y2+5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->wage_per_day.'</div>
', 113, $y2, 148-113, 90, 'auto');

                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->paydate.'</div>
', 168, $y2, 179-168, 90, 'auto');

                $y2 = 211;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 170, $y2, 19, 90, 'auto');


                $y1 = 212;

                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 61, $y1, 79-61, 90, 'auto');




                $y1 = 232;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.','.$mou->moo.','.$mou->soi.','.$mou->subdistricts.'</div>
', 35, $y1, 64, 90, 'auto');

                $y1 = 247.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->districts.','.$mou->province.','.$mou->zipcode.'</div>
', 20, $y1, 79, 90, 'auto');


                $y2 = 224;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.','.$mou->moo.','.$mou->soi_n.','.$mou->subdistricts_en.','.$mou->districts_en.','.$mou->province_en.','.$mou->zipcode.'</div>
', 112, $y2, 77, 90, 'auto');

                $y2 = 232+4;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->preriod_year.'.'.$mou->preriod_month.'</div>
', 163, $y2, 176-163, 90, 'auto');

                $y2 = 251;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->address_no.','.$mou->moo.','.$mou->soi_n.','.$mou->subdistricts_en.'</div>
', 120, $y2, 189-120, 90, 'auto');

                $y2 = $y2+5.5;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->districts_en.','.$mou->province_en.','.$mou->zipcode.'</div>
', 112, $y2, 189-112, 90, 'auto');


            }  // 9

            if($pageNo ==  10) { // 10

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                //left

                $y1 = 25;
                $start = 61;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 35;
                $start = 49;
                $end =71;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 65;
                $start = 51;
                $end =68;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 84;
                $start = 44;
                $end =60;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');


                //rigth

                $y1 = 25;
                $start = 171;
                $end =185;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 30;
                $start = 130;
                $end =144;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 35;
                $start = 168;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->work_time.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 40;
                $start = 141;
                $end =149;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.(7-$mou->week_holiday).'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 60;
                $start = 114;
                $end =122;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 70;
                $start = 156;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 80;
                $start = 114;
                $end =122;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->week_holiday.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 94;
                $start = 156;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">18</div>
', $start, $y1, $end-$start, 90, 'auto');



            } // 10
            if($pageNo ==  11){ // 11
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            } // 11
            if($pageNo ==  12){ // 11
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

            }  // 12
            if($pageNo ==  13){ // 13
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

// left

                $y1 = 92;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 151;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 176;
                $start = 31;
                $end =83;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

// right

                $y1 = 87;
                $start = 128;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 112;
                $start = 128;
                $end =175;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 172;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title_en.$mou->witness->first_name_en.' '.$mou->witness->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 192;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title_en.$mou->witnessIdTwo->first_name_en.' '.$mou->witnessIdTwo->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 212;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title_en.$mou->witness->first_name_en.' '.$mou->witness->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 233;
                $start = 132;
                $end =171;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title_en.$mou->witnessIdTwo->first_name_en.' '.$mou->witnessIdTwo->last_name_en.'</div>
', $start, $y1, $end-$start, 90, 'auto');




            } // 13

//            if($pageNo == 14){ // 14
//                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
//                $pdf -> AddPage();
//                $pdf ->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);
//                $y1 = 38;
//                $start = 89;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">9/74-75 M 3 Don Kai Kai, Or Krathum Baen District, Samut Sakhon 74110</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 45;
//                $start = 89;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">9/74-75 หมู่ 3 ตำบลดอนไก่ดี อำเภอกระทุ่มแบน จ.สมุทรสาคร 74110</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 52;
//                $start = 139;
//                $end =149;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.(date('d',strtotime($mou->document_date))).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 52;
//                $start = 160;
//                $end =182;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.(date('M',strtotime($mou->document_date))).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 52;
//                $start = 189;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.(date('Y',strtotime($mou->document_date))).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 58;
//                $start = 139;
//                $end =149;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.(date('d',strtotime($mou->document_date))).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 58;
//                $start = 157;
//                $end =180;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$this->DateThaiType($mou->document_date,'month').'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 58;
//                $start = 189;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.(date('Y',strtotime($mou->document_date))+543).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 65;
//                $start = 97;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name_en.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 71;
//                $start = 97;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 78;
//                $start = 17;
//                $end =149;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title_en.$mou->company->first_name_en.' '.$mou->company->last_name_th.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 78;
//                $start = 163;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->position_en.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 84;
//                $start = 17;
//                $end =149;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 84;
//                $start = 163;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->position.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 91;
//                $start = 59;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyleSmall . '">Ms. Wilaiporn Ngamcharoen</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 91;
//                $start = 173;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">001/2562</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 97;
//                $start = 34;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyleSmall . '">นางสาววิไลพร งามเจริญ</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 97;
//                $start = 169;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">001/2562</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                ///////
//                $y1 = 104;
//                $start = 17;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->userId1->prefix->title_en.$mou->userId1->first_name_en.' '.$mou->userId1->last_name_en.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 104;
//                $start = 173;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->userId1->license.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 110;
//                $start = 17;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->userId1->prefix->title.$mou->userId1->first_name.' '.$mou->userId1->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 110;
//                $start = 173;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->userId1->license.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                ///////
//                $y1 = 116;
//                $start = 17;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->user->prefix->title_en.$mou->user->first_name_en.' '.$mou->user->last_name_en.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 116;
//                $start = 173;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->user->license.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 123;
//                $start = 17;
//                $end =145;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 123;
//                $start = 173;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->user->license.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                ///
//                $y1 = 129;
//                $start = 69;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">Licensee</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 136;
//                $start = 46;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">ผูร้บัอนุญาต</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                ////
//                $y1 = 142;
//                $start = 36;
//                $end =152;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">Company bringing foreigners to work in IKP Super Service Company Limited</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 142;
//                $start = 159;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">-</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                ////
//                $y1 = 148;
//                $start = 36;
//                $end =152;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">บริษัท นำคนต่างด้าวมาทำงานในประเทศ ไอเคพี ซุปเปอร์ เซอร์วิส จำกัด</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 148;
//                $start = 159;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">-</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                //
//
//                $y1 = 155;
//                $start = 29;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">9/74-75 M 3 Don Kai Kai, Or Krathum Baen District, Samut Sakhon 74110</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 161;
//                $start = 29;
//                $end =199;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">9/74-75 หมู่ 3 ตำบลดอนไก่ดี อำเภอกระทุ่มแบน จ.สมุทรสาคร 74110</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 168;
//                $start = 105;
//                $end =157;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->nationality->title_en.'       '.($mou->male+$mou->female).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 180;
//                $start = 95;
//                $end =124;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->nationality->title.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 180;
//                $start = 136;
//                $end =153;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.($mou->male+$mou->female).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 193;
//                $start = 49;
//                $end =153;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">Ms. Wilaiporn Ngamcharoen ,'.$mou->userId1->prefix->title_en.$mou->userId1->first_name_en.' '.$mou->userId1->last_name_en.' ,'.$mou->user->prefix->title_en.$mou->user->first_name_en.' '.$mou->user->last_name_en.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 206;
//                $start = 22;
//                $end =153;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">นางสาววิไลพร งามเจริญ ,'.$mou->userId1->prefix->title.$mou->userId1->first_name.' '.$mou->userId1->last_name.' ,'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.' </div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 233;
//                $start = 23;
//                $end =79;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 240;
//                $start = 35;
//                $end =92;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->position.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 259;
//                $start = 23;
//                $end =79;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//                $y1 = 278;
//                $start = 23;
//                $end =79;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//
//                $y1 = 233;
//                $start = 117;
//                $end =175;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">นางสาววิไลพร งามเจริญ</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//                $y1 = 252;
//                $start = 117;
//                $end =175;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->userId1->prefix->title.$mou->userId1->first_name.' '.$mou->userId1->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//
//                $y1 = 272;
//                $start = 117;
//                $end =175;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//
//
//
//
//            }   // 14

            if($pageNo ==  15){ // 15
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 101;
                $start = 59;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 109;
                $start = 28;
                $end =67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->phone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

  $y1 = 109;
                $start = 154;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 210;
                $start = 74;
                $end =142;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 210;
                $start = 158;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 217;
                $start = 34;
                $end =101;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 243;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">นางสาววิไลพร งามเจริญ</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 266;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 243;
                $start = 117;
                $end =160;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 266;
                $start = 117;
                $end =160;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



            }  // 15

            if($pageNo ==  16){ // 16
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $y1 = 79;
                $start = 46;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 86;
                $start = 104;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $dis_name =" อำเภอ";
                $sub_disname = " ตำบล";
                if($mou->company->province == "กรุงเทพมหานคร"){
                    $dis_name =" เขต";
                    $sub_disname = " แขวง";
                }


                $y1 = 94;
                $start = 29;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="'.$blockStyleSmall.'">'.$mou->company->address_no.' หมู่ '.$mou->company->moo.' '.$mou->company->soi.$sub_disname.$mou->company->subdistricts.$dis_name.$mou->company->districts.' จังหวัด'.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 101;
                $start = 122;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->telephone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 133;
                $start = 97;
                $end =122;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 133;
                $start = 133;
                $end =152;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.($mou->male+$mou->female).'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 256;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 279;
                $start = 41;
                $end =81;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 256;
                $start = 117;
                $end =160;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">นางสาววิไลพร งามเจริญ</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 279;
                $start = 117;
                $end =160;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



            }  // 16

        }

//
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 90, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 129, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  50, 1, 90, 'auto');
//
//        }
//
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//        }
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//        }

        $pdf->AddPage();
        $pdf->Image(Yii::$app->basePath.'/web/'.$mou->agent->license_file, 0, 0, 210, 297, 'jpg', '', true, false);
//
//        $pdf->AddPage();
//        $pdf->Image(Yii::$app->basePath.'/web/doc/nj-17.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
//        $y1 = 137;
//        $start = 48;
//        $end =179;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 117;
//        $end =125;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->number_price.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 161;
//        $end =183;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//     $y1 = 151;
//        $start = 30;
//        $end =175;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$this->Convert($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 238;
//        $start = 39;
//        $end =85;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');



//        $y1 = 61;
//        $start = 32;
//        $end =169;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->telephone.'</div>
//', $start, $y1, $end-$start, 90, 'auto');


//        for ($i=0;$i<310;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 90, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 129, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  50, 1, 90, 'auto');
//
//        }
//
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//        }
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
