<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CauseOut */

$this->title = 'Create Cause Out';
$this->params['breadcrumbs'][] = ['label' => 'Cause Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cause-out-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
