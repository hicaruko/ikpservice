<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'name_en') ?>

    <?= $form->field($model, 'address_no') ?>

    <?php // echo $form->field($model, 'moo') ?>

    <?php // echo $form->field($model, 'soi') ?>

    <?php // echo $form->field($model, 'road') ?>

    <?php // echo $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'business_type_id') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'first_name') ?>

    <?php // echo $form->field($model, 'first_name_en') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'last_name_th') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'position_en') ?>

    <?php // echo $form->field($model, 'prefix_id') ?>

    <?php // echo $form->field($model, 'provinces_id') ?>

    <?php // echo $form->field($model, 'districts_id') ?>

    <?php // echo $form->field($model, 'subdistricts_id') ?>

    <?php // echo $form->field($model, 'license') ?>

    <?php // echo $form->field($model, 'license_en') ?>

    <?php // echo $form->field($model, 'soi_n') ?>

    <?php // echo $form->field($model, 'road_en') ?>

    <?php // echo $form->field($model, 'province') ?>

    <?php // echo $form->field($model, 'province_en') ?>

    <?php // echo $form->field($model, 'districts') ?>

    <?php // echo $form->field($model, 'districts_en') ?>

    <?php // echo $form->field($model, 'subdistricts') ?>

    <?php // echo $form->field($model, 'subdistricts_en') ?>

    <?php // echo $form->field($model, 'zipcode') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
