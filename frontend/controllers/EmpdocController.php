<?php

namespace frontend\controllers;

use app\models\Authorize;
use app\models\Company;
use app\models\Employee;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;

use app\models\User;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class EmpdocController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }
    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        if($_GET['type'] == 1){
            $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
            $Emp = Employee::find()->where(['mou_id'=>$mou->id])->all();
        } else {
            $mou = MouNew::find()->where(['id'=>$_GET['id']])->one();
            $Emp = EmployeeInform::find()->where(['mou_new_id'=>$mou->id])->all();

        }

        $mou_user = User::find()->where(['id'=>$_GET['uid']])->one();
        $authorize = Authorize::find()->where(['user_id'=>$_GET['uid'],'mou_id'=>$_GET['id'],'type'=>$_GET['type']])->one();


        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleLeft = "text-align: left;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";

        $pageCounts = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/btdoc.pdf');

//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);


        for ($pageNo = 1; $pageNo <= $pageCounts; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            if($pageNo ==  1){ // 15
                $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


                $y1 = 69;
                $start = 93;
                $end =107;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 69;
                $start = 116;
                $end =144;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 69;
                $start = 150;
                $end =168;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'year').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 101;
                $start = 59;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 109;
                $start = 28;
                $end =67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->phone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

  $y1 = 109;
                $start = 154;
                $end =181;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 210;
                $start = 73;
                $end =141;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 210;
                $start = 156;
                $end =183;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->code.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 217;
                $start = 33;
                $end =100;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 242;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 265;
                $start = 44;
                $end =82;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$authorize->witness->prefix->title.$authorize->witness->first_name.' '.$authorize->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 265;
                $start = 119;
                $end =159;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$authorize->witnessIdTwo->prefix->title.$authorize->witnessIdTwo->first_name.' '.$authorize->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


            }  // 15



        }

//
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 90, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 129, 1, 90, 'auto');
//
//        }
//        for ($i=0;$i<210;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $cdd = $i;
//            if($i > 100 && $i<200){
//                $cdd = $i -100;
//            }  else if($i > 200 ){
//                $cdd = $i -200;
//            }
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  50, 1, 90, 'auto');
//
//        }
//$paydate
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//        }
//
//        for ($i=0;$i<295;$i++){
//            $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//            $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//        }


//        $ttt = htmlentities("ใช้ดำเนินกา แจ้งคนงานต่างด้าวเข้าทำงาน <br/>
//ตามเอกสารแจ้งเข้าแรงงาน เลขที่ LE20070024   <br/>
//ให้กับนายจ้าง บริษัท xxxxxx   <br/>
//โดยมอบผู้รับมอบอำนาจดำเนินการ ชื่อ xxxxx");
//        $wm = \Mpdf\Utils\UtfString::strcode2utf($ttt);

//

//        $pdf->AddPage();
//        $pdf->Image(Yii::$app->basePath.'/web/doc/nj-17.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
//        $y1 = 137;
//        $start = 48;
//        $end =179;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 117;
//        $end =125;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->number_price.'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 144;
//        $start = 161;
//        $end =183;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//        $y1 = 151;
//        $start = 30;
//        $end =175;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$this->Convert($mou->number_price*$mou->price).'</div>
//', $start, $y1, $end-$start, 90, 'auto');
//
//        $y1 = 238;
//        $start = 39;
//        $end =85;
//        $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
//', $start, $y1, $end-$start, 90, 'auto');


        foreach ($Emp as $key=>$value){

            $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/BT_EM_L_new44.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

            $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
            $pdf->defaultfooterline = 0;
            $pdf->defaultfooterfontstyle='B';
            $pdf->defaultfooterline=0;
            $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
            $pdf->SetFooter($footer);

            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

                $tplIdx = $pdf->importPage($pageNo);

                $size = $pdf->getTemplateSize($pageNo);

                if($pageNo ==  10){ // 7
                    $pdf->AddPage();
                    $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);



//


                    $dis_name =" อ.";
                    $sub_disname = " ต.";
                    if($mou->company->province == "กรุงเทพมหานคร"){
                        $dis_name =" เขต";
                        $sub_disname = " แขวง";
                    }

                    $y1 = 70;
                    $start = 115;
                    $end =179;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.' ม.'.$mou->company->moo.' '.$mou->company->soi.$sub_disname.$mou->company->subdistricts.$dis_name.$mou->company->districts.' จ.'.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                    $y1 = 78;
                    $start = 104;
                    $end =113;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                    $y1 = 78;
                    $start = 123;
                    $end =156;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                    $y1 = 78;
                    $start = 163;
                    $end =178;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($authorize->create_at,'year').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                    $y1 = 91;
                    $start = 77;
                    $end = 177;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 98;
                    $start = 38;
                    $end = 67;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->nationality->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $vaNum = $value->emp_number;
                    $y1 = 97;
                    $start = 110;
                    $end = 114;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $vaNum[0] . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 97;
                    $start = 117;
                    $end = 121;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $vaNum[1] .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 121;
                    $end = 125;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[2]. '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 126;
                    $end = 129;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'  .$vaNum[3]. '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 130;
                    $end = 133;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[4].'</div>
', $start, $y1, $end - $start, 90, 'auto');




                    $y1 = 97;
                    $start = 138;
                    $end = 141;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[5].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 97;
                    $start = 142;
                    $end = 146;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[6].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 97;
                    $start = 147;
                    $end = 151;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[7].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 97;
                    $start = 152;
                    $end = 155;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[8].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 97;
                    $start = 156;
                    $end = 161;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[9].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 163;
                    $end = 167;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[10].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 167;
                    $end = 172;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[11].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 97;
                    $start = 174;
                    $end = 177;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[12].'</div>
', $start, $y1, $end - $start, 90, 'auto');



                    $y1 = 212;
                    $start = 86;
                    $end =135;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end-$start, 90, 'auto');


                    $y1 = 118;
                    $start = 40;
                    $end =178;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">ต้องทำงาน</div>
', $start, $y1, $end-$start, 90, 'auto');



                    $y1 = 151;
                    $start = 79;
                    $end =178;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                    $y1 = 157;
                    $start = 41;
                    $end = 58;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->address_no .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 157;
                    $start = 66;
                    $end = 82;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->moo .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 157;
                    $start = 90;
                    $end = 133;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->soi .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 157;
                    $start = 141;
                    $end = 178;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->road .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 164;
                    $start = 45;
                    $end = 66;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->subdistricts .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 164;
                    $start = 84;
                    $end = 132;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->districts .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 164;
                    $start = 144;
                    $end = 178;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou_user->province .'</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 184;
                    $start = 68;
                    $end =137;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                    $y1 = 232;
                    $start = 86;
                    $end =135;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou_user->prefix->title.$mou_user->first_name.' '.$mou_user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                    $y1 = 252;
                    $start = 86;
                    $end =135;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$authorize->witness->prefix->title.$authorize->witness->first_name.' '.$authorize->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                    $y1 = 271;
                    $start = 86;
                    $end =135;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$authorize->witnessIdTwo->prefix->title.$authorize->witnessIdTwo->first_name.' '.$authorize->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');




                } // 9

            }

        }




        $pdf -> Output('myOwn.pdf', 'i');

    }

}
