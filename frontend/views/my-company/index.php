<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MyCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-company-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create My Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'name_en',
            'address_no',
            'soi',
            //'soi_en',
            //'road',
            //'road_en',
            //'districts',
            //'districts_en',
            //'sub_ districts',
            //'sub_ districts_en',
            //'province',
            //'province_en',
            //'phone',
            //'license',
            //'first_name',
            //'first_name_en',
            //'last_name',
            //'last_name_en',
            //'position',
            //'position_en',
            //'prefix_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
