<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MouSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mou';
$this->params['breadcrumbs'][] = $this->title;

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
$ActionColumn = ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update}'];
if($user->type == 0) {
    $ActionColumn = ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update} {delete}'];
}
$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}
?>
<div class="mou-index  ">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('สร้างคำร้อง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'id',
            'document_date',
            [
                'label' => 'ประเภท',
                'attribute'=>'type',
                'headerOptions' => ['width' => '80'],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->type == 1){
                        return 'New';
                    } else {
                        return 'Return';
                    }
                },
            ],
            'code',
//            'type',
            'nationality.title',
            'company.name',
            'male',
            'female',
            'update_date',

            [
                'label' => 'ยกเลิก',
                'attribute'=>'cancle',
                'headerOptions' => ['width' => '80'],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->cancle == 1){
                        return 'ยกเลิก';
                    } else {
                        return 'ปกติ';
                    }
                },
            ],
//            '',
            //'agent_id',
            //'grantee_id',
            //'qualification_age',
            //'qualification_height',
            //'qualification_weight',
            //'preriod_year',
            //'preriod_month',
            //'wage_per_day',
            //'week_holiday',
            //'annual',
            //'holiday_id',
            //'dan_Immigration_id',
            //'nationality_id',
            //'witness_id',
            //'witness_id_two',
            //'paydate',
            //'create_at',
            //'update_date',
            //'print',
            //'version',
            //'document_date',
            //'user_id',
            [
                'label' => 'Create By',
                'attribute'=>'user_id',
                'headerOptions' => ['width' => '80'],
                'filter'=>$datalists,
                'value' => function($model, $key, $index)
                {
                    $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                    if($datas){
                        return $datas->first_name." ".$datas->last_name;
                    }


                },
            ],
            $ActionColumn,
        ],
    ]); ?>
</div>
</div>
