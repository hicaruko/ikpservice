<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HistoryUpdate */

$this->title = 'Update History Update: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'History Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="history-update-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
