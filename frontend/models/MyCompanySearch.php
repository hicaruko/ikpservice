<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MyCompany;

/**
 * MyCompanySearch represents the model behind the search form of `app\models\MyCompany`.
 */
class MyCompanySearch extends MyCompany
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'prefix_id'], 'integer'],
            [['name', 'name_en', 'address_no', 'soi', 'soi_en', 'road', 'road_en', 'districts', 'districts_en', 'sub_ districts', 'sub_ districts_en', 'province', 'province_en', 'phone', 'license', 'first_name', 'first_name_en', 'last_name', 'last_name_en', 'position', 'position_en'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MyCompany::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prefix_id' => $this->prefix_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'address_no', $this->address_no])
            ->andFilterWhere(['like', 'soi', $this->soi])
            ->andFilterWhere(['like', 'soi_en', $this->soi_en])
            ->andFilterWhere(['like', 'road', $this->road])
            ->andFilterWhere(['like', 'road_en', $this->road_en])
            ->andFilterWhere(['like', 'districts', $this->districts])
            ->andFilterWhere(['like', 'districts_en', $this->districts_en])
            ->andFilterWhere(['like', 'sub_ districts', $this->sub_ districts])
            ->andFilterWhere(['like', 'sub_ districts_en', $this->sub_ districts_en])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'province_en', $this->province_en])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'last_name_en', $this->last_name_en])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'position_en', $this->position_en]);

        return $dataProvider;
    }
}
