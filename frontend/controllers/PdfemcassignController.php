<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\Employee;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class PdfemcassignController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
        $value = Employee::find()->where(['id'=>$_GET['em_id']])->one();
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";



        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/BT_EM_C_new44.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);



            if($pageNo ==  10){ // 7
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

                $dis_name =" อ.";
                $sub_disname = " ต.";
                if($mou->company->province == "กรุงเทพมหานคร"){
                    $dis_name =" เขต";
                    $sub_disname = " แขวง";
                }

                $y1 = 70;
                $start = 115;
                $end =179;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->company->address_no.' ม.'.$mou->company->moo.' '.$mou->company->soi.$sub_disname.$mou->company->subdistricts.$dis_name.$mou->company->districts.' จ.'.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 78;
                $start = 104;
                $end =113;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($mou->create_at,'day').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 78;
                $start = 123;
                $end =156;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'. $this->DateThaiType($mou->create_at,'month').'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 91;
                $start = 77;
                $end = 177;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 98;
                $start = 38;
                $end = 67;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->nationality->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $vaNum = $value->emp_number;
                $y1 = 97;
                $start = 110;
                $end = 114;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $vaNum[0] . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 97;
                $start = 117;
                $end = 121;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $vaNum[1] .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 121;
                $end = 125;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[2]. '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 126;
                $end = 129;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'  .$vaNum[3]. '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 130;
                $end = 133;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[4].'</div>
', $start, $y1, $end - $start, 90, 'auto');




                $y1 = 97;
                $start = 138;
                $end = 141;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[5].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 97;
                $start = 142;
                $end = 146;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[6].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 97;
                $start = 147;
                $end = 151;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[7].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 97;
                $start = 152;
                $end = 155;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[8].'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 97;
                $start = 156;
                $end = 161;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[9].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 163;
                $end = 167;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[10].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 167;
                $end = 172;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[11].'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 97;
                $start = 174;
                $end = 177;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' .$vaNum[12].'</div>
', $start, $y1, $end - $start, 90, 'auto');



                $y1 = 212;
                $start = 86;
                $end =135;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 118;
                $start = 40;
                $end =178;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">ต้องทำงาน</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 151;
                $start = 79;
                $end =178;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

                $y1 = 157;
                $start = 41;
                $end = 58;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->address_no .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 157;
                $start = 66;
                $end = 82;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->moo .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 157;
                $start = 90;
                $end = 133;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->soi .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 157;
                $start = 141;
                $end = 178;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->road .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 164;
                $start = 45;
                $end = 66;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->subdistricts .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 164;
                $start = 84;
                $end = 132;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->districts .'</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 164;
                $start = 144;
                $end = 178;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->user->province .'</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 184;
                $start = 68;
                $end =137;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 232;
                $start = 86;
                $end =135;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->user->prefix->title.$mou->user->first_name.' '.$mou->user->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');



                $y1 = 252;
                $start = 86;
                $end =135;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


                $y1 = 271;
                $start = 86;
                $end =135;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');




            } // 9

        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
