<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Out */

$this->title = 'Update สร้างแจ้งออก: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'รายการแจ้งออก', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="out-update">
    <?php if(isset($_GET['type'])) { ?>

        <?php if($_GET['type'] == 1) { ?>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        <?php } else { ?>
            <?= $this->render('_formmou', [
                'model' => $model,
            ]) ?>
        <?php }  ?>

    <?php } else {  ?>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php }  ?>
</div>
