<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="employee-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12" >
            <?= $form->field($model, 'photo')->fileInput() ?>
            <?php if(!$model->isNewRecord) {

                echo '<embed src="'.Url::base().'/'.$model->photo.'" width="500" height="500" />';


            } ?>
        <?= $form->field($model, 'passport_file')->fileInput() ?>
         <?php if(!$model->isNewRecord) { ?>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Passport</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        $doc_passport = \app\models\Directory::find()->where(['key'=>'e-passport'])->one();
                        $doc_passportData = \app\models\Documents::find()->where(['directory_id'=>$doc_passport->id,'ref_id'=>$model->id])->all();
                        if(count($doc_passportData) > 0){
                            foreach ($doc_passportData as $key=>$value){
                                echo '<embed src="'.Url::base().'/'.$value->file.'" width="500" height="500" />';
                             }
                        }
                     ?>
                </div>
                <!-- /.box-body -->
            </div>
        <?php  } ?>
        <?php if(!$model->isNewRecord) { ?>


        <?= $form->field($model, 'code')->textInput(['maxlength' => true,'readonly'=>true,'value'=>'emp-'.$model->id]); ?>
        <?php } ?>

        <?php if(isset($_GET['out'])) { ?>
            <div style="display: none;">
                <?= $form->field($model, 'out_id')->textInput(['value'=>$_GET['out']]) ?>
            </div>
        <?php } else { ?>

        <?php }  ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4>1. ข้อมูลนายจ้าง</h4>
        </div>
        <div class="col-md-12">
            <?php
            if($user->type == 0) {
                $natiC = ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name');
            } else {
                $natiC = ArrayHelper::map(\app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all(), 'id', 'name');
            }


            ?>
            <div class="row" style="display: none;">

                <?php
                $out = \app\models\Out::find()->where(['id'=>$_GET['out']])->one();
                echo  $form->field($model, 'company_id')->textInput(['value' => $out->company_id]);
                ?>
            </div>
            <?php
            echo  $out->company->name;
            ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <h4> 2. ข้อมูลประวัติ</h4>
        </div>
        <div class="col-md-6" style="display: none;">
            <?php
            $mou = \app\models\Out::find()->where(['id'=>$_GET['out']])->one();
            echo $form->field($model, 'nationality_id')->textInput(['value' => $mou->nationality_id]) ;
            ?>

        </div>




        <div class="col-md-6">
            <?php
            $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
            echo $form->field($model, 'prefix_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกคำนำหน้าชื่อ']

            ); ?>
        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name_en')->textInput(['maxlength' => true]) ?>

        </div>


        <div class="col-md-6">


            <?php
            echo $form->field($model, 'sex')->dropDownList(
                array(1=>"ชาย",2=>"หญิง"),
                ['prompt'=>'เลือกเพศ']

            ); ?>

        </div>



    </div>






    <div class="row">
        <div class="col-md-12">
            <h4> 3 ข้อมูลเอกสารต้นทาง
            </h4>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>


        </div>


    </div>


    <div class="row">
        <div class="col-md-12">
            <h4> 4. ข้อมูลใบอนุญาตทำงาน
            </h4>
        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'work_permit_number')->textInput(['maxlength' => true]) ?>

        </div>
    </div>


        <?php
        $natiC = ArrayHelper::map(\app\models\CauseOut::find()->all(), 'id', 'title');
        echo $form->field($model, 'cause_out_id')->dropDownList(
            $natiC,
            ['prompt'=>'เลือกเหตุผลที่ออก']

        ); ?>





    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
