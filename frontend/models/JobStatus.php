<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "job_status".
 *
 * @property int $id
 * @property string $title
 * @property string $status
 * @property int $alert
 * @property int $photo
 * @property int $doc
 * @property int $date
 * @property MouStatus[] $mouStatuses
 */
class JobStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'job_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'string', 'max' => 255],
            [['alert','photo','doc','date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMouStatuses()
    {
        return $this->hasMany(MouStatus::className(), ['job_status_id' => 'id']);
    }
}
