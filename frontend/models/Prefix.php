<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prefix".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 *
 * @property Company[] $companies
 * @property Grantee[] $grantees
 * @property Payee[] $payees
 * @property Witness[] $witnesses
 */
class Prefix extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prefix';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'title_en'], 'required'],
            [['title', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_en' => 'Title En',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['prefix_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrantees()
    {
        return $this->hasMany(Grantee::className(), ['prefix_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayees()
    {
        return $this->hasMany(Payee::className(), ['prefix_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitnesses()
    {
        return $this->hasMany(Witness::className(), ['prefix_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PrefixQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrefixQuery(get_called_class());
    }
}
