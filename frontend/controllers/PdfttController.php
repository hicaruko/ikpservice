<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\Mou;
use app\models\Nationality;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Yii;

class PdfttController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function Convert($amount_number)
        {
            $amount_number = number_format($amount_number, 2, ".","");
            $pt = strpos($amount_number , ".");
            $number = $fraction = "";
            if ($pt === false)
                $number = $amount_number;
            else
            {
                $number = substr($amount_number, 0, $pt);
                $fraction = substr($amount_number, $pt + 1);
            }

            $ret = "";
            $baht = $this->ReadNumber ($number);
            if ($baht != "")
                $ret .= $baht . "บาท";

            $satang = $this->ReadNumber($fraction);
            if ($satang != "")
                $ret .=  $satang . "สตางค์";
            else
                $ret .= "ถ้วน";
            return $ret;
        }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }


    public function actionIndex()
    {
        $mou = Mou::find()->where(['id'=>$_GET['id']])->one();
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);
        $pdfs = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";

        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/tt2.pdf');



        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Work</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table name='footer' width=\"100%\">
           <tr>
             <td style='font-size: 16px; padding-bottom: -30px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            if ($pageNo == 1) {  // 1

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


            }

            $pdf->AddPage();
            $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

        }


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";

        $pdf->AddPage();
        $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

        $y1 = 79;
        $start = 46;
        $end =183;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->name.'</div>
', $start, $y1, $end-$start, 90, 'auto');

        $y1 = 86;
        $start = 104;
        $end =183;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->license.'</div>
', $start, $y1, $end-$start, 90, 'auto');

        $y1 = 94;
        $start = 29;
        $end =183;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->address_no.' '.$mou->company->moo.' '.$mou->company->soi.' '.$mou->company->subdistricts.' '.$mou->company->districts.' '.$mou->company->province.' '.$mou->company->zipcode.'</div>
', $start, $y1, $end-$start, 90, 'auto');

        $y1 = 101;
        $start = 122;
        $end =183;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->telephone.'</div>
', $start, $y1, $end-$start, 90, 'auto');

        $y1 = 133;
        $start = 97;
        $end =122;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->nationality->title.'</div>
', $start, $y1, $end-$start, 90, 'auto');

        $y1 = 133;
        $start = 133;
        $end =152;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.($mou->male+$mou->female).'</div>
', $start, $y1, $end-$start, 90, 'auto');



        $y1 = 256;
        $start = 41;
        $end =81;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->company->prefix->title.$mou->company->first_name.' '.$mou->company->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


        $y1 = 279;
        $start = 41;
        $end =81;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witness->prefix->title.$mou->witness->first_name.' '.$mou->witness->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');


        $y1 = 256;
        $start = 117;
        $end =160;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">นางสาววิไลพร งามเจริญ</div>
', $start, $y1, $end-$start, 90, 'auto');


        $y1 = 279;
        $start = 117;
        $end =160;
        $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$mou->witnessIdTwo->prefix->title.$mou->witnessIdTwo->first_name.' '.$mou->witnessIdTwo->last_name.'</div>
', $start, $y1, $end-$start, 90, 'auto');





        $pdf -> Output('myOwn.pdf', 'i');

    }

}
