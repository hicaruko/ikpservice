<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\Employee;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Pdfbt46inController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = MouNew::find()->where(['id'=>$_GET['id']])->one();
        $value = EmployeeInform::find()->where(['id'=>$_GET['em_id']])->one();
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";



        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/BT_EM_M_new44.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);


            if ($pageNo == 4) { // 6


                $dis_name = " อ.";
                $sub_disname = " ต.";
                if ($mou->company->province == "กรุงเทพมหานคร") {
                    $dis_name = " เขต";
                    $sub_disname = " แขวง";
                }

                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 225, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 184, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  120, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//                }


                     $y1 = 184;
                    $start = 44;
                    $end = 61;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $value->renew_year . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 184;
                    $start = 144;
                    $end = 194;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $this->DateThai($value->renew_end) . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                if($mou->company->type == 2){
                    $y1 = 48;
                    $start = 92;
                    $end = 135;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->license . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 48;
                    $start = 29;
                    $end = 30;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">/</div>
', $start, $y1, $end - $start, 90, 'auto');
                } else {
                    $y1 = 29;
                    $start = 112;
                    $end = 130;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->license . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 29;
                    $start = 165;
                    $end = 186;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->license_price . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                    $y1 = 29;
                    $start = 71;
                    $end = 103;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $this->DateThai($mou->company->license_date) . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                    $y1 = 28;
                    $start = 29;
                    $end = 30;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">/</div>
', $start, $y1, $end - $start, 90, 'auto');
                }




                $y1 = 59;
                $start = 86;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->name . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 64;
                $start = 62;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->address_no . ' ม.' . $mou->company->moo . ' ' . $mou->company->soi . $sub_disname . $mou->company->subdistricts . $dis_name . $mou->company->districts . ' จ.' . $mou->company->province . ' ' . $mou->company->zipcode . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 74;
                $start = 64;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->businessType->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 147;
                $start = 117;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">'.$value->prefix->title_en.' ' . $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 153;
                $start = 48;
                $end = 110;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $value->nationality->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 159;
                $start = 58;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->address_no . ' ม.' . $mou->company->moo . ' ' . $mou->company->soi . $sub_disname . $mou->company->subdistricts . $dis_name . $mou->company->districts . ' จ.' . $mou->company->province . ' ' . $mou->company->zipcode . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 180;
                $start = 99;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->address_no . ' ม.' . $mou->company->moo . ' ' . $mou->company->soi . $sub_disname . $mou->company->subdistricts . $dis_name . $mou->company->districts . ' จ.' . $mou->company->province . ' ' . $mou->company->zipcode . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 164;
                $start = 59;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $value->job_type . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 169;
                $start = 59;
                $end = 194;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $value->job_des . '</div>
', $start, $y1, $end - $start, 90, 'auto');


//                $y1 = 195;
//                $start = 44;
//                $end = 57;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyleSmall . '">' . $mou->preriod_year . '</div>
//', $start, $y1, $end - $start, 90, 'auto');
//
//                $y1 = 195;
//                $start = 60;
//                $end = 71;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyleSmall . '">' . $mou->preriod_month . '</div>
//', $start, $y1, $end - $start, 90, 'auto');

//                $y1 = 195;
//                $start = 120;
//                $end = 190;
//                $pdf->WriteFixedPosHTML('
//<div style="' . $blockStyleSmall . '">31 มีนาคม 2565</div>
//', $start, $y1, $end - $start, 90, 'auto');
//
                $y1 = 194;
                $start = 66;
                $end = 96;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->wage_per_day . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 258;
                $start = 103;
                $end = 149;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->prefix->title . $mou->company->first_name . ' ' . $mou->company->last_name . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 224;
                $start = 20;
                $end = 191;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">ไม่มีคนไทยมาสมัครงาน</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 263;
                $start = 114;
                $end = 149;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->position . '</div>
', $start, $y1, $end - $start, 90, 'auto');

            }  // 12


        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
