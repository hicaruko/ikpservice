<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'Update Employee: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="employee-update">


    <?php if($_GET['type'] == 1) { ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    <?php } else {?>
        <?= $this->render('_formmou', [
            'model' => $model,
        ]) ?>
    <?php } ?>

</div>
