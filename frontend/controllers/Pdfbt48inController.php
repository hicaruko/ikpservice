<?php

namespace frontend\controllers;

use app\models\Company;
use app\models\EmployeeInform;
use app\models\Mou;
use app\models\MouNew;
use app\models\Nationality;
use FontLib\Table\Type\name;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Utils\UtfString;
use Mpdf\Mpdf;
use Yii;


class Pdfbt48inController extends \yii\web\Controller
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false)
            $number = $amount_number;
        else
        {
            $number = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }

        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";

        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }

        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
                ((($divider == 10) && ($d == 1)) ? "" :
                    ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }
    public function DateThaiType($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        if($type == 'full'){
            return "$strDay $strMonthThai $strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonthThai";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }

    public function getAge($birthday) {
        $then = strtotime($birthday);
        return(floor((time()-$then)/31556926));
    }

    public function DateThaiTypeNumber($strDate,$type = 'full')
    {
        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("m",strtotime($strDate));
        $strDay= date("d",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        if($type == 'full'){
            return "$strDay"."/"."$strMonth"."/"."$strYear";

        } else  if($type == 'day'){
            return "$strDay";
        } else  if($type == 'month'){
            return "$strMonth";
        } else  if($type == 'year'){
            return "$strYear";
        }
    }
    public function actionIndex()
    {
        $mou = MouNew::find()->where(['id'=>$_GET['id']])->one();
        $value = EmployeeInform::find()->where(['id'=>$_GET['em_id']])->one();;
//        $Company = Company::find()->where(['id'=>$mou->company_id])->one();
//        $pdf = Yii::$app->pdf; // or new Pdf();
//        $mpdf = $pdf->api; // fetches mpdf api
//        $mpdf->SetHeader('Kartik Header'); // call methods or set any properties
//        $file = Yii::getAlias('@web/doc/MOULaos.pdf');
//        $mpdf->WriteHtml($file); // call mpdf write html
//        echo $mpdf->Output('filename', 'I'); // call the mpdf api output as needed

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::$app->basePath.'/web/doc/th-sarabun-psk',
            ]),
            'fontdata' => $fontData + [
                    'sarabun' => [
                        'R' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Bold.ttf',
                    ]
                ],
            'default_font' => 'sarabun'
        ]);

//        $pdf = new Fpdi();
//        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/testre3.pdf');
//        $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
//
//        $pdf->addPage();
//        $pdf->useImportedPage($pageId, 10, 10, 90);
//        $pdf->Output();
//        background-color: rgba(10,86,140,0.34);


        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyle = "text-align: center;font-family: sarabun;font-size: 16px";
        $blockStyleSmall = "text-align: center;font-family: sarabun;font-size: 13px";
        $blockStyles = "background-color: #0d6aad;text-align: center;font-family: sarabun;font-size: 2px;border:0.1";
        $blockStyless = "background-color: rgba(10,86,140,0.34);text-align: center;font-family: sarabun;font-size: 16px;border:0.1";



        $pageCount = $pdf->setSourceFile(Yii::$app->basePath.'/web/doc/bt48.pdf');


//        $pdf->OverWrite($pageCount, $search, $replacement, 'I', $pageCount ) ;

        $footer = "<div style='margin-bottom: -30px;bottom:-30px;'>$mou->code Demand</div>";
        $pdf->defaultfooterline = 0;
        $pdf->defaultfooterfontstyle='B';
        $pdf->defaultfooterline=0;
        $footer = "<table style='z-index: 99;' name='footer' width=\"100%\" >
           <tr>
             <td style='font-size: 16px; padding-bottom: -40px;' align=\"left\">".$mou->code."</td>
           </tr>
         </table>";
        $pdf->SetFooter($footer);

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {

            $tplIdx = $pdf->importPage($pageNo);

            $size = $pdf->getTemplateSize($pageNo);

            if ($pageNo == 1) { // 5

                $blockStyle = "text-align: center;font-family: sarabun;font-size: 18px";
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);


//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 225, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 97, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  120, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//                }


                $y1 = 105;
                $start = 41;
                $end = 196;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyleSmall . '">' . $mou->company->prefix->title . $mou->company->first_name . ' ' . $mou->company->last_name . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 113;
                $start = 40;
                $end = 64;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->address_no . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 113;
                $start = 81;
                $end = 127;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->moo . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 113;
                $start = 134;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->soi . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 124;
                $start = 32;
                $end = 66;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->road . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 124;
                $start = 83;
                $end = 131;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->subdistricts . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 124;
                $start = 148;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->districts . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 135;
                $start = 34;
                $end = 68;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->province . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 135;
                $start = 88;
                $end = 102;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->zipcode . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 135;
                $start = 115;
                $end = 150;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->telephone . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 164;
                $start = 78;
                $end = 196;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">'.$value->prefix->title_en.' '. $value->first_name_en . " " . $value->last_name_en . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 175;
                $start = 32;
                $end = 98;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->nationality->title . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 175;
                $start = 110;
                $end = 162;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $this->DateThai($value->date_of_birth, 'full') . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 175;
                $start = 173;
                $end = 184;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $this->getAge($value->date_of_birth) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                if($value->other_document == 0){
                    $y1 = 185;
                    $start = 26;
                    $end =28;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">/</div>
', $start, $y1, $end-$start, 90, 'auto');

                } else {


                    $y1 = 195;
                    $start = 26;
                    $end =28;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">/</div>
', $start, $y1, $end-$start, 90, 'auto');

                    $y1 = 194;
                    $start = 66;
                    $end = 200;
                    $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->other_type . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                }



                $y1 = 206;
                $start = 32;
                $end = 79;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->other_number . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 206;
                $start = 94;
                $end = 131;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->other_from . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 206;
                $start = 143;
                $end = 196;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->other_location . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 216;
                $start = 40;
                $end = 107;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $this->DateThai($value->other_start) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 216;
                $start = 123;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $this->DateThai($value->other_end) . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 248;
                $start = 56;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->job_type . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 259;
                $start = 40;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->job_des . '</div>
', $start, $y1, $end - $start, 90, 'auto');






            }   // 5
            if($pageNo ==  2){ // 6
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);

//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 225, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i, 97, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<210;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $cdd = $i;
//                    if($i > 100 && $i<200){
//                        $cdd = $i -100;
//                    }  else if($i > 200 ){
//                        $cdd = $i -200;
//                    }
//
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$cdd.'</div>
//', $i,  43, 1, 90, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 80, $i, 1, 1, 'auto');
//
//                }
//
//                for ($i=0;$i<295;$i++){
//                    $blockStyles = "background-color: #".$this->random_color()."70;text-align: center;font-family: Arial;font-size: 2px;border:0.1;opacity: 0.5;";
//                    $pdf->WriteFixedPosHTML('
//<div style="'.$blockStyles.'">'.$i.'</div>
//', 150, $i, 1, 1, 'auto');
//
//                }


                $y1 = 24;
                $start = 72;
                $end = 89;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->address_no . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 24;
                $start = 102;
                $end = 138;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->moo . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 24;
                $start = 145;
                $end = 196;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->soi . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 34;
                $start = 33;
                $end = 71;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->road . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 34;
                $start = 89;
                $end = 132;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->subdistricts . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 34;
                $start = 149;
                $end = 200;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->districts . '</div>
', $start, $y1, $end - $start, 90, 'auto');



                $y1 = 44;
                $start = 36;
                $end = 71;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->province . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 44;
                $start = 90;
                $end = 103;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->zipcode . '</div>
', $start, $y1, $end - $start, 90, 'auto');


                $y1 = 44;
                $start = 116;
                $end = 157;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $mou->company->telephone . '</div>
', $start, $y1, $end - $start, 90, 'auto');

                $y1 = 55;
                $start = 67;
                $end = 95;
                $pdf->WriteFixedPosHTML('
<div style="' . $blockStyle . '">' . $value->renew_year . '</div>
', $start, $y1, $end - $start, 90, 'auto');



            }  // 7
            if($pageNo ==  3){ // 6
                $pdf->AddPage();
                $pdf->useTemplate($tplIdx, null, null, $size['w'], $size['h'], FALSE);




            }  // 7


        }

        $pdf -> Output('myOwn.pdf', 'i');

    }

}
