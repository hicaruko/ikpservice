<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessType */

$this->title = 'Update Business Type: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Business Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="business-type-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
