<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'แจ้งออก';
$this->params['breadcrumbs'][] = $this->title;

$datass = app\models\User::find()->all();
$datalists = [];
foreach ($datass as $values) {
    $datalists[$values->id] = $values->first_name . " " . $values->last_name;
}
?>
<div class="out-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('สร้างแจ้งออก', ['create','type'=>1], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'code',
            'out_date',
            [
                'attribute'=>'company_id',
                'filter'=>ArrayHelper::map(\app\models\Company::find()->asArray()->all(), 'id', 'name'),
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    return $model->company->name;
                }

            ],
            'nationality.title',
            'note:ntext',
//
//            'id',
//            'company_id',
//            'out_date',
//            'out_total',
//            'witness_id',
            //'witness_id2',
            //'user_id',
            //'unit_price',
            //'note:ntext',
            [
                'label' => 'Create By',
                'attribute'=>'user_id',
                'headerOptions' => ['width' => '80'],
                'filter'=>$datalists,
                'value' => function($model, $key, $index)
                {
                    $datas = app\models\User::find()->where(['id'=>$model->user_id])->one();
                    if($datas){
                        return $datas->first_name." ".$datas->last_name;
                    }


                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete}</div>',
                'urlCreator' => function( $action, $model, $key, $index ){
                    if ($action == "view") {
                        return Url::to(['out/view', 'id' => $model->id, 'type' => $_GET['type']]);
                    }
                    if ($action == "update") {
                        if(isset($_GET['type'])) {
                            if ($_GET['type'] == 2) {
                                return Url::to(['out/update', 'id' => $model->id, 'type' => $_GET['type']]);

                            } else {
                                return Url::to(['out/update', 'id' => $model->id, 'type' => $_GET['type']]);

                            }
                        } else {
                            return Url::to(['out/update', 'id' => $model->id, 'type' => $_GET['type']]);

                        }

                    }
                    if ($action == "delete") {
                        return Url::to(['out/delete', 'id' => $model->id, 'type' => $_GET['type']]);
                    }
                }

            ],
        ],
    ]); ?>
</div>
