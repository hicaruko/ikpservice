<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MyCompany */

$this->title = 'Create My Company';
$this->params['breadcrumbs'][] = ['label' => 'My Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-company-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
