<?php

namespace frontend\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Witness;

/**
 * WitnessSearch represents the model behind the search form of `app\models\Witness`.
 */
class WitnessSearch extends Witness
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'prefix_id'], 'integer'],
            [['first_name', 'first_name_en', 'last_name', 'last_name_en','user_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Witness::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        $user_login = User::findOne(Yii::$app->user->id);

        if($user_login){
            if($user_login->type != 0){
                $query->andFilterWhere([
                    'id' => $this->id,
                    'prefix_id' => $this->prefix_id,
                    'user_id' => Yii::$app->user->id,
                ]);
            } else {
                $query->andFilterWhere([
                    'id' => $this->id,
                    'prefix_id' => $this->prefix_id,
                    'user_id' => $this->user_id,

                ]);
            }
        } else {
            $query->andFilterWhere([
                'id' => $this->id,
                'prefix_id' => $this->prefix_id,
                'user_id' => $this->user_id,

            ]);
        }

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'first_name_en', $this->first_name_en])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'last_name_en', $this->last_name_en]);

        return $dataProvider;
    }
}
