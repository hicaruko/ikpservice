<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Subdistricts]].
 *
 * @see Subdistricts
 */
class SubdistrictsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Subdistricts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Subdistricts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
