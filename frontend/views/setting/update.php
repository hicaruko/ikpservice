<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->title = 'แก้ไขการตั้งค่า ';
$this->params['breadcrumbs'][] = 'แก้ไขการตั้งค่า';
?>
<div class="setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
