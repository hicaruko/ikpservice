<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agent".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $license
 * @property string $license_file
 * @property int $nationality_id
 *
 * @property Nationality $nationality
 * @property Mou[] $mous
 */
class Agent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'license', 'license_file'], 'string'],
            [['nationality_id'], 'required'],
            [['nationality_id'], 'integer'],
            [['nationality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nationality::className(), 'targetAttribute' => ['nationality_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'license' => 'License',
            'license_file' => 'License File',
            'nationality_id' => 'Nationality ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(Nationality::className(), ['id' => 'nationality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMous()
    {
        return $this->hasMany(Mou::className(), ['agent_id' => 'id']);
    }
}
