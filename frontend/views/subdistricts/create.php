<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subdistricts */

$this->title = 'Create Subdistricts';
$this->params['breadcrumbs'][] = ['label' => 'Subdistricts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subdistricts-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
