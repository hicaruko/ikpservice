<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "out".
 *
 * @property int $id
 * @property string $code
 * @property int $company_id
 * @property string $out_date
 * @property int $out_total
 * @property int $witness_id
 * @property int $witness_id2
 * @property int $user_id
 * @property int $unit_price
 * @property int $nationality_id
 * @property int $mou_id
 * @property int $type
 * @property string $create_at

 * @property string $note
 *
 *
 * @property User $user
 * @property Mou $mou
 * @property Company $company
 * @property Witness $witness
 * @property Witness $witnessIdTwo
 * @property Nationality $nationality

 */

class Out extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'out_total', 'witness_id', 'witness_id2', 'user_id', 'unit_price','nationality_id'], 'integer'],
            [['out_date','type','mou_id','create_at'], 'safe'],
            [['note','code'], 'string'],
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->create_at = date('Y-m-d H:i:s');
                $mou = Out::find()->select(['id'=>'MAX(`id`)'])->one();
                if($mou){
                    $this->code = 'Out-'.date('Ymd').'-'.($mou->id + 1);
                } else {
                    $this->code = 'Out-'.date('Ymd').'-1';

                }
            } else {

            }
            return true;
        }
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'company_id' => 'บริษัทนายจ้าง',
            'out_date' => 'วันที่ลาออก',
            'out_total' => 'จำนวนคนแจ้งออก',
            'witness_id' => 'พยานคนที่ 1',
            'witness_id2' => 'พยานคนที่ 2',
            'user_id' => 'ผู้รับมอบอำนาจ',
            'unit_price' => 'Unit Price',
            'nationality_id' => 'สัญชาติ',
            'note' => 'Note',
            'type' => 'type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMou()
    {
        return $this->hasOne(Mou::className(), ['id' => 'mou_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitness()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(Nationality::className(), ['id' => 'nationality_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitnessIdTwo()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id2']);
    }


}
