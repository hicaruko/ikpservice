<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 * @property string $first_name
 * @property string $first_name_en
 * @property string $last_name
 * @property string $last_name_en
 * @property string $license
 * @property string $phone
 * @property int $prefix_id
 * @property int $is_grantee
 * @property string $license_start
 * @property string $license_end
 * @property string $license_file

 * @property string $address_no
 * @property string $moo
 * @property string $soi
 * @property string $road
 * @property string $soi_n
 * @property string $road_en
 * @property string $province
 * @property string $province_en
 * @property string $districts
 * @property string $districts_en
 * @property string $subdistricts
 * @property string $subdistricts_en
 * @property string $zipcode
 *
 *
 * @property HistoryUpdate[] $historyUpdates
 * @property Mou[] $mous
 * @property Prefix $prefix
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public $password;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'first_name', 'first_name_en', 'last_name', 'last_name_en', 'prefix_id','phone','license_start','license_end'], 'required'],
            [['password'], 'required', 'on' => 'create'],
            [['password'], 'safe', 'on' => 'update'],
            [['status', 'created_at', 'updated_at', 'type', 'prefix_id'], 'integer'],
            [['email','is_grantee', 'address_no' , 'moo', 'soi', 'road' , 'soi_n', 'road_en', 'province', 'province_en', 'districts', 'districts_en', 'subdistricts', 'subdistricts_en', 'zipcode','license_file','password'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'first_name', 'first_name_en', 'last_name', 'last_name_en', 'license','phone'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['prefix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prefix::className(), 'targetAttribute' => ['prefix_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'first_name' => 'First Name',
            'first_name_en' => 'First Name En',
            'last_name' => 'Last Name',
            'last_name_en' => 'Last Name En',
            'license' => 'License',
            'license_start' => 'วันที่ออกบัตร',
            'license_end' => 'บัตรใช้ได้ถึง',
            'prefix_id' => 'Prefix ID',
            'phone' => 'Phone',
            'is_grantee' => 'is_grantee',
            'address_no' => 'ที่อยู่เลขที่',
            'moo' => 'หมู่',
            'soi' => 'ซอยภาษาไทย',
            'road' => 'ถนนภาษาไทย',
            'soi_n' => 'ซอยภาษาอังกฤษ',
            'road_en' => 'ถนนภาษาอังกฤษ',
            'province' => 'จังหวัดภาษาไทย',
            'province_en' => 'จังหวัดภาษาอังกฤษ',
            'districts' => 'อำเภอภาษาไทย',
            'districts_en' => 'อำเภอภาษาอังกฤษ',
            'subdistricts' => 'ตำบลภาษาไทย',
            'subdistricts_en' => 'ตำบลภาษาอังกฤษ',
            'zipcode' => 'รหัสไปรษณีย์',
            'license_file' => 'ไฟล์ 3 คน',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->created_at = date('Y-m-d H:i:s');
                $this->updated_at = date('Y-m-d H:i:s');
            } else {
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistoryUpdates()
    {
        return $this->hasMany(HistoryUpdate::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMous()
    {
        return $this->hasMany(Mou::className(), ['user_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefix()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return \common\models\User|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
