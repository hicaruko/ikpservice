<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OtherDocumentType */

$this->title = 'Update Other Document Type: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Other Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="other-document-type-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
