<?php

namespace frontend\controllers;

use app\models\Directory;
use app\models\Documents;
use app\models\Employee;
use app\models\EmployeeInform;
use Yii;
use app\models\EmployeeOut;
use frontend\models\EmployeeOutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmployeeOutController implements the CRUD actions for EmployeeOut model.
 */
class EmployeeOutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmployeeOut models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeOutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmployeeOut model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function getUploadPath(){
        return Yii::getAlias('@webroot').'/employee/';
    }
    public function getUploadPathDoc(){
        return Yii::getAlias('@webroot').'/documents/';
    }

    public function getUploadUrl(){
        return Yii::getAlias('@web').'/employee/';
    }

    /**
     * Creates a new EmployeeOut model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmployeeOut();
        if($_GET['type'] == 1){
            $model->scenario = 'create';
            $directory = Directory::find()->where(['key'=>"e-passport"])->one();
            if ($model->load(Yii::$app->request->post())) {
                if(isset($_GET['mou'])){
                    $model->out_id =  $_GET['mou'];
                }
                $model->code="out-".$model->id;
                $photo  = UploadedFile::getInstance($model, 'photo');
                $passport_image  = UploadedFile::getInstance($model, 'passport_file');
                $path = $this->getUploadPath();
                $pathDoc = $this->getUploadPathDoc();
                $pathUrl = $this->getUploadUrl();
                if ($photo !== null) {
                    $fileName = $model->first_name_en."-".md5($photo->baseName.time()) . '.' . $photo->extension;
                    if($photo->saveAs( $path.$fileName)){
                        $model->photo = 'employee/'.$fileName;
                        if($passport_image !== null) {
                            $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName.time()) . '.' . $passport_image->extension;
                            if($passport_image->saveAs( $pathDoc.$fileName_passport)) {
                                if( $model->save()){
                                    $newDoc = new Documents();
                                    $newDoc->out_id = $model->out_id;
                                    $newDoc->file = 'documents/'.$fileName_passport;
                                    $newDoc->directory_id = $directory->id;
                                    $newDoc->doc_type = 2;
                                    $newDoc->ref_id = $model->id;
                                    $newDoc->save();
                                    if (isset($_GET['mou'])) {
                                        $Employee = Employee::findOne($model->employee_id);
                                        $Employee->status =2;
                                        $Employee->save();
                                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                                    } else {
                                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);

                                    }
                                } else {
                                    print_r($model->getErrors());
                                }
                            }
                        } else {
                            $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                        }

                    } else {
                        if ($passport_image !== null) {
                            $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName.time()) . '.' . $passport_image->extension;
                            if($passport_image->saveAs( $pathDoc.$fileName_passport)) {
                                if( $model->save()){
                                    $newDoc = new Documents();
                                    $newDoc->out_id = $model->out_id;
                                    $newDoc->file = 'documents/'.$fileName_passport;
                                    $newDoc->directory_id = $directory->id;
                                    $newDoc->doc_type = 2;
                                    $newDoc->ref_id = $model->id;
                                    $newDoc->save();
                                    if (isset($_GET['mou'])) {
                                        $Employee = Employee::findOne($model->employee_id);
                                        $Employee->status =2;
                                        $Employee->save();
                                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                                    } else {
                                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);

                                    }
                                } else {
                                    print_r($model->getErrors());
                                }
                            }
                        } else {
                            $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                        }

                    }
                } else {
                    if ($passport_image !== null) {
                        $fileName_passport = $model->first_name_en."-".md5($passport_image->baseName . time()) . '.' . $passport_image->extension;
                        if ($passport_image->saveAs($pathDoc . $fileName_passport)) {
                            if( $model->save()){
                                $newDoc = new Documents();
                                $newDoc->out_id = $model->out_id;
                                $newDoc->file = 'documents/'.$fileName_passport;
                                $newDoc->directory_id = $directory->id;
                                $newDoc->doc_type = 1;
                                $newDoc->ref_id = $model->id;
                                $newDoc->save();
                                if (isset($_GET['mou'])) {
                                    $Employee = Employee::findOne($model->employee_id);
                                    $Employee->status = 2;
                                    $Employee->save();
                                    return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                                } else {
                                    return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                                }
                            } else {
                                print_r($model->getErrors());
                            }

                        }
                    } else {
                        $model->addError('passport_file', 'กรุณาเลือกเอกสาร passport');
                    }
                }

            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if($model->save()){
                    if (isset($_GET['mou'])) {
                        $Employee = Employee::findOne($model->employee_id);
                        $Employee->status =2;
                        $Employee->save();
                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                    } else {
                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                    }
                } else {
                    print_r($model->getErrors());
                }

            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


        }

    }

    /**
     * Updates an existing EmployeeOut model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPath = $model->photo;
        if ($model->load(Yii::$app->request->post())) {
            $pathDoc = $this->getUploadPathDoc();
            $passport_image = UploadedFile::getInstance($model, 'passport_file');
            if ($passport_image !== null) {
                $directory = Directory::find()->where(['key' => "e-passport"])->one();
                $fileName_passport = $model->first_name_en . "-" . md5($passport_image->baseName . time()) . '.' . $passport_image->extension;
                $directoryCk = Documents::find()->where(['doc_type' => 2,'out_id'=>$model->out_id,'ref_id'=>$model->id])->one();
                if($directoryCk){
                    $DocumentsRm =  Documents::findOne($directoryCk->id);
                    $DocumentsRm->delete();
                }
                if ($passport_image->saveAs($pathDoc . $fileName_passport)) {
                    $newDoc = new Documents();
                    $newDoc->out_id = $model->out_id;
                    $newDoc->file = 'documents/' . $fileName_passport;
                    $newDoc->directory_id = $directory->id;
                    $newDoc->doc_type = 2;
                    $newDoc->ref_id = $model->id;
                    $newDoc->save();
                }
            }
            $photo  = UploadedFile::getInstance($model, 'photo');
            $path = $this->getUploadPath();
            $pathDoc = $this->getUploadPathDoc();
            $pathUrl = $this->getUploadUrl();
            $model->code="out-".$model->id;
            if ($photo !== null) {
                $fileName = md5($photo->baseName.time()) . '.' . $photo->extension;
                if($photo->saveAs( $path.$fileName)){
                    $model->photo = 'employee/'.$fileName;
                    if($model->save()){
                        if(isset($_GET['mou'])){
                            return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                        } else {
                            return $this->redirect(['view', 'id' => $model->id]);

                        }
                    }

                } else {
                    $model->photo = $oldPath;
                    if($model->save()) {
                        if (isset($_GET['mou'])) {
                            return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                        } else {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }
            } else {
                $model->photo = $oldPath;
                if($model->save()) {

                    if (isset($_GET['mou'])) {
                        return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id]);

                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmployeeOut model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model =  $this->findModel($id);
        $model->delete();
        if (isset($_GET['mou'])) {
            $Employee = Employee::findOne($model->employee_id);
            $Employee->status = 1;
            $Employee->save();
            return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);
        } else {
            return $this->redirect(['out/viewemp', 'id' => $model->out_id,'type'=>$_GET['type']]);

        }

     }

    /**
     * Finds the EmployeeOut model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeOut the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployeeOut::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
