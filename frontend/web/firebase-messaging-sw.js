 // * Here is is the code snippet to initialize Firebase Messaging in the Service
 // * Worker when your app is not hosted on Firebase Hosting.
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here. Other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/8.4.3/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/8.4.3/firebase-messaging.js');
 // Initialize the Firebase app in the service worker by passing in
 // your app's Firebase config object.
 // https://firebase.google.com/docs/web/setup#config-object
 firebase.initializeApp({
     apiKey: "AIzaSyAzKKifW-uYj3_P37sX5LHWTnkeEd7KBJw",
     authDomain: "ikpservice-62544.firebaseapp.com",
     databaseURL: "https://ikpservice-62544-default-rtdb.firebaseio.com",
     projectId: "ikpservice-62544",
     storageBucket: "ikpservice-62544.appspot.com",
     messagingSenderId: "1004205395694",
     appId: "1:1004205395694:web:d3814c8366c79523eaedff",
     measurementId: "G-2BQZLHMFC6"
 });
 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// Keep in mind that FCM will still show notification messages automatically
// and you should use data messages for custom notifications.
// For more info see:
// https://firebase.google.com/docs/cloud-messaging/concept-options
 messaging.onBackgroundMessage((payload) => {
     console.log('[firebase-messaging-sw.js] Received background message ', payload);
     // Customize notification here
     const notificationTitle = 'Background Message Title';
     const notificationOptions = {
         body: 'Background Message body.',
         icon: '/apple-icon.png'
     };

     self.registration.showNotification(notificationTitle,
         notificationOptions);
 });
