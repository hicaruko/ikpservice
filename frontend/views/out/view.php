<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Out */

$this->title = $model->company->name." / แจ้งออก ";
$this->params['breadcrumbs'][] = ['label' => 'แจ้งออก', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="out-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="row">
        <div class="col-md-9">

            <div class="row">
                <div class="col-md-6">
                    <h4>ข้อมูล แจ้งออก</h4>
                    <?php
                    if($model->mou_id != NULL){
                        echo "<b>Mou Code</b>: ".$model->code;
                    }
                    ?>
                    <br>

                    <?php echo "<b>".$model->getAttributeLabel('code').'</b>: '.$model->code ?>
                    <br>
                    <?php echo "<b>".$model->getAttributeLabel('out_date').'</b>: '.$model->out_date ?>
                </div>
                <div class="col-md-6">
                    <h4>ข้อมูล นายจ้าง</h4>

                    <?php $Company =  \app\models\Company::find()->where(['id'=>$model->company_id])->one() ?>
                    <?php if($Company) { ?>
                        <?php echo "<b>".$model->getAttributeLabel('company_id').'</b>: '.$Company->name ?>
                    <?php } ?>

                </div>
            </div>

            <h4>ข้อมูลลายเซนเอกสาร</h4>

            <div class="row">
                <div class="col-md-4">

                    <?php $Witness = app\models\Witness::find()->where(['id'=>$model->witness_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id').'</b>: '.$Witness->first_name." ".$Witness->last_name ?> <br>

                    <?php $witness_id_two = app\models\Witness::find()->where(['id'=>$model->witness_id2])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('witness_id2').'</b>: '.$witness_id_two->first_name." ".$witness_id_two->last_name ?> <br>

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <h4>ผู้รับผิดชอบงาน</h4>

                    <?php $User = app\models\User::find()->where(['id'=>$model->user_id])->one() ?>
                    <?php echo "<b>".$model->getAttributeLabel('user_id').'</b>: '.$User->first_name.' '.$User->last_name ?> <br>


                </div>
            </div>

            <br>
            <br>

        </div>
        <div class="col-md-3">
            <?= Html::a('รายละเอียดแจ้งออก', ['view', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-default btn-block']) ?>
            <?= Html::a('ข้อมูลลูกจ้าง', ['viewemp', 'id' => $model->id,'type'=>$model->type], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('เอกสารประกอบการ', ['viewdocument', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
            <?= Html::a('ประวัติสถานะ', ['viewstatus', 'id' => $model->id ,'type' => 3], ['class' => 'btn btn-primary btn-block']) ?>



            <br>
            <br>
            <?= Html::a('พิมพ์เอกสารแจ้งออก', ['pdfbt12out/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <?= Html::a('พิมพ์เอกสารมอบอำนาจ', ['companydoc3/index', 'id' => $model->id], ['class' => 'btn btn-info btn-block','target'=>"_blank"]) ?>
            <br>
            <?= Html::a('แก้ไขแจ้งออก', ['update', 'id' => $model->id], ['class' => 'btn btn-warning btn-block']) ?>

        </div>

    </div>


</div>
