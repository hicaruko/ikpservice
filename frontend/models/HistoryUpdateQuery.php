<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[HistoryUpdate]].
 *
 * @see HistoryUpdate
 */
class HistoryUpdateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return HistoryUpdate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return HistoryUpdate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
