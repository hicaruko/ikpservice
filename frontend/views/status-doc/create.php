<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StatusDoc */

$this->title = 'Create Status Doc';
$this->params['breadcrumbs'][] = ['label' => 'Status Docs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-doc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
