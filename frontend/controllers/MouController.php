<?php

namespace frontend\controllers;

use app\models\Authorize;
use app\models\Directory;
use app\models\DirectorySearch;
use app\models\DirectoryTemp;
use app\models\Document;
use app\models\DocumentsSearch;
use app\models\Employee;
use app\models\EmployeeSearch;
use app\models\MouStatusSearch;
use app\models\Out;
use frontend\models\AuthorizeSearch;
use frontend\models\OutSearch;
use Yii;
use app\models\Mou;
use frontend\models\MouSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MouController implements the CRUD actions for Mou model.
 */
class MouController extends Controller
{
    /**
     * {@inheritdoc}
     */



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all Mou models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MouSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mou model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new AuthorizeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id,1);

        $searchModelO = new OutSearch();
        $dataProviderO = $searchModelO->searchMou(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelP' => $searchModel,
            'dataProviderP' => $dataProvider,
            'searchModelO' => $searchModelO,
            'dataProviderO' => $dataProviderO,
        ]);
    }
    public function actionViewemp($id)
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id);

        return $this->render('viewemp', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewstatus($id)
    {
        $searchModel = new MouStatusSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id,$_GET['type']);
        return $this->render('viewstatus', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewreport($id)
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id);
        return $this->render('viewreport', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    } public function actionViewsubdocument($id)
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$_GET['dir']);
        return $this->render('viewsubdocument', [
            'model' => $this->findModel($id),
            'dir' => Directory::find()->where(['id'=>$_GET['dir']])->one(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewdocument($id)
    {



        $DirectoryTemp = DirectoryTemp::find()->where(['status'=>1])->all();

        $mouData = Mou::find()->where(['id'=>$id])->one();

        foreach ($DirectoryTemp as $key=>$value){
            $DirectorysCheck = Directory::find()->where(['mou_id'=>$id,'bt23'=>0,'directory_temp_id'=>$value->id])->one();
            if(!$DirectorysCheck){
                $Directory = new Directory();
                $Directory->mou_id = $id;
                $Directory->bt23 = 0;
                $Directory->create_at = $mouData->create_at;
                $Directory->update_at = $mouData->create_at;
                $Directory->name = $value->title;
                $Directory->directory_temp_id = $value->id;
                $Directory->order = $value->order;
                $Directory->status = 1;
                $Directory->save();
            } else {
                $Directory = Directory::findOne($DirectorysCheck->id);
                $Directory->name = $value->title;
                $Directory->order = $value->order;
                $Directory->save();
            }

        }


        $searchModel = new DirectorySearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id);
        return $this->render('viewdocument', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Creates a new Mou model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mou();

        if ($model->load(Yii::$app->request->post())) {


            if($model->save()){
                return $this->redirect(['index']);;
            } else {

//                print_r($model->getErrors());
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Mou model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);;
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Mou model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model  =  $this->findModel($id);
        $Authorize = Authorize::find()->where(['mou_id'=>$model->id,'type'=>1])->all();
        foreach ($Authorize as $key=>$value){
            Authorize::findOne($value->id)->delete();
        }

        $Out = Out::find()->where(['mou_id'=>$model->id,'type'=>2])->all();
        foreach ($Out as $key=>$value){
            Out::findOne($value->id)->delete();
        }

        $Directory = Directory::find()->where(['mou_id'=>$model->id,'bt23'=>0])->all();
        foreach ($Directory as $key=>$value){
            $Document = Document::find()->where(['directory_id'=>$value->id])->all();
            foreach ($Document as $keys=>$values){
                Document::findOne($values->id)->delete();
            }
            Directory::findOne($value->id)->delete();
        }
        $Employee = Employee::find()->where(['mou_id'=>$model->id])->all();
        foreach ($Employee as $key=>$value){
            Employee::findOne($value->id)->delete();
        }
        $model->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Mou model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mou the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mou::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
