<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();
$out = \app\models\Out::find()->where(['id'=>$_GET['mou']])->one();

?>

<div class="employee-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <h4>1. ข้อมูลนายจ้าง</h4>
        </div>
        <div class="col-md-12">

            <?php

            echo  $out->company->name;
            ?>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12">
            <h4>2. ข้อมูลลูกจ้าง</h4>
        </div>
        <div class="col-md-12">
            <div style="display: none;">
                <?= $form->field($model, 'out_id')->textInput(['value'=>$_GET['mou']]) ?>
            </div>
            <?php
            echo $out->mou_id;
            $datas = \app\models\Employee::find()->where(['mou_id'=>$out->mou_id])->all();
            $datalists = [];
            foreach ($datas as $values){

                   $datalists[$values->id] = $values->first_name_en." ".$values->last_name_en;

            }



            echo $form->field($model, 'employee_id')->widget(Select2::classname(), [
                'data' => $datalists,
                'options' => ['placeholder' => 'เลือกลูกจ้าง ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            ?>

            <?php
            $natiC = ArrayHelper::map(\app\models\CauseOut::find()->all(), 'id', 'title');
            echo $form->field($model, 'cause_out_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกเหตุผลที่ออก']

            ); ?>
        </div>

    </div>







    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
