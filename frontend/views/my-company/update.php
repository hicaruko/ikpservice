<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MyCompany */

$this->title = 'Update My Company: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'My Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="my-company-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
