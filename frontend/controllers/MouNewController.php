<?php

namespace frontend\controllers;

use app\models\Directory;
use app\models\DirectorySearch;
use app\models\DirectoryTemp;
use app\models\DocumentsSearch;
use app\models\EmployeeInformSearch;
use app\models\EmployeeSearch;
use app\models\MouNew;
use app\models\MouStatusSearch;
use frontend\models\AuthorizeSearch;
use frontend\models\MouNewTwoSearch;
use Yii;
use app\models\Mou;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MouController implements the CRUD actions for Mou model.
 */
class MouNewController extends Controller
{
    /**
     * {@inheritdoc}
     */



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all Mou models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MouNewTwoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mou model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new AuthorizeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id,2);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelP' => $searchModel,
            'dataProviderP' => $dataProvider,
        ]);
    }
    public function actionViewemp($id)
    {
        $searchModel = new EmployeeInformSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id);

        return $this->render('viewemp', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewstatus($id)
    {
        $searchModel = new MouStatusSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id,$_GET['type']);
        return $this->render('viewstatus', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewreport($id)
    {
        $searchModel = new EmployeeInformSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$id);
        return $this->render('viewreport', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionViewsubdocument($id)
    {
        $searchModel = new DocumentsSearch();
        $dataProvider = $searchModel->searchBymou(Yii::$app->request->queryParams,$_GET['dir']);
        return $this->render('viewsubdocument', [
            'model' => $this->findModel($id),
            'dir' => Directory::find()->where(['id'=>$_GET['dir']])->one(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionViewdocument($id)
    {

        $DirectoryTemp = DirectoryTemp::find()->where(['bt23'=>1])->all();
        $mouData = MouNew::find()->where(['id'=>$id])->one();
        foreach ($DirectoryTemp as $key=>$value){
            $DirectorysCheck = Directory::find()->where(['mou_id'=>$id,'bt23'=>1,'directory_temp_id'=>$value->id])->one();
            if(!$DirectorysCheck){
                $Directory = new Directory();
                $Directory->mou_id = $id;
                $Directory->bt23 = 1;
                $Directory->key = $value->key;
                $Directory->create_at = $mouData->create_at;
                $Directory->update_at = $mouData->create_at;
                $Directory->name = $value->title;
                $Directory->directory_temp_id = $value->id;
                $Directory->order = $value->order;
                $Directory->status = 1;
                $Directory->save();
            } else {
                $Directory = Directory::findOne($DirectorysCheck->id);
                $Directory->name = $value->title;
                $Directory->order = $value->order;
                $Directory->key = $value->key;
                $Directory->doc_type = $value->doc_type;
                $Directory->save();
            }

        }


        $searchModel = new DirectorySearch();
        $dataProvider = $searchModel->searchBymouBT(Yii::$app->request->queryParams,$id);
        print_r($dataProvider);
        return $this->render('viewdocument', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Creates a new Mou model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MouNew();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                return $this->redirect(['index']);;
            } else {
//                print_r($model->getErrors());
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Mou model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Mou model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Mou model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mou the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MouNew::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
