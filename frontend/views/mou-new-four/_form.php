<?php

use app\models\Grantee;
use app\models\User;
use app\models\Witness;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJs(' 
   $(document).ready(function(){
        $(".mounew-address").change(function(){
         if($(this).is(":checked")){
            
         $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#mounew-company_id").val(),
            type: "get",
            dataType: "json",
        })
        .done(function(response) {
                console.log(response)
                $("#mounew-telephone").val(response.telephone);
                $("#mounew-fax").val(response.fax);
                $("#mounew-address_no").val(response.address_no);
                $("#mounew-moo").val(response.moo);
                $("#mounew-road").val(response.road);
                $("#mounew-province").val(response.province);
                $("#mounew-districts").val(response.districts);
                $("#mounew-subdistricts").val(response.subdistricts);
                $("#mounew-zipcode").val(response.zipcode);
                
                $("#mounew-soi").val(response.soi);
                $("#mounew-soi_n").val(response.soi_n);
                $("#mounew-road_en").val(response.road_en);
                $("#mounew-province_en").val(response.province_en);
                $("#mounew-districts_en").val(response.districts_en);
                $("#mounew-subdistricts_en").val(response.subdistricts_en);
                $("#mounew-zipcode").val(response.zipcode);
        })
        .fail(function() {
            console.log("error");
            
        });
      
            }else{
                $("#mounew-telephone").val("");
                $("#mounew-fax").val("");
                $("#mounew-address_no").val("");
                $("#mounew-moo").val("");
                $("#mounew-road").val("");
                $("#mounew-province").val("");
                $("#mounew-districts").val("");
                $("#mounew-subdistricts").val("");
                $("#mounew-zipcode").val("");
                
                $("#mounew-soi_n").val("");
                $("#mounew-soi").val("");
                $("#mounew-road_en").val("");
                $("#mounew-province_en").val("");
                $("#mounew-districts_en").val("");
                $("#mounew-subdistricts_en").val("");
                $("#mounew-zipcode").val("");

            }
        });

    });
   
   ', \yii\web\View::POS_READY);
$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="mounew-form">

    <?php $form = ActiveForm::begin(); ?>


<!--    --><?php
//    $nati = ArrayHelper::map(\app\models\Nationality::find()->all(), 'id', 'title');
//    echo $form->field($model, 'nationality_id')->dropDownList(
//        $nati,
//        ['prompt'=>'เลือกสัญชาติ']
//    ); ?>

    <?= $form->field($model, 'number_price')->textInput() ?>

    <div class="row">

        <div class="col-md-12">
            <h4>ข้อมูล นายจ้าง</h4>
            <?php
            $natiCData = array();
             if($user->type == 0) {
                $natiC = ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name');
            } else {
                $natiC = ArrayHelper::map(\app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all(), 'id', 'name');
            }
            foreach ($natiC as $key=>$value){
                if(!$model->isNewRecord){
                    $checkIs = \app\models\Company::find()->where(['id'=>$model->company_id])->one();

                    $check = \app\models\MouNew::find()->where(['company_id'=>$key])->one();
                    if(!$check){
                        array_push($natiCData,array(
                            'id'=>$key,
                            'name'=>$value
                        ));
                    }
                    array_push($natiCData,array(
                        'id'=>$checkIs->id,
                        'name'=>$checkIs->name
                    ));
                } else {
                    $check = \app\models\MouNew::find()->where(['company_id'=>$key])->one();
                    if(!$check){
                        array_push($natiCData,array(
                            'id'=>$key,
                            'name'=>$value
                        ));
                    }
                }


            }

            $natiCData = ArrayHelper::map($natiCData, 'id', 'name');


            echo $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data' => $natiCData,
                'options' => ['placeholder' => 'เลือกนายจ้าง ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "clear" => '
                    function(e) {
                

}
                    ',

                    "change" => '
                     function(e) { 
                    if($("#mounew-company_id").val()){ 
                     $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#mounew-company_id").val(),
            type: "get",
            dataType: "json",
        }).done(function(response) {
                console.log(response)
                $("#mounew-telephone").val(response.telephone);
                $("#mounew-fax").val(response.fax);
                $("#mounew-address_no").val(response.address_no);
                $("#mounew-moo").val(response.moo);
                $("#mounew-road").val(response.road);
                $("#mounew-province").val(response.province);
                $("#mounew-districts").val(response.districts);
                $("#mounew-subdistricts").val(response.subdistricts);
                $("#mounew-zipcode").val(response.zipcode);
                
                $("#mounew-soi").val(response.soi);
                $("#mounew-soi_n").val(response.soi_n);
                $("#mounew-road_en").val(response.road_en);
                $("#mounew-province_en").val(response.province_en);
                $("#mounew-districts_en").val(response.districts_en);
                $("#mounew-subdistricts_en").val(response.subdistricts_en);
                $("#mounew-zipcode").val(response.zipcode);
        }).fail(function() {
            console.log("error");
            
        });
        
        } else {
             $("#mounew-telephone").val("");
                $("#mounew-fax").val("");
                $("#mounew-address_no").val("");
                $("#mounew-moo").val("");
                $("#mounew-road").val("");
                $("#mounew-province").val("");
                $("#mounew-districts").val("");
                $("#mounew-subdistricts").val("");
                $("#mounew-zipcode").val("");
                
                $("#mounew-soi").val("");
                $("#mounew-soi_n").val("");
                $("#mounew-road_en").val("");
                $("#mounew-province_en").val("");
                $("#mounew-districts_en").val("");
                $("#mounew-subdistricts_en").val("");
                $("#mounew-zipcode").val("");
        }
      
             }
                    ',
                ],
            ]);
             ?>
        </div>
    </div>

    <h4>ข้อมูลสถานที่ทำงาน     <label><input type="checkbox" class="mounew-address" name="" value="1" aria-invalid="false"> ที่อยู่เดียวกับนายจ้าง</label>
    </h4>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'address_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'moo')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'soi')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'road')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'districts')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'subdistricts')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'soi_n')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'road_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'province_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'districts_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'subdistricts_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'preriod_year')->textInput() ?>
            <?= $form->field($model, 'preriod_month')->textInput() ?>
            <?= $form->field($model, 'wage_per_day')->textInput() ?>
            <?= $form->field($model, 'week_holiday')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'annual')->textInput() ?>

            <?php
            $natiC = ArrayHelper::map(\app\models\Holiday::find()->all(), 'id', 'title');
            echo $form->field($model, 'holiday_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกวันหยุด']

            ); ?>

            <?= $form->field($model, 'work_time')->textInput() ?>
            <?= $form->field($model, 'paydate')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput() ?>

        </div>
    </div>


    <h4>ข้อมูลลายเซ็นเอกสาร</h4>

    <div class="row">
        <div class="col-md-4">


                <?php $UserFixed = User::find()->where(['type'=>1])->one(); ?>
                 <?php if(!$model->isNewRecord)  { ?>

                    <?php if($user->type == 0) { ?>

                        <?php

                        $datas = User::find()->where(['is_grantee'=>1])->all();

                        $datalists = [];
                        foreach ($datas as $values){
                            $datalists[$values->id] = $values->first_name." ".$values->last_name;
                        }

                         echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                             'data' => $datalists,
                             'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
                             'pluginOptions' => [
                                 'allowClear' => true
                             ],
                         ]);

                        ?>


                        <div style="display: none;">

                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                        </div>
                    <?php } else { ?>
                         <div style="display: none;">
                             <?php $form->field($model, 'user_id')->textInput(['value'=>Yii::$app->user->id]) ?>
                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                    <?php }?>

                <?php } else { ?>
                <?php if($user->type == 0) { ?>

                        <?php
                        $datas = User::find()->where(['is_grantee'=>1])->all();
                        $datalists = [];
                        foreach ($datas as $values){
                            $datalists[$values->id] = $values->first_name." ".$values->last_name;
                        }

                         echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                             'data' => $datalists,
                             'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
                             'pluginOptions' => [
                                 'allowClear' => true
                             ],
                         ]);

                      ?>

                         <div style="display: none">

                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                     <?php } else { ?>
                         <div style="display: none">

                             <?php echo $form->field($model, 'user_id')->textInput(['value'=>Yii::$app->user->id]) ?>
                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                     <?php }?>


                 <?php }?>

        </div>
        <div class="col-md-4">
            <?php
            if($user->type == 0) {
                $datas = Witness::find()->all();
            } else {
                $datas = Witness::find()->where(['user_id'=>Yii::$app->user->getId()])->all();

            }
            $datalists = [];
            foreach ($datas as $values){
                $datalists[$values->id] = $values->first_name." ".$values->last_name;
            }

            echo $form->field($model, 'witness_id')->widget(Select2::classname(), [
                'data' => $datalists,
                'options' => ['placeholder' => 'เลือกพยานคนที่ 1 ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);


            ?>


            <?php

            echo $form->field($model, 'witness_id_two')->widget(Select2::classname(), [
                'data' => $datalists,
                'options' => ['placeholder' => 'เลือกพยานคนที่ 2 ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

              ?>

        </div>

    </div>

    <?php
    if($user->type == 0) {
        echo $form->field($model, 'cancle')->dropDownList(
            array(0 => "ไม่ใช่", 1 => "ใช่"),
            ['prompt' => '===== ยกเลิก =====']
        );
    }
    ?>
    <br>
    <br>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>
