<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Mou]].
 *
 * @see Mou
 */
class MouQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Mou[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Mou|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
