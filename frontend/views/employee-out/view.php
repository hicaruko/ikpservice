<?php

use app\models\Directory;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employee-view">


    <p>

        <?php
        if ($_GET['type'] == 2) {

            echo Html::a('Update', ['update', 'id' => $model->id, 'mou' => $_GET['mou'], 'type' => $_GET['type']], ['class' => 'btn btn-primary']);

        } else {
            echo Html::a('Update', ['update', 'id' => $model->id, 'out' => $_GET['out'], 'type' => $_GET['type']], ['class' => 'btn btn-primary']);

        }
        ?>

       <?php
        if ($_GET['type'] == 2) {
            echo Html::a('Delete', ['delete', 'id' => $model->id,'mou' => $_GET['mou'], 'type' => $_GET['type']], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        } else {
            echo Html::a('Delete', ['delete', 'id' => $model->id, 'type' => $_GET['type']], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        }

        ?>

    </p>
    <div class="box box-default">

        <div class="box-header with-border">
            <h3 class="box-title">ใบอนุญาตทำงาน</h3>
        </div>
        <div class="box-body">

            <?php
            echo '<embed src="'.Url::base().'/'.$model->photo.'" width="500" height="500" />';
            ?>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title">Passport</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php
            $doc_passport = \app\models\Directory::find()->where(['key'=>'e-passport'])->one();
            $doc_passportData = \app\models\Documents::find()->where(['directory_id'=>$doc_passport->id,'ref_id'=>$model->id])->all();
            if(count($doc_passportData) > 0){
                foreach ($doc_passportData as $key=>$value){
                    echo '<embed src="'.Url::base().'/'.$value->file.'" width="500" height="500" />';
                }
            }
            ?>
        </div>
        <!-- /.box-body -->
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'first_name_en',
            'last_name_en',
            'nickname',
            'blood',
            'date_of_birth',
            'sex',
            'contact_number',
            'age',
            'email:email',
            'foreign_address:ntext',
            'photo:ntext',
            'passport',
            'passport_ext',
            'passport_is',
            'passport_ext_alert:ntext',
            'issue_location',
            'visa_is',
            'visa_ext',
            'visa_number',
            'visa_type',
            'stamped',
            'entrance_card',
            'workpermit_is',
            'workpermit_ext',
            'tt_number',
            'work_permit_number',
            'job_description',
            'position',
            'report_tm_date',
            'report_status',
            'id',
            'type',
            'address:ntext',
            'company_id',
            'nationality_id',
            'prefix_id',
        ],
    ]);

    ?>

    <?php
    $Directory = Directory::find()->where(['key'=>'e-invoice','mou_id'=>$model->id])->one();
    $doc_list = array();
    if($Directory){
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }

    } else {
        $DirectoryTemp = \app\models\DirectoryTemp::find()->where(['key'=>'e-invoice'])->one();
        $Directory = new Directory();
        $Directory->out_id = $model->id;
        $Directory->status = 1;
        $Directory->order = $DirectoryTemp->order;
        $Directory->directory_temp_id = $DirectoryTemp->id;
        $Directory->bt23 = $DirectoryTemp->bt23;
        $Directory->doc_type = $DirectoryTemp->doc_type;
        $Directory->key = $DirectoryTemp->key;
        $Directory->save();
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }
    }



    ?>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ใบเสร็จ</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?php
            ActiveForm::begin([
                'options'=>['enctype'=>'multipart/form-data'] // important
            ]);
            echo FileInput::widget([
                'name'=>'input_file',
                'options' => ['multiple' => true],

                'pluginOptions' => [
                    'previewFileType' => 'any',
                    'initialPreviewFileType' => 'image',
                    'uploadUrl' => Url::toRoute(["documents/upload-file-em",'id'=>$Directory->id,'mou'=>$model->id,'em'=>$model->id]),
                    'initialPreview'=>$doc_list,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>false,
                    'initialPreviewConfig'=>$doc_config,
                    'showCaption' => true,
                    'showPreview' => true,

//                         'purifyHtml'=> true,

                ],
                'pluginEvents'=>[
                    "filebatchuploadcomplete" => "function() { window.location.reload(true);
                            }",
                ]
            ]);
            ActiveForm::end();

            ?>
        </div>
    </div>


    <?php
    $Directory = Directory::find()->where(['key'=>'e-doctor','mou_id'=>$model->id])->one();
    $doc_list = array();
    if($Directory){
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }

    } else {
        $DirectoryTemp = \app\models\DirectoryTemp::find()->where(['key'=>'e-doctor'])->one();
        $Directory = new Directory();
        $Directory->out_id = $model->id;
        $Directory->status = 1;
        $Directory->order = $DirectoryTemp->order;
        $Directory->directory_temp_id = $DirectoryTemp->id;
        $Directory->bt23 = $DirectoryTemp->bt23;
        $Directory->doc_type = $DirectoryTemp->doc_type;
        $Directory->key = $DirectoryTemp->key;
        $Directory->save();
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }
    }



    ?>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ใบแพทย์</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?php
            ActiveForm::begin([
                'options'=>['enctype'=>'multipart/form-data'] // important
            ]);
            echo FileInput::widget([
                'name'=>'input_file',
                'options' => ['multiple' => true],

                'pluginOptions' => [
                    'previewFileType' => 'any',
                    'initialPreviewFileType' => 'image',
                    'uploadUrl' => Url::toRoute(["documents/upload-file-em",'id'=>$Directory->id,'mou'=>$model->id,'em'=>$model->id]),
                    'initialPreview'=>$doc_list,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>false,
                    'initialPreviewConfig'=>$doc_config,
                    'showCaption' => true,
                    'showPreview' => true,

//                         'purifyHtml'=> true,

                ],
                'pluginEvents'=>[
                    "filebatchuploadcomplete" => "function() { window.location.reload(true);
                            }",
                ]
            ]);
            ActiveForm::end();

            ?>
        </div>
    </div>



    <?php
    $Directory = Directory::find()->where(['key'=>'e-pink','mou_id'=>$model->id])->one();
    $doc_list = array();
    if($Directory){
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }

    } else {
        $DirectoryTemp = \app\models\DirectoryTemp::find()->where(['key'=>'e-pink'])->one();
        $Directory = new Directory();
        $Directory->out_id = $model->id;
        $Directory->status = 1;
        $Directory->order = $DirectoryTemp->order;
        $Directory->directory_temp_id = $DirectoryTemp->id;
        $Directory->bt23 = $DirectoryTemp->bt23;
        $Directory->doc_type = $DirectoryTemp->doc_type;
        $Directory->key = $DirectoryTemp->key;
        $Directory->save();
        $doc = \app\models\Documents::find()->where(['directory_id'=>$Directory->id])->all();
        $doc_config = array();
        foreach ($doc as $value){

            $ext = pathinfo(Url::to('@web/'.$value->file, true), PATHINFO_EXTENSION);
            array_push($doc_list,Url::to('@web/'.$value->file, true));
            if($ext != 'pdf' && $ext != 'doc' && $ext != 'docx'){
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>'image',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,
                ));
            } else {
                array_push($doc_config,array(
                    'caption'=>$value->file,
                    'type'=>$ext.'',
                    'url' => Url::to(['documents/delete-em','id'=>$value->id,'em_id'=>$model->id,'mou'=>$model->id,'dir'=>$Directory->id]),
                    'key'=>$value->id,

                ));
            }

        }
    }



    ?>

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">ใบชมพู</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <?php
            ActiveForm::begin([
                'options'=>['enctype'=>'multipart/form-data'] // important
            ]);
            echo FileInput::widget([
                'name'=>'input_file',
                'options' => ['multiple' => true],

                'pluginOptions' => [
                    'previewFileType' => 'any',
                    'initialPreviewFileType' => 'image',
                    'uploadUrl' => Url::toRoute(["documents/upload-file-em",'id'=>$Directory->id,'mou'=>$model->id,'em'=>$model->id]),
                    'initialPreview'=>$doc_list,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>false,
                    'initialPreviewConfig'=>$doc_config,
                    'showCaption' => true,
                    'showPreview' => true,

//                         'purifyHtml'=> true,

                ],
                'pluginEvents'=>[
                    "filebatchuploadcomplete" => "function() { window.location.reload(true);
                            }",
                ]
            ]);
            ActiveForm::end();

            ?>
        </div>
    </div>


</div>
