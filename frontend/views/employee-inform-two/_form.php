<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */

$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="employee-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12" >

            <?php if(!$model->isNewRecord) {
             echo '<img src="'.Url::base().'/'.$model->photo.'" class="" alt="User Image" width="100">';

            } ?>
        <?= $form->field($model, 'photo')->fileInput() ?>
        <?= $form->field($model, 'passport_file')->fileInput() ?>
         <?php if(!$model->isNewRecord) { ?>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Passport</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        $doc_passport = \app\models\Directory::find()->where(['key'=>'e-passport'])->one();
                        $doc_passportData = \app\models\Documents::find()->where(['directory_id'=>$doc_passport->id,'ref_id'=>$model->id])->all();
                        if(count($doc_passportData) > 0){
                            foreach ($doc_passportData as $key=>$value){
                                echo '<embed src="'.Url::base().'/'.$value->file.'" width="auto" height="auto" />';
                            }
                        }
                     ?>
                </div>
                <!-- /.box-body -->
            </div>
        <?php  } ?>
        <?php if(!$model->isNewRecord) { ?>


        <?= $form->field($model, 'code')->textInput(['maxlength' => true,'readonly'=>true,'value'=>'emp-'.$model->id]); ?>
        <?php } ?>

        <?php if(isset($_GET['mou'])) { ?>
            <div style="display: none;">
                <?= $form->field($model, 'mou_new_id')->textInput(['value'=>$_GET['mou']]) ?>
            </div>
        <?php } else { ?>
            <?php
            $natiC = ArrayHelper::map(\app\models\MouNew::find()->all(), 'id', 'code');

            echo $form->field($model, 'mou_new_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกเข้า Mou'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

        <?php }  ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4>1. ข้อมูลนายจ้าง</h4>
        </div>
        <div class="col-md-12">
            <?php
            if($user->type == 0) {
                $natiC = ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name');
            } else {
                $natiC = ArrayHelper::map(\app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all(), 'id', 'name');
            }

            echo $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกนายจ้าง ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => '
                     function(e) { 
                    if($("#employeeinform-company_id").val()){ 
                     $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#employeeinform-company_id").val(),
            type: "get",
            dataType: "json",
        }).done(function(response) {
                console.log(response)
                var add = "เลขที่ "+response.address_no+" หมู่ "+response.moo+" ตำบล"+response.subdistricts+" อำเภอ"+response.districts+" จังหวัด"+response.province+" "+response.zipcode ;
                $("#employeeinform-address").val(add)
                
        }).fail(function() {
            console.log("error");
            
        });
        
        } else {
              
        }
      
             }
                    ',
                ],
            ]);
            ?>

            <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>


        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <h4> 2. ข้อมูลประวัติ</h4>
        </div>
        <div class="col-md-6">

            <?php
            $nati = ArrayHelper::map(\app\models\Nationality::find()->all(), 'id', 'title');
            echo $form->field($model, 'nationality_id')->dropDownList(
                $nati,
                ['prompt'=>'เลือกสัญชาติ']
            ); ?>

        </div>

        <div class="col-md-6">
            <?php
            echo $form->field($model, 'job_type')->dropDownList(
                array(
                        "กรรมกรก่อสร้าง"=>"กรรมกรก่อสร้าง",
                        "กรรมกร"=>"กรรมกร",
                        "ผู้รับใช้ในบ้าน"=>"ผู้รับใช้ในบ้าน",
                        "ประมง"=>"ประมง",
                        "ขายของหน้าร้าน"=>"ขายของหน้าร้าน",
                        "ช่างฝีมือ"=>"ช่างฝีมือ",
                        "ลูกจ้างในครัวเรือนส่วนบุคคล"=>"ลูกจ้างในครัวเรือนส่วนบุคคล",
                ),
                ['prompt'=>'เลือกตำแหน่งหน้าที่']





            ); ?>
        </div>
        <div class="col-md-6">
            <?php
            echo $form->field($model, 'job_des')->dropDownList(
                array(
                    "ยกของแบกของ และทำงานทั่วไปในโรงงาน"=>"ยกของแบกของ และทำงานทั่วไปในโรงงาน",
                    "ยกของแบกของ และทำงานทั่วไปในไซด์งานก่อสร้าง"=>"ยกของแบกของ และทำงานทั่วไปในไซด์งานก่อสร้าง",
                    "งานบ้านทั่วไป"=>"ลูกจ้างในครัวเรือนส่วนบุคคล",
                    "งานบ้าน ปัด กวาด เช็ด ถู"=>"งานบ้าน ปัด กวาด เช็ด ถู",
                    "งานประมง"=>"งานประมง"
                ),
                ['prompt'=>'เลือกลักษณะงาน']

            ); ?>
        </div>


        <div class="col-md-6">
            <?php
            $natiC = ArrayHelper::map(\app\models\Prefix::find()->all(), 'id', 'title');
            echo $form->field($model, 'prefix_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกคำนำหน้าชื่อ']

            ); ?>
        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'first_name_en')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name_en')->textInput(['maxlength' => true]) ?>

        </div>




        <div class="col-md-6">

            <?= $form->field($model,'date_of_birth')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>

        </div>
        <div class="col-md-6">


            <?php
            echo $form->field($model, 'sex')->dropDownList(
                array(1=>"ชาย",2=>"หญิง"),
                ['prompt'=>'เลือกเพศ']

            ); ?>

        </div>


        <div class="col-md-6">
            <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>


        </div>


    </div>






    <div class="row">
        <div class="col-md-12">
            <h4> 3 ข้อมูลเอกสารต้นทาง
            </h4>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'emp_number')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model,'passport_is')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>
            <?= $form->field($model,'passport_ext')->widget(DatePicker::className(),['pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'issue_location')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'co_location')->textInput(['maxlength' => true]) ?>

        </div>
    </div>






    <?php
    if($user->type == 0) {
        echo $form->field($model, 'status')->dropDownList(
            array(0 => "ไม่ใช่", 1 => "ใช่"),
            ['prompt' => '===== ยกเลิก =====']
        );
    }
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
