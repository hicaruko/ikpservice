<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mou_new".
 *
 * @property int $id
 * @property string $code
 * @property int $company_id
 * @property int $male
 * @property int $female

 * @property string $qualification_age
 * @property string $qualification_height
 * @property string $qualification_weight
 * @property int $preriod_year
 * @property int $preriod_month
 * @property int $wage_per_day
 * @property int $week_holiday
 * @property int $annual
 * @property int $holiday_id
 * @property int $nationality_id
 * @property int $witness_id
 * @property int $witness_id_two
 * @property string $paydate
 * @property string $create_at
 * @property string $update_date
 * @property int $print
 * @property int $version
 * @property string $document_date
 * @property int $user_id
 * @property int $work_time
 * @property int $price
 * @property int $type
 * @property int $status
 * @property int $number_price
 * @property int $cancle
 * @property int $running
 *

 * @property string $telephone
 * @property string $fax
 * @property string $license_en
 * @property string $address_no
 * @property string $moo
 * @property string $soi
 * @property string $road
 * @property string $soi_n
 * @property string $road_en
 * @property string $province
 * @property string $province_en
 * @property string $districts
 * @property string $districts_en
 * @property string $subdistricts
 * @property string $subdistricts_en
 * @property string $zipcode
 * @property string $importing_detail
 * @property string $payment_detail

 *
 *
 * @property HistoryUpdate[] $historyUpdates
 * @property Company $company
 * @property Holiday $holiday
 * @property Nationality $nationality
 * @property User $user
 * @property User $userId1
 * @property Witness $witness
 * @property Witness $witnessIdTwo
 */
class MouNewTwo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mou_new_two';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'holiday_id', 'witness_id', 'user_id','number_price'], 'required'],
            [['company_id', 'male', 'female', 'preriod_year', 'preriod_month', 'wage_per_day', 'week_holiday', 'annual', 'holiday_id' , 'nationality_id', 'witness_id', 'witness_id_two', 'print', 'version', 'user_id', 'work_time', 'price'], 'integer'],
            [['create_at', 'update_date', 'document_date','importing_detail','payment_detail','user_id1','type','status','cancle','running','type','cancle'], 'safe'],
            [['code'], 'string', 'max' => 45],
            [['qualification_age', 'qualification_height', 'qualification_weight', 'paydate', 'address_no', 'telephone', 'fax', 'license_en', 'address_no' , 'moo', 'soi', 'road' , 'soi_n', 'road_en', 'province', 'province_en', 'districts', 'districts_en', 'subdistricts', 'subdistricts_en', 'zipcode'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
//            [['dan_Immigration_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanImmigration::className(), 'targetAttribute' => ['dan_Immigration_id' => 'id']],
            [['user_id1'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id1' => 'id']],
            [['holiday_id'], 'exist', 'skipOnError' => true, 'targetClass' => Holiday::className(), 'targetAttribute' => ['holiday_id' => 'id']],
            [['nationality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nationality::className(), 'targetAttribute' => ['nationality_id' => 'id']],
//            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['witness_id'], 'exist', 'skipOnError' => true, 'targetClass' => Witness::className(), 'targetAttribute' => ['witness_id' => 'id']],
            [['witness_id_two'], 'exist', 'skipOnError' => true, 'targetClass' => Witness::className(), 'targetAttribute' => ['witness_id_two' => 'id']],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'รหัสคำร้อง',
            'company_id' => 'นายจ้าง/ผู้ประกอบการณ์',
            'male' => 'ชาย',
            'female' => 'หญิง',
            'qualification_age' => 'อายุ',
            'qualification_height' => 'ส่วนสูง',
            'qualification_weight' => 'น้ำหนัก',
            'preriod_year' => 'ระยะเวลาทำงาน (ปี)',
            'preriod_month' => 'ระยะเวลาทำงาน (เดือน)',
            'wage_per_day' => 'ค่าจ้างต่อวัน',
            'week_holiday' => 'วันหยุดประจำสัปดาห์',
            'annual' => 'วันหยุดพักผ่อนประจำปี',
            'holiday_id' => 'วันหยุดตามประเพณี',
            'nationality_id' => 'สัญชาติ',
            'witness_id' => 'พยานคนที่ 1',
            'witness_id_two' => 'พยานคนที่ 2',
            'paydate' => 'จ่ายเงินทุกวันที่',
            'create_at' => 'วันที่สร้าง',
            'update_date' => 'วันที่แก้ไข',
            'print' => 'จำนวนใจการพิม',
            'version' => 'เวอร์ชั่น',
            'document_date' => 'วันที่ออกเอกสาร',
            'user_id' => 'ผู้รับมอบอำนาจ',
            'work_time' => 'ชั่วโมงการทำงานปกติ',
            'price' => 'อัตราค่าบริการต่อคน',
            'user_id1' => 'ผู้รับมอบอำนาจ',
            'telephone' => 'เบอร์โทร',
            'fax' => 'แฟกซ์',
            'address_no' => 'ที่อยู่เลขที่',
            'moo' => 'หมู่',
            'soi' => 'ซอยภาษาไทย',
            'road' => 'ถนนภาษาไทย',
            'soi_n' => 'ซอยภาษาอังกฤษ',
            'road_en' => 'ถนนภาษาอังกฤษ',
            'province' => 'จังหวัดภาษาไทย',
            'province_en' => 'จังหวัดภาษาอังกฤษ',
            'districts' => 'อำเภอภาษาไทย',
            'districts_en' => 'อำเภอภาษาอังกฤษ',
            'subdistricts' => 'ตำบลภาษาไทย',
            'subdistricts_en' => 'ตำบลภาษาอังกฤษ',
            'zipcode' => 'รหัสไปรษณีย์',
            'importing_detail' => 'รหัสไปรษณีย์',
            'payment_detail' => 'รหัสไปรษณีย์',
            'type' => 'ประเภท',
            'number_price' => 'จำนวนลูกจ้างทั้งหมด',
            'cancle' => 'ยกเลิก',

        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
                $this->create_at = date('Y-m-d H:i:s');
                $this->update_date = date('Y-m-d H:i:s');
                $this->document_date = date('Y-m-d');
                $mou = MouNewTwo::find()->select(['id'=>'MAX(`id`)'])->one();
                if($mou){
                    $this->code = '4-08-'.date('Ymd').'-'.($mou->id + 1);
                } else {
                    $this->code = '4-08-'.date('Ymd').'-1';
                }
            } else {
                $this->update_date = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistoryUpdates()
    {
        return $this->hasMany(HistoryUpdate::className(), ['mou_id' => 'id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserId1()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHoliday()
    {
        return $this->hasOne(Holiday::className(), ['id' => 'holiday_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(Nationality::className(), ['id' => 'nationality_id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitness()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWitnessIdTwo()
    {
        return $this->hasOne(Witness::className(), ['id' => 'witness_id_two']);
    }

    /**
     * {@inheritdoc}
     * @return MouNewTwoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MouNewTwoQuery(get_called_class());
    }
}
