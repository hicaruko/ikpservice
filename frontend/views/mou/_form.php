<?php

use app\models\Grantee;
use app\models\User;
use app\models\Witness;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mou */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJs(' 
   $(document).ready(function(){
        $(".mou-address").change(function(){
         if($(this).is(":checked")){
            
         $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#mou-company_id").val(),
            type: "get",
            dataType: "json",
        })
        .done(function(response) {
                console.log(response)
                $("#mou-telephone").val(response.telephone);
                $("#mou-fax").val(response.fax);
                $("#mou-address_no").val(response.address_no);
                $("#mou-moo").val(response.moo);
                $("#mou-road").val(response.road);
                $("#mou-province").val(response.province);
                $("#mou-districts").val(response.districts);
                $("#mou-subdistricts").val(response.subdistricts);
                $("#mou-zipcode").val(response.zipcode);
                
                $("#mou-soi").val(response.soi);
                $("#mou-soi_n").val(response.soi_n);
                $("#mou-road_en").val(response.road_en);
                $("#mou-province_en").val(response.province_en);
                $("#mou-districts_en").val(response.districts_en);
                $("#mou-subdistricts_en").val(response.subdistricts_en);
                $("#mou-zipcode").val(response.zipcode);
        })
        .fail(function() {
            console.log("error");
            
        });
      
            }else{
                $("#mou-telephone").val("");
                $("#mou-fax").val("");
                $("#mou-address_no").val("");
                $("#mou-moo").val("");
                $("#mou-road").val("");
                $("#mou-province").val("");
                $("#mou-districts").val("");
                $("#mou-subdistricts").val("");
                $("#mou-zipcode").val("");
                
                $("#mou-soi_n").val("");
                $("#mou-road_en").val("");
                $("#mou-province_en").val("");
                $("#mou-districts_en").val("");
                $("#mou-subdistricts_en").val("");
                $("#mou-zipcode").val("");

            }
        });

    });
   
   ', \yii\web\View::POS_READY);
$user = \app\models\User::find()->where(['id'=>Yii::$app->user->getId()])->one();

?>

<div class="mou-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php
     echo $form->field($model, 'type')->dropDownList(
        array(1=>'New',2=>'Return')
    ); ?>
    <?php
    $nati = ArrayHelper::map(\app\models\Nationality::find()->all(), 'id', 'title');
    echo $form->field($model, 'nationality_id')->dropDownList(
        $nati,
        ['prompt'=>'เลือกสัญชาติ']
    ); ?>
    <div class="row">

        <div class="col-md-12">
            <h4>ข้อมูล นายจ้าง</h4>
            <?php
//             if($user->type == 0) {
//                $natiC = ArrayHelper::map(\app\models\Company::find()->all(), 'id', 'name');
//            } else {
//                $natiC = ArrayHelper::map(\app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all(), 'id', 'name');
//            }

            $natiCData = array();
            if($user->type == 0) {
                $natiC = \app\models\Company::find()->all();
            } else {
                $natiC = \app\models\Company::find()->where(['user_id'=>Yii::$app->user->getId()])->all();
            }
            $natiCDataData = array();

            foreach ($natiC as $values) {
                if($values->branch){
                    if($values->branch != ''){
                        $natiCDataData[$values->id] = $values->name."(".$values->branch.")";
                    } else {
                        $natiCDataData[$values->id] = $values->name;
                    }

                } else {
                    $natiCDataData[$values->id] = $values->name;

                }
            }
            echo $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data' => $natiCDataData,
                'options' => ['placeholder' => 'เลือกนายจ้าง ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "clear" => '
                    function(e) {
                

}
                    ',

                    "change" => '
                     function(e) { 
                    if($("#mou-company_id").val()){ 
                     $.ajax({
           url: "'.Yii::$app->request->baseUrl.'/index.php/company/get-company?id="+$("#mou-company_id").val(),
            type: "get",
            dataType: "json",
        }).done(function(response) {
                console.log(response)
                $("#mou-telephone").val(response.telephone);
                $("#mou-fax").val(response.fax);
                $("#mou-address_no").val(response.address_no);
                $("#mou-moo").val(response.moo);
                $("#mou-road").val(response.road);
                $("#mou-province").val(response.province);
                $("#mou-districts").val(response.districts);
                $("#mou-subdistricts").val(response.subdistricts);
                $("#mou-zipcode").val(response.zipcode);
                
                $("#mou-soi").val(response.soi);
                $("#mou-soi_n").val(response.soi_n);
                $("#mou-road_en").val(response.road_en);
                $("#mou-province_en").val(response.province_en);
                $("#mou-districts_en").val(response.districts_en);
                $("#mou-subdistricts_en").val(response.subdistricts_en);
                $("#mou-zipcode").val(response.zipcode);
        }).fail(function() {
            console.log("error");
            
        });
        
        } else {
             $("#mou-telephone").val("");
                $("#mou-fax").val("");
                $("#mou-address_no").val("");
                $("#mou-moo").val("");
                $("#mou-road").val("");
                $("#mou-province").val("");
                $("#mou-districts").val("");
                $("#mou-subdistricts").val("");
                $("#mou-zipcode").val("");
                
                $("#mou-soi").val("");
                $("#mou-soi_n").val("");
                $("#mou-road_en").val("");
                $("#mou-province_en").val("");
                $("#mou-districts_en").val("");
                $("#mou-subdistricts_en").val("");
                $("#mou-zipcode").val("");
        }
      
             }
                    ',
                ],
            ]);
             ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>ประสงค์จะจ้างคนต่างด้าว</h4>
            <?php if(!$model->isNewRecord)  { ?>
                <?php if($user->type == 0) { ?>
                    <?= $form->field($model, 'male')->textInput() ?>
                    <?= $form->field($model, 'female')->textInput() ?>
                <?php } else  {  ?>
                    <?= $form->field($model, 'male')->textInput(['readonly'=> true]) ?>
                    <?= $form->field($model, 'female')->textInput(['readonly'=> true]) ?>
                <?php }  ?>
            <?php } else {  ?>
            <?= $form->field($model, 'male')->textInput() ?>
            <?= $form->field($model, 'female')->textInput() ?>
            <?php }  ?>
            <?= $form->field($model, 'number_price')->textInput() ?>


        </div>
        <div class="col-md-12">
            <h4>คุณสมบัติที่ต้องการ</h4>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'qualification_age')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'qualification_height')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'qualification_weight')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>
    <h4>ข้อมูลสถานที่ทำงาน     <label><input type="checkbox" class="mou-address" name="" value="1" aria-invalid="false"> ที่อยู่เดียวกับนายจ้าง</label>
    </h4>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'address_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'moo')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">

            <?= $form->field($model, 'soi')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'road')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'districts')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'subdistricts')->textInput(['maxlength' => true]) ?>


        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'soi_n')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'road_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'province_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'districts_en')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'subdistricts_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'preriod_year')->textInput() ?>
            <?= $form->field($model, 'preriod_month')->textInput() ?>
            <?= $form->field($model, 'wage_per_day')->textInput() ?>
            <?= $form->field($model, 'week_holiday')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'annual')->textInput() ?>

            <?php
            $natiC = ArrayHelper::map(\app\models\Holiday::find()->all(), 'id', 'title');
            echo $form->field($model, 'holiday_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกวันหยุด']

            ); ?>

            <?= $form->field($model, 'work_time')->textInput() ?>
            <?= $form->field($model, 'paydate')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput() ?>

        </div>
    </div>


    <h4>ข้อมูลลายเซ็นเอกสาร</h4>

    <div class="row">
        <div class="col-md-4">
            <?php
            $natiC = ArrayHelper::map(\app\models\Agent::find()->all(), 'id', 'name');

            echo $form->field($model, 'agent_id')->widget(Select2::classname(), [
                'data' => $natiC,
                'options' => ['placeholder' => 'เลือกเอเจนซี่ ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);


            ?>

                <?php $UserFixed = User::find()->where(['type'=>1])->one(); ?>
                 <?php if(!$model->isNewRecord)  { ?>

                    <?php if($user->type == 0) { ?>

                        <?php

                        $datas = User::find()->where(['is_grantee'=>1])->all();

                        $datalists = [];
                        foreach ($datas as $values){
                            $datalists[$values->id] = $values->first_name." ".$values->last_name;
                        }


                         echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                             'data' => $datalists,
                             'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
                             'pluginOptions' => [
                                 'allowClear' => true
                             ],
                         ]);

                        ?>


                        <div style="display: none;">

                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                        </div>
                    <?php } else { ?>
                         <div style="display: none;">
                             <?php $form->field($model, 'user_id')->textInput(['value'=>Yii::$app->user->id]) ?>
                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                    <?php }?>

                <?php } else { ?>
                <?php if($user->type == 0) { ?>

                        <?php
                        $datas = User::find()->where(['is_grantee'=>1])->all();
                        $datalists = [];
                        foreach ($datas as $values){
                            $datalists[$values->id] = $values->first_name." ".$values->last_name;
                        }

                         echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                             'data' => $datalists,
                             'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
                             'pluginOptions' => [
                                 'allowClear' => true
                             ],
                         ]);

                      ?>

                         <div style="display: none">

                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                     <?php } else { ?>
                         <div style="display: none">

                             <?php echo $form->field($model, 'user_id')->textInput(['value'=>Yii::$app->user->id]) ?>
                             <?= $form->field($model, 'user_id1')->textInput(['value'=>$UserFixed->id]) ?>
                         </div>
                     <?php }?>



                 <?php }?>

        </div>
        <div class="col-md-4">
            <?php
            if($user->type == 0) {
                $datas = Witness::find()->all();
            } else {
                $datas = Witness::find()->where(['user_id'=>Yii::$app->user->getId()])->all();

            }
            $datalists = [];
            foreach ($datas as $values){
                $datalists[$values->id] = $values->first_name." ".$values->last_name;
            }

            echo $form->field($model, 'witness_id')->widget(Select2::classname(), [
                'data' => $datalists,
                'options' => ['placeholder' => 'เลือกพยานคนที่ 1 ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);


            ?>


            <?php

            echo $form->field($model, 'witness_id_two')->widget(Select2::classname(), [
                'data' => $datalists,
                'options' => ['placeholder' => 'เลือกพยานคนที่ 2 ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

              ?>

        </div>
        <div class="col-md-4">
            <?php
            $natiC = ArrayHelper::map(\app\models\DanImmigration::find()->all(), 'id', 'title');
            echo $form->field($model, 'dan_Immigration_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกด่านตรวจคนเข้าเมือง']
            ); ?>
            <?php
            $natiC = ArrayHelper::map(\app\models\Training::find()->all(), 'id', 'title');
            echo $form->field($model, 'training_id')->dropDownList(
                $natiC,
                ['prompt'=>'เลือกศุนย์อบรม']

            ); ?>


        </div>
    </div>


    <?php
    if($user->type == 0) {

//        $datas = User::find()->where(['is_grantee' => 1])->all();
//        $datalists = [];
//        foreach ($datas as $values) {
//            $datalists[$values->id] = $values->first_name . " " . $values->last_name;
//        }
//
//        echo $form->field($model, 'user_other')->widget(Select2::classname(), [
//            'data' => $datalists,
//            'options' => ['placeholder' => 'เลือกผู้รับมอบอำนาจ ...'],
//            'pluginOptions' => [
//                'allowClear' => true
//            ],
//        ]);


        echo $form->field($model, 'cancle')->dropDownList(
            array(0 => "ไม่ใช่", 1 => "ใช่"),
            ['prompt' => 'ยกเลิก.']
        );
    }
    ?>
    <br>
    <br>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>
