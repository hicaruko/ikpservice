<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DanImmigrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dan Immigrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dan-immigration-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dan Immigration', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'title_en',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
