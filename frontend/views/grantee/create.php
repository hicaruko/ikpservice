<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grantee */

$this->title = 'Create Grantee';
$this->params['breadcrumbs'][] = ['label' => 'Grantees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grantee-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
