<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payee".
 *
 * @property int $id
 * @property string $first_name
 * @property string $first_name_en
 * @property string $last_name
 * @property string $last_name_en
 * @property int $prefix_id
 *
 * @property Prefix $prefix
 */
class Payee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'first_name_en', 'last_name', 'last_name_en', 'prefix_id'], 'required'],
            [['prefix_id'], 'integer'],
            [['first_name', 'first_name_en', 'last_name', 'last_name_en'], 'string', 'max' => 255],
            [['prefix_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prefix::className(), 'targetAttribute' => ['prefix_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'first_name_en' => 'First Name En',
            'last_name' => 'Last Name',
            'last_name_en' => 'Last Name En',
            'prefix_id' => 'Prefix ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrefix()
    {
        return $this->hasOne(Prefix::className(), ['id' => 'prefix_id']);
    }

    /**
     * {@inheritdoc}
     * @return PayeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PayeeQuery(get_called_class());
    }
}
