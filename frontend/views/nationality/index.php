<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NationalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nationalities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nationality-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Nationality', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'title_en',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
