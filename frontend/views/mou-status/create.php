<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MouStatus */

$this->title = 'สร้างสถานะ';
$this->params['breadcrumbs'][] = ['label' => 'Mou Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mou-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
