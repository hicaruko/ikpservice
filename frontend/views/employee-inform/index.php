<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มลูกจ้าง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Photo',
                'attribute'=>'photo',
                'headerOptions' => ['width' => '80'],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    return '<img src="'.Url::base().'/'.$model->photo.'" class="" alt="User Image" width="100">';
                },
            ],
            'code',
            'first_name_en',
            'last_name_en',
            'passport',
            'emp_number',
//            'report_status',
            //'nickname',
            //'blood',
            //'date_of_birth',
            //'sex',
            //'contact_number',
            //'age',
            //'email:email',
            //'foreign_address:ntext',
            //'photo:ntext',
            //'passport',
            //'passport_ext',
            //'passport_is',
            //'passport_ext_alert:ntext',
            //'issue_location',
            //'visa_is',
            //'visa_ext',
            //'visa_number',
            //'visa_type',
            //'stamped',
            //'entrance_card',
            //'workpermit_is',
            //'workpermit_ext',
            //'tt_number',
            //'work_permit_number',
            //'job_description',
            //'position',
            //'report_tm_date',
            //'report_status',
            //'mou_id',
            //'type',
            //'address:ntext',
            //'company_id',
            //'nationality_id',
            //'prefix_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
